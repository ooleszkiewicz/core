﻿using System;
using System.Runtime.Serialization;
using CPExpert.Core.Interfaces.Datas;

namespace CPExpert.Core.Objects.Datas.DataBases.Entities
{
    [Serializable]
    [DataContract]
    public class BaseEntity<DB_KEY, DATA_MODEL_TYPE> : IBase<DB_KEY, DATA_MODEL_TYPE>
    {
        public BaseEntity(DATA_MODEL_TYPE entityType)
        {
            var dtNow = DateTime.Now;
            DbKey = default(DB_KEY);
            DataModelType = entityType;
            AppKey = Guid.Empty;
            CreateDate = dtNow;
            LastEditDate = dtNow;
            CreateUser = Guid.Empty;
            LastEditUser = Guid.Empty;
        }

        [DataMember]
        public DB_KEY DbKey { get; set; }

        [DataMember]
        public Guid AppKey { get; set; }

        [DataMember]
        public DATA_MODEL_TYPE DataModelType { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public Guid CreateUser { get; set; }

        [DataMember]
        public DateTime LastEditDate { get; set; }

        [DataMember]
        public Guid LastEditUser { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}