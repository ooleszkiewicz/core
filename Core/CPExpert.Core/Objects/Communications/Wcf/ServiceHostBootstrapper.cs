﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Autofac;
using CPExpert.Core.Interfaces.Communications.Wcf;

namespace CPExpert.Core.Objects.Communications.Wcf
{
    public class ServiceHostBootstrapper
    {
        public readonly IContainer Container;

        public ServiceHostBootstrapper()
        {
            var builder = new ContainerBuilder();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            builder.RegisterAssemblyModules(assemblies);

            builder.RegisterAssemblyTypes(assemblies)
                .Where(type => type.IsAssignableTo<IServiceHostInitializer>())
                .As<IServiceHostInitializer>()
                .SingleInstance();

            Container = builder.Build();
        }

        public void Start()
        {
            foreach (var serviceHostInitializer in Container.Resolve<IEnumerable<IServiceHostInitializer>>())
            {
                var host = serviceHostInitializer.Initialize();
                if (host == null || host.State == CommunicationState.Faulted)
                {
                    throw new Exception("Service host start ERROR");
                }
            }
        }

        public void Stop()
        {
            Dispose();
        }

        private void Dispose()
        {
            Container?.Dispose();
        }
    }
}