﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using CPExpert.Core.Interfaces.Communications.Wcf;

namespace CPExpert.Core.Objects.Communications.Wcf
{
    public class ServiceConnector<WCF_INTERFACE, CHANNEL_FACTORY> where WCF_INTERFACE : IWcfBase where CHANNEL_FACTORY : ChannelFactory<WCF_INTERFACE>
    {
        private readonly ClientCredentials _clientCredentials;

        #region Constructor
        public ServiceConnector(string webServiceAddress, System.ServiceModel.Channels.Binding customBinding, ClientCredentials clientCredentials = null, InstanceContext callbackContext = null, int maxTryReconectOnFaultedOrCloseState = 5)
        {
            _currentTryReconectOnFaultedOrCloseState = 0;
            _maxTryReconectOnFaultedOrCloseState = maxTryReconectOnFaultedOrCloseState;
            WebServiceAddress = webServiceAddress;
            if (WcfServiceIsValidAddress)
            {
                _clientCredentials = clientCredentials;
                if (SetCustomBinding(customBinding))
                {
                    _WcfService = default(WCF_INTERFACE);
                    _clientChannelFactory = null;
                    _callbackContext = callbackContext;
                }
                else
                {
                    throw new Exception("Create binding error!");
                }
            }
            else
            {
                throw new Exception($"WCF service address: [{WebServiceAddress}] is wrong!");
            }
        }
        #endregion

        #region Public methods and implementation
        public bool Connect()
        {
            try
            {
                lock (locker)
                {
                    if (WcfServiceIsValidAddress)
                    {
                        if (CreateClientEndpoint())
                        {
                            if (_WcfService.IsWcfServiceAvailable())
                            {
                                _currentTryReconectOnFaultedOrCloseState = 0;
                                return true;
                            }

                            Diagnostic.LogError($"WCF service at address: [{WebServiceAddress}] is not working!");
                        }
                        else
                        {
                            Diagnostic.LogError($"Error when create client and connect to WCF service");
                        }
                    }
                    else
                    {
                        Diagnostic.LogError($"WCF service address: [{WebServiceAddress}] is wrong!");
                    }
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                Disconnect();
            }

            return false;
        }
        public bool Disconnect()
        {
            try
            {
                lock (locker)
                {
                    if (_clientChannelFactory != null)
                    {
                        _clientChannelFactory.Faulted -= OnFaultOrClosededEventHandler;
                        _clientChannelFactory.Closed -= OnFaultOrClosededEventHandler;
                        _clientChannelFactory.Abort();
                        _clientChannelFactory.Close();
                        _clientChannelFactory = null;
                    }

                    return true;
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
            }

            return false;
        }
        public bool Reconnect()
        {
            if (Disconnect())
            {
                return Connect();
            }

            return false;
        }
        /// <summary>
        /// ONLY FOR TEST!
        /// </summary>
        public void Close(string password)
        {
            if (password.Equals("1qaz@WSX"))
            {
                //only for test!
                _clientChannelFactory.Close();
            }
        }
        #endregion

        public string WebServiceAddress { get; private set; }
        public WCF_INTERFACE WcfService
        {
            get
            {
                lock (locker)
                {
                    if (_WcfService == null ||
                        _clientChannelFactory == null ||
                        _clientChannelFactory.State == CommunicationState.Closed ||
                        _clientChannelFactory.State == CommunicationState.Closing ||
                        _clientChannelFactory.State == CommunicationState.Faulted)
                    {
                        Reconnect();
                    }

                    if (_WcfService != null && _clientChannelFactory != null)
                    {
                        if (_clientChannelFactory.State != CommunicationState.Opened)
                        {
                            var maxTimeOutAttemptToOpenCommunicationChannel = DateTime.Now.AddSeconds(30);
                            while (_clientChannelFactory.State != CommunicationState.Opened && DateTime.Now < maxTimeOutAttemptToOpenCommunicationChannel)
                            {
                                Thread.Sleep(500);
                            }
                        }

                        if (_clientChannelFactory.State == CommunicationState.Opened)
                        {
                            return _WcfService;
                        }

                        throw new Exception("Can't open communication channel between CLIENT and SERVER, try use CONNECT or RECONNECT method, or check service is available and address to service is correct!");
                    }

                    throw new Exception("Unhandled exception!");
                }
            }
        }
        public bool WcfServiceIsValidAddress
        {
            get
            {
                try
                {
                    if (!string.IsNullOrEmpty(WebServiceAddress) && (WebServiceAddress.StartsWith(@"http://") || WebServiceAddress.StartsWith(@"https://") || WebServiceAddress.StartsWith(@"net.tcp://")))
                    {
                        var isSuccess = Uri.IsWellFormedUriString(WebServiceAddress, UriKind.RelativeOrAbsolute);
                        if (isSuccess)
                        {
                            if (Uri.TryCreate(WebServiceAddress, UriKind.RelativeOrAbsolute, out var uriResult))
                            {
                                return uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps || uriResult.Scheme == Uri.UriSchemeNetTcp;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Diagnostic.Log(e);
                }

                return false;
            }
        }

        private readonly object locker = new object();
        private WCF_INTERFACE _WcfService { get; set; }
        private ChannelFactory<WCF_INTERFACE> _clientChannelFactory { get; set; }
        private System.ServiceModel.Channels.Binding _customBinding { get; set; }
        private InstanceContext _callbackContext { get; set; }
        private readonly int _maxTryReconectOnFaultedOrCloseState;
        private int _currentTryReconectOnFaultedOrCloseState;

        #region Private implementation
        private bool CreateClientEndpoint()
        {
            if (_customBinding == null)
            {
                throw new Exception("Required binding data!");
            }

            //wywolujac metode trzeba zadbac o zamkniecie starego klienta polaczonego do wcfa
            if (!Disconnect())
            {
                Diagnostic.LogError("Disconnect ERROR! log on method CreateClientEndpoint");
            }

            if (!WcfServiceIsValidAddress)
            {
                throw new Exception("Required endpoint service address");
            }

            var endpointServiceAddressUri = new Uri(WebServiceAddress);
            var endpointAddress = new EndpointAddress(endpointServiceAddressUri);

            if (typeof(CHANNEL_FACTORY) == typeof(ChannelFactory<WCF_INTERFACE>))
            {
                _clientChannelFactory = new ChannelFactory<WCF_INTERFACE>(_customBinding, endpointAddress);
            }
            else if (typeof(CHANNEL_FACTORY) == typeof(DuplexChannelFactory<WCF_INTERFACE>))
            {
                if (_callbackContext == null)
                {
                    return false;
                }

                _clientChannelFactory = new DuplexChannelFactory<WCF_INTERFACE>(_callbackContext, _customBinding, endpointAddress);
            }


            if (_clientCredentials != null)
            {
                var defaultCredentials = _clientChannelFactory.Endpoint.Behaviors.Find<ClientCredentials>();
                _clientChannelFactory.Endpoint.Behaviors.Remove(defaultCredentials);
                _clientChannelFactory.Endpoint.Behaviors.Add(_clientCredentials);
            }
            _WcfService = _clientChannelFactory.CreateChannel();
            //podpiecie eventów po uruchomieniu klienta
            _clientChannelFactory.Faulted += OnFaultOrClosededEventHandler;
            _clientChannelFactory.Closed += OnFaultOrClosededEventHandler;
            return _WcfService != null && _clientChannelFactory.State != CommunicationState.Faulted;
        }
        private bool SetCustomBinding(System.ServiceModel.Channels.Binding customBinding)
        {
            _customBinding = customBinding ?? throw new Exception("Required customBinding (customBinding is null)");
            return true;
        }
        private void OnFaultOrClosededEventHandler(object sender, EventArgs e)
        {
            if (_maxTryReconectOnFaultedOrCloseState >= _currentTryReconectOnFaultedOrCloseState)
            {
                _currentTryReconectOnFaultedOrCloseState++;
                Reconnect();
            }
            else
            {
                Disconnect();
            }
        }
        #endregion
    }
}