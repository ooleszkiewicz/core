﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CPExpert.Core.Objects.Communications.Wcf.Authorization
{
    [Serializable]
    [DataContract]
    public class AuthorizeUserDataResponse
    {
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid AppKey { get; set; }

        [DataMember]
        public List<AuthorizeUserGroupDataResponse> Groups { get; set; }
    }
}