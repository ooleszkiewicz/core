﻿using System;
using System.Configuration;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.ServiceModel;
using System.Xml;
using CPExpert.Core.Helpers;
using CPExpert.Core.Interfaces.Communications.Wcf;

namespace CPExpert.Core.Objects.Communications.Wcf.Authorization
{
    public class CustomUserValidator : UserNamePasswordValidator
    {
        /// <inheritdoc />
        /// <summary>
        /// Validate user name and password (hashed)
        /// SET on app.config: AuthServiceAddress - wcf service provide auth data
        /// SET on app.config: SYSTEM_KEY - key for encrypt and decrypt data
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="password">User password</param>
        public override void Validate(string userName, string password)
        {
            ServiceConnector<IWcfAuthorization, ChannelFactory<IWcfAuthorization>> authWcfClient = null;
            try
            {
                var binding = new WSHttpBinding
                {
                    Name = "ClientWcf_WSHttpBinding_AuthorizeService",
                    MaxBufferPoolSize = int.MaxValue,
                    MaxReceivedMessageSize = int.MaxValue,
                    ReaderQuotas = new XmlDictionaryReaderQuotas
                    {
                        MaxArrayLength = int.MaxValue,
                        MaxBytesPerRead = int.MaxValue,
                        MaxDepth = int.MaxValue,
                        MaxNameTableCharCount = int.MaxValue,
                        MaxStringContentLength = int.MaxValue
                    },
                    ReceiveTimeout = TimeSpan.FromMinutes(10),
                    SendTimeout = TimeSpan.FromMinutes(10),
                };

                authWcfClient = new ServiceConnector<IWcfAuthorization, ChannelFactory<IWcfAuthorization>>(Convert.ToString(ConfigurationManager.AppSettings["AuthServiceAddress"]), binding);
                if (authWcfClient.Connect())
                {
                    if (!authWcfClient.WcfService.AuthorizeSuccess(Cryptography.Md5Hash.Encoder(userName, Convert.ToString(ConfigurationManager.AppSettings["SYSTEM_KEY"])), Cryptography.Md5Hash.Encoder(password, Convert.ToString(ConfigurationManager.AppSettings["SYSTEM_KEY"]))))
                    {
                        throw new SecurityTokenException($"Username >{userName}< is not allowed access");
                    }
                }
                else
                {
                    throw new Exception("Error when call to authorize service! Authorize service unavailable");
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
            finally
            {
                authWcfClient?.Disconnect();
            }
            
        }
    }
}