﻿using System.ServiceModel;

namespace CPExpert.Core.Objects.Communications.Wcf.Authorization
{
    public class RoleAuthorizationManager : ServiceAuthorizationManager
    {
        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            return true;
        }
    }
}