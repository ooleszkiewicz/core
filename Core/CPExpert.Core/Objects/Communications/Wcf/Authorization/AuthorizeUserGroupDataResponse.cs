﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using CPExpert.Core.Enums.DataBase.Security;

namespace CPExpert.Core.Objects.Communications.Wcf.Authorization
{
    [Serializable]
    [DataContract]
    public class AuthorizeUserGroupDataResponse
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid AppKey { get; set; }

        [DataMember]
        public List<SecurityAccessTypes> AccessTypes { get; set; }
    }
}