﻿namespace CPExpert.Core.Objects
{
    public abstract class Test
    {
        protected Test()
        {
            Initialize();
        }

        public abstract void Initialize();
    }
}