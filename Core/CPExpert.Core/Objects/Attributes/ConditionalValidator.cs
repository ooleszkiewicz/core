﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CPExpert.Core.Helpers;
using CPExpert.Core.Helpers.Internationalization;

namespace CPExpert.Core.Objects.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public abstract class ConditionalValidator : ValidationAttribute
    {
        protected readonly ValidationAttribute InnerAttribute;
        public string DependentProperty { get; set; }
        public object TargetValue { get; set; }
        public object MyValue { get; set; }
        protected abstract string ValidationName { get; }

        protected virtual IDictionary<string, object> GetExtraValidationParameters()
        {
            return new Dictionary<string, object>();
        }

        protected ConditionalValidator(ValidationAttribute innerAttribute, string dependentProperty, object targetValue)
        {
            InnerAttribute = innerAttribute;
            DependentProperty = dependentProperty;
            TargetValue = targetValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // get a reference to the property this validation depends upon
            var containerType = validationContext.ObjectInstance.GetType();
            var field = containerType.GetProperty(DependentProperty);
            if (field != null)
            {
                // get the value of the dependent property
                var dependentvalue = field.GetValue(validationContext.ObjectInstance, null);

                // compare the value against the target value
                if ((dependentvalue == null && TargetValue == null) || (dependentvalue != null && dependentvalue.Equals(TargetValue)))
                {
                    // match => means we should try validating this field
                    if (!InnerAttribute.IsValid(value))
                    {
                        // validation failed - return an error
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                    }
                    if (MyValue != null && validationContext.ObjectInstance.Equals(MyValue))
                    {
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
                    }
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            string value = Translation.Translate(ErrorMessage);
            if (string.IsNullOrEmpty(value)) value = ErrorMessage;
            return string.Format(value, name);
        }
    }
}