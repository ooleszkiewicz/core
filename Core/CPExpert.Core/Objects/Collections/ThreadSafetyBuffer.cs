﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPExpert.Core.Enums;
using CPExpert.Core.Interfaces.Collections;
using CPExpert.Core.Objects.Events.EventArguments;
using Exception = System.Exception;

namespace CPExpert.Core.Objects.Collections
{
    public class ThreadSafetyBuffer<T> : IThreadSafetyBuffer<T>
    {
        public ThreadSafetyBuffer(IList<T> items) : this()
        {
            try
            {
                if (items != null && items.Any())
                {
                    foreach (var item in items)
                    {
                        if (item != null)
                        {
                            _safetyCollection.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public ThreadSafetyBuffer()
        {
            try
            {
                _safetyCollection = new SynchronizedCollection<T>();
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        private readonly SynchronizedCollection<T> _safetyCollection;

        public ICollection<T> GetAll()
        {
            try
            {
                lock (_safetyCollection)
                {
                    if (_safetyCollection != null)
                    {
                        var list = _safetyCollection.ToList();
                        if (list.Any())
                        {
                            var listWithoutNulls = list.Where(x => x != null).ToList();
                            if (listWithoutNulls.Any())
                            {
                                return new List<T>(listWithoutNulls);
                            }
                        }
                    }

                    return new List<T>();
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public bool Add(T item)
        {
            try
            {
                lock (_safetyCollection)
                {
                    if (item != null && _safetyCollection != null)
                    {
                        _safetyCollection.Add(item);
                        var addedElement = GetAll().FirstOrDefault(x => x.Equals(item));
                        if (addedElement != null)
                        {
                            RaiseCollectionChangedEvent(ThreadSafetyBufferChangeTypes.Added, new List<T> {addedElement});
                            return true;
                        }
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public bool AddRange(IList<T> items)
        {
            try
            {
                lock (_safetyCollection)
                {
                    if (items != null && items.Any())
                    {
                        var itemsAddedPass = new List<T>();
                        var itemsAddedFail = new List<T>();

                        foreach (var item in items)
                        {
                            _safetyCollection.Add(item);
                            var addedElement = GetAll().FirstOrDefault(x => x.Equals(item));
                            if (addedElement == null)
                            {
                                itemsAddedFail.Add(item);
                            }
                            else
                            {
                                itemsAddedPass.Add(item);
                            }
                        }

                        if (itemsAddedFail.Any())
                        {
                            //nie wszytskie elementy zostały dodane prawidłowo
                            //musze wycofać te które dodałem prawidłowo
                            foreach (var item in itemsAddedPass)
                            {
                                _safetyCollection.Remove(item);
                            }

                            //sprawdzam czy elementów nie ma na liście
                            foreach (var item in items)
                            {
                                var itemExistOnList = GetAll().FirstOrDefault(x => x.Equals(item)) != null;
                                if (itemExistOnList)
                                {
                                    //jezeli nadal istnieje jakis element po "wywaleniu" sie to zwracam exception'a
                                    throw new Exception("Element after added - error, already exist on TheradSafetyBuffer, REVERT ADDED ERROR!");
                                }
                            }

                            //jezeli wszytsko zostało wycofane prawidłowo to zwracam tylko info że nie udało sie dodac elementów
                            return false;
                        }

                        //udalo sie dodac wszystkie elementy prawidłowo zgłaszam event z lista i status na true (info ze udalo sie dodac)
                        RaiseCollectionChangedEvent(ThreadSafetyBufferChangeTypes.Added, new List<T>(items));
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public bool Remove(T item)
        {
            try
            {
                lock (_safetyCollection)
                {
                    if (item != null && _safetyCollection != null)
                    {
                        var existingItemOnSafetyCollection = GetAll().FirstOrDefault(x => x.Equals(item));

                        if (existingItemOnSafetyCollection != null)
                        {
                            _safetyCollection.Remove(existingItemOnSafetyCollection);
                            T elementAfterRemove = GetAll().FirstOrDefault(x => x.Equals(item));
                            var defaultValue = default(T);
                            if ((defaultValue == null && elementAfterRemove == null) || (defaultValue != null && defaultValue.Equals(elementAfterRemove)))
                            {
                                RaiseCollectionChangedEvent(ThreadSafetyBufferChangeTypes.Removed, new List<T> { item });
                                return true;
                            }
                        }
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public bool RemoveRange(IList<T> items)
        {
            try
            {
                lock (_safetyCollection)
                {
                    if (items != null && items.Any())
                    {
                        var itemsRemovePass = new List<T>();
                        var itemsRemoveFail = new List<T>();

                        foreach (var item in items)
                        {
                            var defaultValue = default(T);
                            _safetyCollection.Remove(item);
                            var elementAfterRemove = GetAll().FirstOrDefault(x => x.Equals(item));
                            if ((defaultValue == null && elementAfterRemove == null) ||
                                (defaultValue != null && defaultValue.Equals(elementAfterRemove)))
                            {
                                //udalo sie usunac
                                itemsRemovePass.Add(item);
                            }
                            else
                            {
                                //nie udalo sie usunac
                                itemsRemoveFail.Add(item);
                            }
                        }

                        if (itemsRemoveFail.Any())
                        {
                            //nie wszytskie elementy zostały usuniete prawidłowo
                            //musze wycofać te które zostały usuniete prawidłowo
                            foreach (var item in itemsRemovePass)
                            {
                                _safetyCollection.Add(item);
                            }

                            //sprawdzam czy elementów wrócił na listę
                            foreach (var item in items)
                            {
                                var defaultValue = default(T);
                                T itemExistOnList = GetAll().FirstOrDefault(x => x.Equals(item));
                                if ((defaultValue == null && itemExistOnList != null) ||
                                    (defaultValue != null && !defaultValue.Equals(itemExistOnList)))
                                {
                                    //jezeli element nie powrócił na listę po "wywaleniu" sie to zwracam exception'a
                                    throw new Exception("Element after removed - error, no exist on TheradSafetyBuffer, REVERT REMOVED ERROR!");
                                }
                            }

                            //jezeli wszytsko zostało wycofane prawidłowo to zwracam tylko info że nie udało sie usunąć elementów
                            return false;
                        }

                        //udalo sie usunąć wszystkie elementy prawidłowo zgłaszam event z lista i status na true (info ze udalo sie usunąć)
                        RaiseCollectionChangedEvent(ThreadSafetyBufferChangeTypes.Removed, new List<T>(items));
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public int Count
        {
            get
            {
                return _safetyCollection.Count;
            }
        }

        public bool Any()
        {
            try
            {
                lock (_safetyCollection)
                {
                    return Count > 0;
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                throw;
            }
        }

        public EventHandler<ThreadSafetyBufferEventArgs<T>> CollectionChangedEvent { get; set; }

        private void RaiseCollectionChangedEvent(ThreadSafetyBufferChangeTypes changeType, IList<T> items)
        {
            if (CollectionChangedEvent != null)
            {
                CollectionChangedEvent(this, new ThreadSafetyBufferEventArgs<T>(changeType, items));
            }
        }
    }
}
