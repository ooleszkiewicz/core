﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using CPExpert.Core.Interfaces.Datas;

namespace CPExpert.Core.Objects.Generators
{
    public class Email
    {
        public Email(bool smtpFromAppConfig = false)
        {
            Template = null;
            Recipients = null;
            Sender = null;
            SmtpServer = null;
            Attachments = null;
            SmtpFromAppConfig = smtpFromAppConfig;
            Errors = new List<string>();
        }

        public void Send()
        {
            bool canSendMessage = Recipients.Any() && Sender != null && SmtpServer != null;

            try
            {
                if (canSendMessage)
                {
                    TrySendMessage();
                }
                else
                {
                    Errors.Add("Error when sending e-mail");
                }
            }
            catch (System.Exception exception)
            {
                Diagnostic.Log(exception);
                throw;
            }
        }

        public IList<string> Errors { get; set; }
        public Email GenerateMessage(IEmailMassage message, string template)
        {
            Message = message;
            Template = template;
            return this;
        }
        public Email AddRecipients(MailAddressCollection recipient)
        {
            Recipients = recipient;
            return this;
        }
        public Email AddSender(MailAddress sender)
        {
            Sender = sender;
            return this;
        }
        public Email SmtpConfig(SmtpClient smtpClient)
        {
            if (SmtpFromAppConfig)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["Smtp"]))
                {
                    SmtpServer = new SmtpClient();
                }
                else
                {
                    Errors.Add("SMTP client config is empty");
                }
            }
            {
                SmtpServer = smtpClient;
            }

            return this;
        }
        public Email AddAttachments(AttachmentCollection attachments)
        {
            Attachments = attachments;
            return this;
        }

        private void TrySendMessage()
        {
            var mail = new MailMessage { From = Sender };

            foreach (var recipient in Recipients)
            {
                mail.To.Add(recipient);
            }

            if (Attachments != null)
            {
                foreach (var attachment in Attachments)
                {
                    mail.Attachments.Add(attachment);
                }
            }

            mail.Subject = Message.Subject;
            mail.Body = TryGenerateBody(Message, Template);
            mail.BodyEncoding = Encoding.UTF8;
            mail.IsBodyHtml = true;

            SmtpServer.Send(mail);
        }
        private static string TryGenerateBody(IEmailMassage message, string template)
        {
            string body = template;

            body = body.Replace("[T_Title]", message.Title);
            body = body.Replace("[T_Welcome]", message.Welcome);
            body = body.Replace("[T_Body]", message.Body);
            body = body.Replace("[T_Footer]", message.Footer);

            return body;
        }

        private IEmailMassage Message { get; set; }
        private string Template { get; set; }
        private MailAddressCollection Recipients { get; set; }
        private MailAddress Sender { get; set; }
        private SmtpClient SmtpServer { get; set; }
        private AttachmentCollection Attachments { get; set; }
        private bool SmtpFromAppConfig { get; set; }
    }
}