﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CPExpert.Core.Enums.Files;
using Exception = System.Exception;
using File = CPExpert.Core.Helpers.File;

namespace CPExpert.Core.Objects.Generators
{
    public class Frame<T> : EventArgs
    {
        public Frame()
        {
            SnapshotDateTime = DateTime.MinValue;
            ImageBytes = null;
            Type = FileExtensions.Undefined;
        }

        /// <summary>
        /// Create from stream (byte[])
        /// </summary>
        /// <param name="image">Image buffer to be create SqFrame</param>
        /// <param name="type">Extension type of image/ picture file</param>
        /// <param name="snapshotDateTime">Create shot image/ picture</param>
        /// <param name="source"></param>
        public void Create(byte[] image, FileExtensions type, DateTime snapshotDateTime, T source)
        {
            if (image != null && image.Length > 0 && type != FileExtensions.Undefined && snapshotDateTime > DateTime.MinValue)
            {
                SnapshotDateTime = snapshotDateTime;
                ImageBytes = image;
                Type = type;
                Source = source;
            }
            else
            {
                SnapshotDateTime = DateTime.MinValue;
                ImageBytes = null;
                Type = FileExtensions.Undefined;
                Source = default(T);
            }
        }

        /// <summary>
        /// Create from file (from storage)
        /// </summary>
        /// <param name="filePath">File paths to be read and create SqFrame</param>
        /// <param name="source"></param>
        public void Create(string filePath, T source)
        {
            try
            {
                if (System.IO.File.Exists(filePath))
                {
                    Source = source;
                    fileName = Path.GetFileName(filePath);
                    var fileExtension = Path.GetExtension(filePath);
                    if (!string.IsNullOrEmpty(fileExtension))
                    {
                        fileExtension = fileExtension.ToLower().Trim('.');

                        foreach (FileExtensions extension in Enum.GetValues(typeof(FileExtensions)))
                        {
                            if (fileExtension.Equals(extension.ToString(), StringComparison.CurrentCultureIgnoreCase))
                            {
                                Type = extension;
                                break;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Wrong file extension, or extension not found!");
                    }

                    if (Type == FileExtensions.Jpeg || Type == FileExtensions.Jpg || Type == FileExtensions.Bmp ||
                        Type == FileExtensions.Gif || Type == FileExtensions.Png)
                    {
                        ImageBytes = GetFile(filePath);
                    }
                    else
                    {
                        throw new Exception("Wrong image file extension!");
                    }

                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                    SnapshotDateTime = GetSnapshotDateTimeFromFileName(fileNameWithoutExtension);

                    if (SnapshotDateTime < new DateTime(1900, 1, 1))
                    {
                        throw new Exception(string.Format("Wrong datetime format, excepted > 1900.01.01 but method GetSnapshotDateTimeFromFileName(Path.GetFileNameWithoutExtension(fileName)); return: {0}", SnapshotDateTime.ToString(CultureInfo.InvariantCulture)));
                    }
                }
                else
                {
                    throw new Exception(string.Format("Can't find file: {0}", filePath));
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <returns></returns>
        private DateTime GetSnapshotDateTimeFromFileName(string fileNameWithoutExtension)
        {
            try
            {
                DateTime parsedDateTime = DateTime.MinValue;
                if (!string.IsNullOrEmpty(fileNameWithoutExtension))
                {
                    //parse datetime from file name
                    string[] dataElements = fileNameWithoutExtension.Split('_');

                    if (dataElements.Length >= 3)
                    {
                        var dateTimeString = string.Format("{0}_{1}", dataElements[dataElements.Length - 2], dataElements[dataElements.Length - 1]);

                        if (!DateTime.TryParseExact(dateTimeString, "yyMMdd_HHmmssfff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                        {
                            if (!DateTime.TryParseExact(dateTimeString, "yyyyMMdd_HHmmssfff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                            {
                                if (!DateTime.TryParseExact(dateTimeString, "yyMMdd_HHmmssff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                                {
                                    if (!DateTime.TryParseExact(dateTimeString, "yyyyMMdd_HHmmssff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                                    {
                                        if (!DateTime.TryParseExact(dateTimeString, "yyyy-MM-dd_HH-mm-ss-fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
                                        {
                                            Diagnostic.LogWarn(string.Format("Cannot parse Image Date Time from image file name. File name: '{0}', device: '{1}'", fileNameWithoutExtension, dataElements[0]));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return parsedDateTime;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                return DateTime.MinValue;
            }
        }

        public DateTime SnapshotDateTime { get; private set; }
        public byte[] ImageBytes { get; private set; }
        public FileExtensions Type { get; private set; }
        public T Source { get; private set; }

        private string fileName { get; set; }
        public string FileName
        {
            get
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    if (ImageBytes != null && ImageBytes.Length > 0)
                    {
                        return string.Format("Image_{0}.{1}", SnapshotDateTime.ToString("yyMMdd_HHmmssfff"), Type.ToString().ToLower());
                    }
                }
                else
                {
                    return fileName;
                }

                return string.Empty;
            }
        }

        public override string ToString()
        {
            return string.Format("SqFrame SnapshotDateTime: {0} imgSize: {1}, type: {2}", SnapshotDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), ImageBytes.Length, Type);
        }

        private byte[] GetFile(string filePath)
        {
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            try
            {
                var task = Task<byte[]>.Factory.StartNew(() =>
                {
                    try
                    {
                        token.ThrowIfCancellationRequested();

                        //staram sie wczytać do pamieci plik z dysku
                        int maxTryGetImageFromFile = 5;
                        int currentTryGetImageFromFile = 0;
                        byte[] fileToReturn = null;

                        while (fileToReturn == null && (maxTryGetImageFromFile > currentTryGetImageFromFile) && !token.IsCancellationRequested)
                        {
                            try
                            {
                                fileToReturn = File.Read(filePath);
                            }
                            catch
                            {
                                Thread.Sleep(100);
                                currentTryGetImageFromFile++;
                            }
                        }

                        if (fileToReturn != null)
                        {
                            return fileToReturn;
                        }
                        
                    }
                    catch (Exception e)
                    {
                        Diagnostic.Log(e);
                        return null;
                    }

                    return null;
                }, token);

                //maksymalny czas na zapis i odczyt pliku poprzez FTP to 30s
                task.Wait(10000, token);
                return task.Result;

            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
            finally
            {
                tokenSource.Cancel();
                tokenSource.Dispose();
            }
        }
    }
}