﻿using System;
using System.Collections.Generic;
using CPExpert.Core.Enums;

namespace CPExpert.Core.Objects.Events.EventArguments
{
    public class ThreadSafetyBufferEventArgs<T> : EventArgs
    {
        public ThreadSafetyBufferEventArgs(ThreadSafetyBufferChangeTypes changeType, IList<T> items)
        {
            ChangeType = changeType;
            Items = items;
        }

        public ThreadSafetyBufferChangeTypes ChangeType { get; set; }
        public IList<T> Items { get; set; }
    }
}