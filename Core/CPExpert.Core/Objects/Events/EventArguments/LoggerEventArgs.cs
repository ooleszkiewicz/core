﻿using System;
using NLog;

namespace CPExpert.Core.Objects.Events.EventArguments
{
    public class LoggerEventArgs : EventArgs
    {
        public LoggerEventArgs(LogLevel logLevel, string message)
        {
            LogLevel = logLevel;
            Message = message;
        }

        public LogLevel LogLevel { get; private set; }
        public string Message { get; private set; }
    }
}