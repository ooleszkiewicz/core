﻿namespace CPExpert.Core.Interfaces.Collections
{
    public interface IOrdered
    {
        int Ordinal { get; set; }
    }
}