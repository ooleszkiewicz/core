﻿using System;
using System.Collections.Generic;
using CPExpert.Core.Objects.Events.EventArguments;

namespace CPExpert.Core.Interfaces.Collections
{
    public interface IThreadSafetyBuffer<T>
    {
        bool Add(T item);
        bool AddRange(IList<T> items);
        bool Remove(T item);
        bool RemoveRange(IList<T> items);
        ICollection<T> GetAll();
        int Count { get; }
        bool Any();
        EventHandler<ThreadSafetyBufferEventArgs<T>> CollectionChangedEvent { get; set; } 
    }
}