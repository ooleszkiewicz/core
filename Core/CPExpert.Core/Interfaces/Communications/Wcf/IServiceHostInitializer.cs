﻿using System;
using System.ServiceModel;

namespace CPExpert.Core.Interfaces.Communications.Wcf
{
    public interface IServiceHostInitializer : IDisposable
    {
        ServiceHost Initialize();
    }
}