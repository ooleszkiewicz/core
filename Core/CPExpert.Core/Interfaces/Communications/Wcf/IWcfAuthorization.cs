﻿using System.ServiceModel;
using CPExpert.Core.Objects.Communications.Wcf.Authorization;

namespace CPExpert.Core.Interfaces.Communications.Wcf
{
    [ServiceContract]
    public interface IWcfAuthorization : IWcfBase
    {
        [OperationContract]
        AuthorizeUserDataResponse GetAuthorizeUserData(string userEmailHashed);

        [OperationContract]
        bool AuthorizeSuccess(string userEmailHashed, string passwordHashed);
    }
}