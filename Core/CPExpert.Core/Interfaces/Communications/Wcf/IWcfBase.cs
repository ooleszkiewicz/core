﻿using System.ServiceModel;

namespace CPExpert.Core.Interfaces.Communications.Wcf
{
    [ServiceContract]
    public interface IWcfBase
    {
        [OperationContract]
        bool IsWcfServiceAvailable();

        [OperationContract]
        int SingleSessionTest_Iterator();
    }
}