﻿using System;

namespace CPExpert.Core.Interfaces.Datas
{
    /// <inheritdoc />
    /// <summary>
    /// Base interface on the basis of which the primary object is created entities in the system implements.
    /// </summary>
    public interface IBase<DB_KEY, ENTITY_TYPE> : ICloneable
    {
        /// <summary>
        /// Data base generated primary key PK
        /// </summary>
        DB_KEY DbKey { get; set; }

        /// <summary>
        ///
        /// </summary>
        ENTITY_TYPE DataModelType { get; set; }

        /// <summary>
        /// A primary key [APP SIDE] entity on the basis of which has been achieved uniqueness of records.
        /// Depending on the implementation of the key may be generated on the application side (the code), or the side of the base
        /// </summary>
        Guid AppKey { get; set; }

        /// <summary>
        /// Entity creation date
        /// </summary>
        DateTime CreateDate { get; set; }

        /// <summary>
        /// Entity creation User (Guid)
        /// </summary>
        Guid CreateUser { get; set; }

        /// <summary>
        /// Entity last edit date
        /// </summary>
        DateTime LastEditDate { get; set; }

        /// <summary>
        /// Entity last edit User.
        /// </summary>
        Guid LastEditUser { get; set; }
    }
}