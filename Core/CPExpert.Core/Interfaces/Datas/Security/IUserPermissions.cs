﻿using CPExpert.Core.Enums.Applications;
using CPExpert.Core.Enums.DataBase;

namespace CPExpert.Core.Interfaces.Datas.Security
{
    public interface IUserPermissions<out SYSTEM_PERMISSIONS_DATA, out USER_DATA>
    {
        bool RecalculateSystemPermisssions();
        USER_DATA InitializeDbUserData { get; }
        AppContextTypes AppContextType { get; }
        DbAccessTypes AccessType { get; }
        SYSTEM_PERMISSIONS_DATA SystemPermissions { get; }
    }
}