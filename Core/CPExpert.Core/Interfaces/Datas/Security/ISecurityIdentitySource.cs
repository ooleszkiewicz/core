﻿using CPExpert.Core.Enums.DataBase.Security;

namespace CPExpert.Core.Interfaces.Datas.Security
{
    public interface ISecurityIdentitySource
    {
        string Name { get; set; }
        IdentitySourceTypes SourceType { get; set; }
    }
}