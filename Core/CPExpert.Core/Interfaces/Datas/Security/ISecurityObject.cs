﻿using CPExpert.Core.Enums.DataBase.Security;

namespace CPExpert.Core.Interfaces.Datas.Security
{
    public interface ISecurityObject
    {
        string Identifier { get; set; }
        string Name { get; set; }
        SecurityObjectTypes Type { get; set; }
    }
}