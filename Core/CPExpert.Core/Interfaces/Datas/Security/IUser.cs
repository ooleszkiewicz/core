﻿namespace CPExpert.Core.Interfaces.Datas.Security
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// 
        /// </summary>
        string Login { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Email { get; set; }
    }
}