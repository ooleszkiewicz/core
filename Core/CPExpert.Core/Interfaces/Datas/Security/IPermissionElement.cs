﻿using System.Collections.Generic;
using CPExpert.Core.Enums.Applications;
using CPExpert.Core.Enums.DataBase.Security;

namespace CPExpert.Core.Interfaces.Datas.Security
{
    public interface IPermissionElement
    {
        AppContextTypes Application { get; set; }
        List<SecurityAccessTypes> AccessRules { get; set; }
    }
}