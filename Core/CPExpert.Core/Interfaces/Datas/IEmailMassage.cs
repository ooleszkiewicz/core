﻿namespace CPExpert.Core.Interfaces.Datas
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEmailMassage
    {
        /// <summary>
        /// 
        /// </summary>
        string Subject { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Welcome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Body { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Footer { get; set; }
    }
}