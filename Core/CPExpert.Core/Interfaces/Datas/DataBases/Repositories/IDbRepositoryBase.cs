﻿using CPExpert.Core.Enums.Applications;
using CPExpert.Core.Interfaces.Datas.Security;

namespace CPExpert.Core.Interfaces.Datas.DataBases.Repositories
{
    public interface IBaseDataRepository<out SYSTEM_PERMISSIONS_DATA, INITIAL_USER_DATA>
    {
        bool Initialize(INITIAL_USER_DATA initializeDbUserData, AppContextTypes appContextType);
        bool IsInitialized { get; }
        IUserPermissions<SYSTEM_PERMISSIONS_DATA, INITIAL_USER_DATA> Permissions { get; }
    }
}