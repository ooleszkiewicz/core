﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CPExpert.Core.Interfaces.Datas.DataBases.Repositories
{
    /// <summary>
    /// Interfaces of implements a generic data access layer
    /// More info: http://blog.magnusmontin.net/2013/05/30/generic-dal-using-entity-framework/
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="SYSTEM_PERMISSIONS_DATA"></typeparam>
    /// <typeparam name="USER_DATA"></typeparam>
    public interface IGenericDataRepository<T, out SYSTEM_PERMISSIONS_DATA, USER_DATA> : IBaseDataRepository<SYSTEM_PERMISSIONS_DATA, USER_DATA> where T : class
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        ///
        /// </summary>
        /// <param name="where"></param>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        ///
        /// </summary>
        /// <param name="where"></param>
        /// <param name="navigationProperties"></param>
        /// <returns></returns>
        T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        ///
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        bool Add(params T[] items);

        /// <summary>
        ///
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        bool Update(params T[] items);

        /// <summary>
        ///
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        bool Remove(params T[] items);

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool Add(T item);

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool Update(T item);

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool Remove(T item);
    }
}