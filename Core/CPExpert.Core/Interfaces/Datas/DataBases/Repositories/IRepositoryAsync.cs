﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPExpert.Core.Objects.Datas.DataBases.Entities;

namespace CPExpert.Core.Interfaces.Datas.DataBases.Repositories
{
    public interface IRepositoryAsync<TEntity, in TUserContextType, TDbKeyType, TDataModelType> : IDisposable where TEntity : BaseEntity<TDbKeyType, TDataModelType>, new()
    {
        IQueryable<TEntity> Queryable { get; }

        Task<TDbKeyType> Add(TEntity entity, TUserContextType userContext);
        Task<ICollection<TDbKeyType>> AddRange(ICollection<TEntity> entities, TUserContextType userContext);

        Task<bool> Delete(TDbKeyType id, TUserContextType userContext);
        Task<IDictionary<TDbKeyType, bool>> DeleteRange(ICollection<TDbKeyType> ids, TUserContextType userContext);
        Task<bool> Delete(Guid appKey, TUserContextType userContext);
        Task<IDictionary<Guid, bool>> DeleteRange(ICollection<Guid> appKeyss, TUserContextType userContext);

        Task<bool> Update(TEntity entity, TUserContextType userContext);
        Task<IDictionary<TDbKeyType, bool>> UpdateRange(ICollection<TEntity> entities, TUserContextType userContext);
    }
}