﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPExpert.Core.Objects.Datas.DataBases.Entities;

namespace CPExpert.Core.Interfaces.Datas.DataBases.Repositories
{
    public interface IRepository<TEntity, in TUserContextType, TDbKeyType, TDataModelType> : IDisposable where TEntity : BaseEntity<TDbKeyType, TDataModelType>, new()
    {
        IQueryable<TEntity> Queryable { get; }

        TDbKeyType Add(TEntity entity, TUserContextType userContext);
        ICollection<TDbKeyType> AddRange(ICollection<TEntity> entities, TUserContextType userContext);

        bool Delete(TDbKeyType id, TUserContextType userContext);
        IDictionary<TDbKeyType, bool> DeleteRange(ICollection<TDbKeyType> ids, TUserContextType userContext);
        bool Delete(Guid appKey, TUserContextType userContext);
        IDictionary<Guid, bool> DeleteRange(ICollection<Guid> appKeyss, TUserContextType userContext);

        bool Update(TEntity entity, TUserContextType userContext);
        IDictionary<TDbKeyType, bool> UpdateRange(ICollection<TEntity> entities, TUserContextType userContext);
    }
}