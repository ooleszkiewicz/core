﻿using System;
using System.Collections.Generic;

namespace CPExpert.Core.Interfaces.Datas.DataBases.Repositories
{
    public interface IBaseRepository<TEntity, in TUserContext> : IDisposable
    {
        //CRUD
        Guid Create(TEntity data, TUserContext userContext);
        TEntity Read(Guid appKey);
        bool Update(TEntity data, TUserContext userContext);
        bool Delete(Guid appKey, TUserContext userContext);
        IList<TEntity> ReadAll();
        bool IsAlreadyExistOnDataBase(string recordControlCheckSum);
    }
}