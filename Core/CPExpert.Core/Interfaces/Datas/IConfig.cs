﻿using System;
using CPExpert.Core.Enums;

namespace CPExpert.Core.Interfaces.Datas
{
    /// <summary>
    /// Base config interface to build config entity/object
    /// </summary>
    public interface IConfig
    {
        /// <summary>
        /// 
        /// </summary>
        string Key { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        ValueTypes ValueType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        DateTime EffectiveDateOf { get; set; }
    }
}