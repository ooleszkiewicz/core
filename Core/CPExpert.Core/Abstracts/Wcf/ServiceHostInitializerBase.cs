﻿using System.Linq;
using System.ServiceModel;
using Autofac;
using Autofac.Integration.Wcf;
using CPExpert.Core.Interfaces.Communications.Wcf;

namespace CPExpert.Core.Abstracts.Wcf
{
    public abstract class ServiceHostInitializerBase<TImplementation, TContract> : IServiceHostInitializer
    {
        readonly ILifetimeScope _lifetimeScope;
        ServiceHost _serviceHost;

        protected ServiceHostInitializerBase(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public ServiceHost Initialize()
        {
            _serviceHost = new ServiceHost(typeof(TImplementation));

            if (_serviceHost.BaseAddresses.Any())
            {
                foreach (var baseAddress in _serviceHost.BaseAddresses)
                {
                    Diagnostic.LogDebug("Service is Running at following address: {0}", baseAddress);
                }
            }

            _serviceHost.AddDependencyInjectionBehavior<TContract>(_lifetimeScope);
            _serviceHost.Open();
            return _serviceHost;
        }

        public void Dispose()
        {
            if (_serviceHost == null)
            {
                return;
            }

            if (_serviceHost.State != CommunicationState.Faulted)
            {
                _serviceHost.Close();
            }
            else
            {
                _serviceHost.Abort();
            }
        }
    }
}