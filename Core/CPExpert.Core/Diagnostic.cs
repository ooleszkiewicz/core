﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CPExpert.Core.Objects.Events.EventArguments;
using NLog;

namespace CPExpert.Core
{
    /// <summary>
    /// Diagnostic for all CpExpert projects.
    /// Until action is needed configuration file NLog.config at the "top" of the project,
    /// the documentation to set up a library NLog available at: https://github.com/NLog/NLog/wiki/Configuration-file
    /// </summary>
    public static class Diagnostic
    {
        /// <summary>
        /// Log message on info level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objectsToLog"></param>
        public static void LogInfo(string message, params object[] objectsToLog)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                if (objectsToLog != null && objectsToLog.Any())
                {
                    var filteredObjects = new object[objectsToLog.Length];
                    objectsToLog.CopyTo(filteredObjects, 0);

                    log.Log(LogLevel.Info, string.Format(message, filteredObjects));
                    RaiseLoggedMessageEvent(LogLevel.Info, string.Format(message, filteredObjects));
                }
                else
                {
                    log.Log(LogLevel.Info, message);
                    RaiseLoggedMessageEvent(LogLevel.Info, message);
                }
            }
        }

        /// <summary>
        /// Log message on debug level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objectsToLog"></param>
        public static void LogDebug(string message, params object[] objectsToLog)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                if (objectsToLog != null && objectsToLog.Any())
                {
                    var filteredObjects = new object[objectsToLog.Length];
                    objectsToLog.CopyTo(filteredObjects, 0);

                    log.Log(LogLevel.Debug, string.Format(message, filteredObjects));
                    RaiseLoggedMessageEvent(LogLevel.Debug, string.Format(message, filteredObjects));
                }
                else
                {
                    log.Log(LogLevel.Debug, message);
                    RaiseLoggedMessageEvent(LogLevel.Debug, message);
                }
            }
        }

        /// <summary>
        /// Log message on error level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objectsToLog"></param>
        public static void LogError(string message, params object[] objectsToLog)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                if (objectsToLog != null && objectsToLog.Any())
                {
                    var filteredObjects = new object[objectsToLog.Length];
                    objectsToLog.CopyTo(filteredObjects, 0);

                    log.Log(LogLevel.Error, string.Format(message, filteredObjects.ToArray()));
                    RaiseLoggedMessageEvent(LogLevel.Error, string.Format(message, filteredObjects));
                }
                else
                {
                    log.Log(LogLevel.Error, message);
                    RaiseLoggedMessageEvent(LogLevel.Error, message);
                }
            }
        }

        /// <summary>
        /// Log message on fatal level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objectsToLog"></param>
        public static void LogFatal(string message, params object[] objectsToLog)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                if (objectsToLog != null && objectsToLog.Any())
                {
                    var filteredObjects = new object[objectsToLog.Length];
                    objectsToLog.CopyTo(filteredObjects, 0);

                    log.Log(LogLevel.Fatal, string.Format(message, filteredObjects));
                    RaiseLoggedMessageEvent(LogLevel.Fatal, string.Format(message, filteredObjects));
                }
                else
                {
                    log.Log(LogLevel.Fatal, message);
                    RaiseLoggedMessageEvent(LogLevel.Fatal, message);
                }
            }
        }

        /// <summary>
        /// Log message on trace level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objectsToLog"></param>
        public static void LogTrace(string message, params object[] objectsToLog)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                if (objectsToLog != null && objectsToLog.Any())
                {
                    var filteredObjects = new object[objectsToLog.Length];
                    objectsToLog.CopyTo(filteredObjects, 0);

                    log.Log(LogLevel.Trace, string.Format(message, filteredObjects));
                    RaiseLoggedMessageEvent(LogLevel.Trace, string.Format(message, filteredObjects));
                }
                else
                {
                    log.Log(LogLevel.Trace, message);
                    RaiseLoggedMessageEvent(LogLevel.Trace, message);
                }
            }
        }

        /// <summary>
        /// Log message on warn level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objectsToLog"></param>
        public static void LogWarn(string message, params object[] objectsToLog)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                if (objectsToLog != null && objectsToLog.Any())
                {
                    var filteredObjects = new object[objectsToLog.Length];
                    objectsToLog.CopyTo(filteredObjects, 0);

                    log.Log(LogLevel.Warn, string.Format(message, filteredObjects));
                    RaiseLoggedMessageEvent(LogLevel.Warn, string.Format(message, filteredObjects));
                }
                else
                {
                    log.Log(LogLevel.Warn, message);
                    RaiseLoggedMessageEvent(LogLevel.Warn, message);
                }
            }
        }

        /// <summary>
        /// Log message from exception
        /// </summary>
        /// <param name="exception"></param>
        public static void Log(Exception exception)
        {
            var currentMethodName = MethodBase.GetCurrentMethod();
            var log = GetLoggerForCalledMethod(currentMethodName.Name);
            if (log != null)
            {
                var message = LogException(exception);

                if (_insertFullNamespace)
                {
                    var calledMethod = GetCallingMethod(currentMethodName.Name);
                    if (calledMethod != null)
                    {
                        var methodName = calledMethod.Name;
                        var reflectedType = calledMethod.ReflectedType;
                        if (reflectedType != null)
                        {
                            message = string.Format("[{0}.{1}] -> {2}", reflectedType.FullName, methodName, message);
                        }
                    }
                }

                log.Log(LogLevel.Error, message);
                RaiseLoggedMessageEvent(LogLevel.Info, string.Format(message, message));
            }
        }

        public static void EnableFullMethodName()
        {
            _insertFullNamespace = true;
        }

        public static void DisableFullMethodName()
        {
            _insertFullNamespace = false;
        }

        public static EventHandler<LoggerEventArgs> LoggedMessageEventHandler { get; set; }

        private static void RaiseLoggedMessageEvent(LogLevel logLevel, string message)
        {
            Task.Factory.StartNew(() =>
            {
                if (LoggedMessageEventHandler != null)
                {
                    LoggedMessageEventHandler(null, new LoggerEventArgs(logLevel, message));
                }
            });
        }

        private static MethodBase GetCallingMethod(string methodCalled)
        {
            try
            {
                var stackTrace = new StackTrace();
                var frames = stackTrace.GetFrames();
                for (var i = 0; i < stackTrace.FrameCount - 1; i++)
                {
                    if (frames != null && frames[i].GetMethod().Name.Equals(methodCalled))
                    {
                        if (!frames[i + 1].GetMethod().Name.Equals(methodCalled))
                        {
                            var methodBase = frames[i + 1].GetMethod();
                            if (methodBase != null)
                            {
                                return methodBase;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger log = LogManager.GetCurrentClassLogger();
                log.Log(LogLevel.Error, LogException(ex));
            }

            return null;
        }
        private static string LogException(Exception exception)
        {
            string message = string.Format(
                "Data: {1}{0} Message: {2}{0} StackTrace: {3}{0} Source: {4}{0}"
                , Environment.NewLine
                , exception.Data
                , exception.Message
                , exception.StackTrace
                , exception.Source);

            if (exception.InnerException != null)
            {
                message += string.Format(":::::::::START INNER EXCEPTION DATE: {1}:::::::::{0}", Environment.NewLine, exception.InnerException.Data);
                message += LogException(exception.InnerException);
                message += string.Format(":::::::::END INNER EXCEPTION DATE: {1}:::::::::{0}", Environment.NewLine, exception.InnerException.Data);
            }

            return message;
        }
        private static Logger GetLoggerForCalledMethod(string methodCalled)
        {
            var callingMethod = GetCallingMethod(methodCalled);
            if (callingMethod != null)
            {
                var reflectedType = callingMethod.ReflectedType;
                if (reflectedType != null)
                {
                    return LogManager.GetLogger(reflectedType.FullName);
                }
            }

            return null;
        }

        private static bool _insertFullNamespace;
    }
}