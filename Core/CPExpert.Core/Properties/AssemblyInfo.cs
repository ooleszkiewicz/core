﻿﻿using System.Reflection;
﻿using System.Resources;
﻿using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CP-EXPERT Core")]
[assembly: AssemblyProduct("CP-EXPERT Core Library")]
[assembly: AssemblyDescription("CP-EXPERT Core library for Computer Programming Expert IT projects")]
[assembly: Guid("d274461a-6bc4-4a2e-b48b-b63c19bbfbec")]

[assembly: AssemblyCompany("CP-EXPERT")]
[assembly: AssemblyCopyright("Copyright © 2018 CP-EXPERT")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.0.108")]
[assembly: NeutralResourcesLanguage("en")]