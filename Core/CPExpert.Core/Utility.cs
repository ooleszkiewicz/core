﻿using System;
using System.Management;
using System.Text.RegularExpressions;
using CPExpert.Core.Helpers;

namespace CPExpert.Core
{
    /// <summary>
    ///
    /// </summary>
    public static class Utility
    {
        public static string GeneratedUniqueHostCode()
        {
            try
            {
                var motherBoardId = string.Empty;
                var motherBoardData = HardwareInfo.GetManagementObjectCollection(HardwareInfo.Keys.Win32_BaseBoard);

                if (motherBoardData != null)
                {
                    var machineName = Environment.MachineName;
                    foreach (var item in motherBoardData)
                    {
                        var mo = (ManagementObject)item;
                        motherBoardId = mo["SerialNumber"].ToString();
                    }

                    if (string.IsNullOrEmpty(machineName) || string.IsNullOrEmpty(motherBoardId))
                    {
                        throw new Exception("Can't generated unique host code");
                    }

                    return Cryptography.HashCheckSum.Calculate(string.Format("[MACHINE_NAME:{0}][MOTHERBOARD_ID:{1}]", machineName, motherBoardId));
                }

                throw new Exception("Can't generated unique host code");
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static double BytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public static double KiloBytesToMegabytes(long kiloBytes)
        {
            return kiloBytes / 1024f;
        }

        public static class RegexPatterns
        {
            public const string ValidRealPattern = @"^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
            public const string ValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
            public const string EmailPattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            public const string PhonePattern = @"^[0-9]{7,}$";

            public static readonly Regex IsAlpha = new Regex("^[a-zA-Z]+$", RegexOptions.Compiled);
            public static readonly Regex IsAlphaNumeric = new Regex("^[a-zA-Z0-9]+$", RegexOptions.Compiled);
            public static readonly Regex IsNotNumber = new Regex("[^0-9.-]", RegexOptions.Compiled);
            public static readonly Regex IsPositiveInteger = new Regex(@"\d{1,10}", RegexOptions.Compiled);
            public static readonly Regex IsNumeric = new Regex("(" + ValidRealPattern + ")|(" + ValidIntegerPattern + ")", RegexOptions.Compiled);
            public static readonly Regex IsWebUrl = new Regex(@"(http|https)://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?", RegexOptions.Singleline | RegexOptions.Compiled);
            public static readonly Regex IsFtpUrl = new Regex(@"(ftp|ftps)://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?", RegexOptions.Singleline | RegexOptions.Compiled);
            public static readonly Regex IsEmail = new Regex(EmailPattern, RegexOptions.Singleline | RegexOptions.Compiled);
            public static readonly Regex RemoveHtml = new Regex(@"<[/]{0,1}\s*(?<tag>\w*)\s*(?<attr>.*?=['""].*?[""'])*?\s*[/]{0,1}>", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            public static readonly Regex IsGuid = new Regex(@"\{?[a-fA-F0-9]{8}(?:-(?:[a-fA-F0-9]){4}){3}-[a-fA-F0-9]{12}\}?", RegexOptions.Compiled);
            public static readonly Regex IsBase64Guid = new Regex(@"[a-zA-Z0-9+/=]{22,24}", RegexOptions.Compiled);
            public static readonly Regex IsIpAddress = new Regex(@"^(((ht)tp(s?))\://)?(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:(6553[0-5]|655[0-2]\d|65[0-4]\d{2}|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}))?$", RegexOptions.Compiled);
            public static readonly Regex IsDomainAddress = new Regex(@"^(((ht)tp(s?))\://)?(\b((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b)(:(6553[0-5]|655[0-2]\d|65[0-4]\d{2}|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}))?$", RegexOptions.Compiled);

            public static readonly Regex HasTwoDot = new Regex("[0-9]*[.][0-9]*[.][0-9]*", RegexOptions.Compiled);
            public static readonly Regex HasTwoMinus = new Regex("[0-9]*[-][0-9]*[-][0-9]*", RegexOptions.Compiled);

            public static readonly Regex IsCultureCode = new Regex(@"^([a-z]{2})|([a-z]{2}-[A-Z]{2})$", RegexOptions.Singleline | RegexOptions.Compiled);
        }

        public static string GetCpExpertCopyright
        {
            get { return string.Format(@"Copyright © {0} CP-EXPERT", DateTime.Now.Year); }
        }
    }
}