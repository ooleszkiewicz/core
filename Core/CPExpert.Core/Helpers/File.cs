﻿using System;
using System.Globalization;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using CPExpert.Core.Enums.Files;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    /// File operation extension
    /// </summary>
    public static class File
    {
        /// <summary>
        /// Method to copy files larger than 2GB
        /// </summary>
        /// <param name="copiedFilePath">File to be copied</param>
        /// <param name="destinationDirectory">Destination folder</param>
        /// <param name="fileNameOnDestinationDirectory">File name on destination folder if null set file name at original copied file</param>
        /// <returns>Copy success?</returns>
        public static bool Copy(string copiedFilePath, string destinationDirectory, string fileNameOnDestinationDirectory = null)
        {
            try
            {
                //check file exist
                if (!System.IO.File.Exists(copiedFilePath))
                {
                    throw new FileNotFoundException("File not found", copiedFilePath);
                }

                //check destination directory and if not exist try create
                if (!Directory.Exists(destinationDirectory))
                {
                    Directory.CreateDirectory(destinationDirectory);
                }

                //ser permission for read file
                SetFullAccessForFile(copiedFilePath);

                //set permission for destination directory
                SetFullAccessForDirectory(destinationDirectory);

                string fileName = fileNameOnDestinationDirectory ?? Path.GetFileName(copiedFilePath);
                if (fileName != null)
                {
                    string destinationFullFilePath = Path.Combine(destinationDirectory, fileName);

                    //set perrmision for destination file if exist
                    if (System.IO.File.Exists(destinationFullFilePath))
                    {
                        SetFullAccessForFile(destinationFullFilePath);
                    }

                    const int bufferSize = 32 * 1024;
                    var buffer = new byte[bufferSize];

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        using (var fileStream = System.IO.File.OpenRead(copiedFilePath))
                        {

                            using (var outFileStream = System.IO.File.OpenWrite(destinationFullFilePath))
                            {
                                int bytesRead;
                                while (0 < (bytesRead = fileStream.Read(buffer, 0, bufferSize)))
                                {
                                    outFileStream.Write(buffer, 0, bytesRead);
                                }
                            }

                        }

                        //checked is file copied
                        if (System.IO.File.Exists(destinationFullFilePath))
                        {
                            return Compare(copiedFilePath, destinationFullFilePath);
                        }

                        throw new Exception(string.Format("I failed to copy the file to the location: {0}", destinationFullFilePath));
                    }

                    throw new Exception(string.Format("Get file name from path: {0} error", copiedFilePath));
                }

                throw new Exception("Can't set file name for new file!");
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// Method to move files larger than 2GB
        /// </summary>
        /// <param name="copiedFilePath">File to be copied</param>
        /// <param name="destinationDirectory">Destination folder</param>
        /// <param name="fileNameOnDestinationDirectory">File name on dest folder if null set file name at original copied file</param>
        public static bool Move(string copiedFilePath, string destinationDirectory, string fileNameOnDestinationDirectory = null)
        {
            try
            {
                bool copySuccess = Copy(copiedFilePath, destinationDirectory, fileNameOnDestinationDirectory);
                if (System.IO.File.Exists(copiedFilePath))
                {
                    System.IO.File.Delete(copiedFilePath);
                }

                return copySuccess && !System.IO.File.Exists(copiedFilePath);
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// The method tries to force full access to the specified file
        /// </summary>
        /// <param name="filePath"></param>
        public static void SetFullAccessForFile(string filePath)
        {
            try
            {
                SetFullAccessForDirectory(Path.GetDirectoryName(filePath));
                var account = WindowsIdentity.GetCurrent(false).Name;
                var fileSecurity = System.IO.File.GetAccessControl(filePath);
                fileSecurity.AddAccessRule(new FileSystemAccessRule(account, FileSystemRights.FullControl, AccessControlType.Allow));
                System.IO.File.SetAccessControl(filePath, fileSecurity);
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// The method tries to force full access to the specified directory
        /// </summary>
        /// <param name="directoryPath"></param>
        public static void SetFullAccessForDirectory(string directoryPath)
        {
            try
            {
                var account = WindowsIdentity.GetCurrent(false).Name;
                var directorySecurity = Directory.GetAccessControl(directoryPath);
                directorySecurity.AddAccessRule(new FileSystemAccessRule(account, FileSystemRights.FullControl, AccessControlType.Allow));
                Directory.SetAccessControl(directoryPath, directorySecurity);
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// Method read file for file less than 2GB
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static byte[] Read(string filePath)
        {
            try
            {
                return System.IO.File.ReadAllBytes(filePath);
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// Method write file for file less than 2GB
        /// </summary>
        /// <param name="destinationFilePath"></param>
        /// <param name="file"></param>
        public static void Write(string destinationFilePath, byte[] file)
        {
            try
            {
                System.IO.File.WriteAllBytes(destinationFilePath, file);
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// If the file is still locked (XP: being recorded)
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileLocked(string filePath)
        {
            return !FileIsFullWritten(filePath, 20, 100, new object());
        }

        public static bool FileIsFullWritten(string filePath, int maxTry, int sleepTime, object locker)
        {
            lock (locker)
            {
                bool fileIsFullWritten = false;
                var currentTry = 0;
                while (!fileIsFullWritten && currentTry <= maxTry)
                {
                    try
                    {
                        fileIsFullWritten = System.IO.File.ReadAllBytes(filePath).Length > 0;
                    }
                    catch
                    {
                        // ignored
                    }
                    finally
                    {
                        if (!fileIsFullWritten)
                        {
                            Thread.Sleep(sleepTime);
                            currentTry++;
                        }
                    }
                }

                return fileIsFullWritten;
            }
        }

        /// <summary>
        /// This method accepts two strings the represent two files to
        /// compare. A return value of 0 indicates that the contents of the files
        /// are the same. A return value of any other value indicates that the
        /// files are not the same.
        /// </summary>
        /// <param name="leftFilePathToBeCompare">File path for left file to be compare</param>
        /// <param name="rightFilePathToBeCompare">File path for right file to be compare</param>
        /// <returns></returns>
        public static bool Compare(string leftFilePathToBeCompare, string rightFilePathToBeCompare)
        {
            try
            {
                int file1Byte;
                int file2Byte;

                // Determine if the same file was referenced two times.
                if (leftFilePathToBeCompare == rightFilePathToBeCompare)
                {
                    // Return true to indicate that the files are the same.
                    return true;
                }

                // Open the two files.
                var fs1 = new FileStream(leftFilePathToBeCompare, FileMode.Open);
                var fs2 = new FileStream(rightFilePathToBeCompare, FileMode.Open);

                // Check the file sizes. If they are not the same, the files
                // are not the same.
                if (fs1.Length != fs2.Length)
                {
                    // Close the file
                    fs1.Close();
                    fs2.Close();

                    // Return false to indicate files are different
                    return false;
                }

                // Read and compare a byte from each file until either a
                // non-matching set of bytes is found or until the end of
                // file1 is reached.
                do
                {
                    // Read one byte from each file.
                    file1Byte = fs1.ReadByte();
                    file2Byte = fs2.ReadByte();
                }
                while ((file1Byte == file2Byte) && (file1Byte != -1));

                // Close the files.
                fs1.Close();
                fs2.Close();

                // Return the success of the comparison. "file1byte" is
                // equal to "file2byte" at this point only if the files are
                // the same.
                return ((file1Byte - file2Byte) == 0);
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fileName">Full file name ex: "zamczysko_axis_20151014_14183746.jpg"</param>
        /// <returns></returns>
        public static DateTime GetDateTimeFromFileNameFormat(string fileName)
        {
            DateTime createDateTimeShot = DateTime.MinValue;

            try
            {
                if (createDateTimeShot == DateTime.MinValue)
                {
                    //filename format example:
                    //zamczysko1_150923_120001421.jpg
                    //zamczysko_axis_20151014_14183746 - axis doesn`t send fff -> only 1/100 - ff
                    //old arh format: IITTTI_2015-09-03_20-47-17-761
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);

                    if (!string.IsNullOrEmpty(fileNameWithoutExtension))
                    {
                        string[] dataElements = fileNameWithoutExtension.Split('_');

                        if (dataElements.Length >= 3)
                        {
                            var dateTimeString = string.Format("{0}_{1}", dataElements[dataElements.Length - 2],
                                dataElements[dataElements.Length - 1]);

                            DateTime parsedDt;
                            if (!DateTime.TryParseExact(dateTimeString, "yyMMdd_HHmmssfff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDt))
                            {
                                if (!DateTime.TryParseExact(dateTimeString, "yyyyMMdd_HHmmssfff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDt))
                                {
                                    if (!DateTime.TryParseExact(dateTimeString, "yyMMdd_HHmmssff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDt))
                                    {
                                        if (!DateTime.TryParseExact(dateTimeString, "yyyyMMdd_HHmmssff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDt))
                                        {
                                            if (!DateTime.TryParseExact(dateTimeString, "yyyy-MM-dd_HH-mm-ss-fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDt))
                                            {
                                                parsedDt = DateTime.MinValue;
                                            }
                                        }
                                    }
                                }
                            }

                            createDateTimeShot = parsedDt;
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("File name error. File name: '{0}'", fileName));
                    }
                }

                return createDateTimeShot;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                return DateTime.MinValue;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullFileNameWithExtension"></param>
        /// <returns></returns>
        public static FileExtensions GetFileExtension(string fullFileNameWithExtension)
        {
            try
            {
                string fileFormatFromFullFileName = Path.GetExtension(fullFileNameWithExtension);

                if (!string.IsNullOrEmpty(fileFormatFromFullFileName))
                {
                    string fileFormatClean = fileFormatFromFullFileName.Trim('.', ' ');
                    return (FileExtensions)Enum.Parse(typeof(FileExtensions), fileFormatClean, true);
                }

                return FileExtensions.Undefined;

            }
            catch (ArgumentException)
            {
                return FileExtensions.Undefined;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}