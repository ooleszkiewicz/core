﻿using System.ComponentModel;
using System.Linq;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    /// Enum extension for:
    /// - converts from a [Description(&quot;&quot;)] to an enum value
    /// - grabs the [Description(&quot;&quot;)] from an enum value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Enum<T>
    {
        /// <summary>
        /// Get enum description for enum, if not exist description get default value of description
        /// </summary>
        /// <param name="enumValue">Specific enum</param>
        /// <param name="defaultDescription">Default distription</param>
        /// <returns>Enum description</returns>
        public static string GetDescription(T enumValue, string defaultDescription)
        {
            try
            {
                var fi = enumValue.GetType().GetField(enumValue.ToString());
                var attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                return attrs.Length > 0 ? ((DescriptionAttribute)attrs[0]).Description : defaultDescription;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        /// <summary>
        /// Get enum description for enum
        /// </summary>
        /// <param name="enumValue">Specific enum</param>
        /// <returns>Enum description</returns>
        public static string GetDescription(T enumValue)
        {
            return GetDescription(enumValue, string.Empty);
        }

        /// <summary>
        /// Get enum by description value
        /// </summary>
        /// <param name="description">Description</param>
        /// <returns>Enum</returns>
        public static T GetEnum(string description)
        {
            try
            {
                var t = typeof(T);
                foreach (var fi in t.GetFields())
                {
                    var attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    if (attrs.Length > 0)
                    {
                        if (attrs.Cast<DescriptionAttribute>().Any(attr => attr.Description.Equals(description)))
                        {
                            return (T)fi.GetValue(null);
                        }
                    }
                }
                return default(T);
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}