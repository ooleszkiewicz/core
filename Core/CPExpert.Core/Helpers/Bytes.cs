﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Polenter.Serialization;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class Bytes
    {
        private static readonly SharpSerializerXmlSettings _settings = new SharpSerializerXmlSettings
        {
            IncludeAssemblyVersionInTypeName = true,
            IncludeCultureInTypeName = true,
            IncludePublicKeyTokenInTypeName = true,
            Encoding = Encoding.Default
        };

        public static byte[] Combine(byte[] first, byte[] second)
        {
            try
            {
                var ret = new byte[first.Length + second.Length];
                Buffer.BlockCopy(first, 0, ret, 0, first.Length);
                Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
                return ret;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static int IndexOf(byte[] data, byte[] pattern, int startIndex = 0)
        {
            var failure = ComputeFailure(pattern);

            var j = 0;

            for (var i = startIndex; i < data.Length; i++)
            {
                while (j > 0 && pattern[j] != data[i])
                {
                    j = failure[j - 1];
                }
                if (pattern[j] == data[i])
                {
                    j++;
                }
                if (j == pattern.Length)
                {
                    return i - pattern.Length + 1;
                }
            }
            return -1;
        }

        public static byte[] FastCopy(byte[] data)
        {
            try
            {
                if (data != null && data.Length > 0)
                {
                    var dataCopied = new byte[data.Length];
                    data.CopyTo(dataCopied, 0);
                    if (dataCopied.SequenceEqual(data))
                    {
                        return dataCopied;
                    }
                }

                return null;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        private static int[] ComputeFailure(IList<byte> pattern)
        {
            var failure = new int[pattern.Count];

            var j = 0;
            for (var i = 1; i < pattern.Count; i++)
            {
                while (j > 0 && pattern[j] != pattern[i])
                {
                    j = failure[j - 1];
                }
                if (pattern[j] == pattern[i])
                {
                    j++;
                }
                failure[i] = j;
            }

            return failure;
        }


        public static byte[] Serialize(object obj, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                SharpSerializer serializer;
                if (settings != null)
                {
                    serializer = new SharpSerializer(settings);
                }
                else
                {
                    serializer = new SharpSerializer(_settings);
                }

                serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));
                var stream = new MemoryStream();
                serializer.Serialize(obj, stream);
                return stream.ToArray();
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static void SerializeToFile(object obj, string filePath, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                var directoryPath = Path.GetDirectoryName(filePath);

                if (!string.IsNullOrEmpty(directoryPath))
                {
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    Diagnostic.LogDebug(string.Format("Serialize to file: {0}", filePath));
                    SharpSerializer serializer;
                    if (settings != null)
                    {
                        serializer = new SharpSerializer(settings);
                    }
                    else
                    {
                        serializer = new SharpSerializer(_settings);
                    }
                    serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));
                    serializer.Serialize(obj, filePath);
                }
                else
                {
                    throw new Exception("Serialize directory path error!");
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static object Deserialize(byte[] fileBytes, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                object obj;

                SharpSerializer serializer;
                if (settings != null)
                {
                    serializer = new SharpSerializer(settings);

                    serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));

                    using (var stream = new MemoryStream(fileBytes))
                    {
                        obj = serializer.Deserialize(stream);
                    }

                    return obj;
                }

                serializer = new SharpSerializer(_settings);

                serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));

                using (var stream = new MemoryStream(fileBytes))
                {
                    obj = serializer.Deserialize(stream);
                }

                return obj;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static object DeserializeFromFile(string filePath, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                var directoryPath = Path.GetDirectoryName(filePath);

                if (!string.IsNullOrEmpty(directoryPath))
                {
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    if (System.IO.File.Exists(filePath))
                    {
                        Diagnostic.LogDebug(string.Format("Deserialize from file: {0}", filePath));

                        SharpSerializer serializer;
                        if (settings != null)
                        {
                            serializer = new SharpSerializer(settings);
                        }
                        else
                        {
                            serializer = new SharpSerializer(_settings);
                        }

                        serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));
                        return serializer.Deserialize(filePath);
                    }

                    Diagnostic.LogDebug("Deserialize file not found!");
                }
                else
                {
                    Diagnostic.LogDebug("Deserialize directory path error!");
                }

                return null;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}