﻿using System.Collections.Generic;
using System.Globalization;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    ///
    /// </summary>
    public static class Validators
    {
        /// <summary>
        /// Check string for valid NIP number
        /// </summary>
        /// <param name="value">NIP</param>
        /// <returns>
        /// <c>true</c> if value is a valid NIP number; otherwise <c>false</c>
        /// </returns>
        public static bool Nip(string value)
        {
            int countZeros = 0;
            if (value.Length == 10 && System.Text.RegularExpressions.Regex.IsMatch(value, @"^[\d]{10}$"))
            {
                for (int i = 0; i < 10; i++)
                {
                    if (int.Parse(value.Substring(i, 1)) == 0 )
                    {
                        countZeros++;
                    }
                }
                if (countZeros == 10)
                {
                    return false;
                }
            }
            int[] multipliers = { 6, 5, 7, 2, 3, 4, 5, 6, 7 };

            if (System.String.IsNullOrEmpty(value))
            {
                return false;
            }

            if (value.Length >= 10)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(value, @"^[\d]{10}$"))
                {
                    var sum = 0;
                    for (int i = 8; i >= 0; i--)
                    {
                        sum += multipliers[i] * int.Parse(value.Substring(i, 1));
                    }
                    return ((sum % 11) != 10 && ((sum % 11) == int.Parse(value.Substring(9, 1))));
                }
            }

            return false;
        }
        /// <summary>
        /// Check string for valid PESEL number
        /// </summary>
        /// <param name="value">PESEL</param>
        /// <returns>
        /// <c>true</c> if value is a valid PESEL number; otherwise <c>false</c>
        /// </returns>
        public static bool Pesel(string value)
        {
            int[] multipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1 };

            if (System.String.IsNullOrEmpty(value))
            {
                return false;
            }

            if (value.Length >= 11)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(value, @"^\d{11}$"))
                {
                    var sum = 0;
                    for (int i = 0; i < 11; i++)
                    {
                        sum += multipliers[i] * int.Parse(value.Substring(i, 1));
                    }
                    return sum % 10 == 0;
                }
            }

            return false;
        }
        /// <summary>
        /// Check string for valid REGON number
        /// </summary>
        /// <param name="value">REGON</param>
        /// <returns>
        /// <c>true</c> if value is a valid REGON number; otherwise <c>false</c>
        /// </returns>
        public static bool Regon(string value)
        {
            int controlSum;
            switch (value.Length)
            {
                case 9:
                case 7:
                    {
                        int[] weights = { 8, 9, 2, 3, 4, 5, 6, 7 };
                        var offset = 9 - value.Length;
                        controlSum = CalculateControlSum(value, weights, offset);
                    }
                    break;
                case 14:
                    {
                        int[] weights = { 2, 4, 8, 5, 0, 9, 7, 3, 6, 1, 2, 4, 8 };
                        controlSum = CalculateControlSum(value, weights);
                    }
                    break;
                default:
                    return false;
            }

            if (controlSum < 0)
            {
                return false;
            }

            var controlNum = controlSum % 11;
            if (controlNum == 10)
            {
                controlNum = 0;
            }
            var lastDigit = int.Parse(value[value.Length - 1].ToString(CultureInfo.InvariantCulture));

            return controlNum == lastDigit;
        }

        private static int CalculateControlSum(string input, IList<int> weights, int offset = 0)
        {
            try
            {
                var controlSum = 0;
                for (var i = 0; i < input.Length - 1; i++)
                {
                    controlSum += weights[i + offset] * int.Parse(input[i].ToString(CultureInfo.InvariantCulture));
                }
                return controlSum;
            }
            catch
            {
                return -1;
            }
        }
    }
}