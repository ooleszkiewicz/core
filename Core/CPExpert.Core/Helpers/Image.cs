﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class Image
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static Bitmap CreateBitmapFromByteArray(byte[] file)
        {
            try
            {
                if (file != null && file.Length > 5)
                {
                    using (var memoryStream = new MemoryStream(file))
                    {
                        var image = System.Drawing.Image.FromStream(memoryStream);
                        return new Bitmap(image);
                    }
                }

                return null;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="imageFormat"></param>
        /// <returns></returns>
        public static byte[] CreateByteArrayFromBitmap(Bitmap bitmap, ImageFormat imageFormat = null)
        {
            try
            {
                if (imageFormat == null)
                {
                    imageFormat = ImageFormat.Jpeg;
                }

                var ms = new MemoryStream();
                bitmap.Save(ms, imageFormat);
                return ms.ToArray();
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                return null;
            }
        }

        /// <summary>
        /// Gets the dimensions of an image.
        /// </summary>
        /// <param name="path">The path of the image to get the dimensions of.</param>
        /// <returns>The dimensions of the specified image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognized format.</exception>
        public static Size GetDimensions(string path)
        {
            using (BinaryReader binaryReader = new BinaryReader(System.IO.File.OpenRead(path)))
            {
                try
                {
                    return GetDimensions(binaryReader);
                }
                catch (ArgumentException e)
                {
                    if (e.Message.StartsWith(errorMessage))
                    {
                        throw new ArgumentException(errorMessage, "path", e);
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Gets the dimensions of an image.
        /// </summary>
        /// <param name="file"></param>
        /// <returns>The dimensions of the specified image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognized format.</exception>
        public static Size GetDimensions(byte[] file)
        {
            Stream stream = new MemoryStream(file);
            using (var binaryReader = new BinaryReader(stream))
            {
                try
                {
                    return GetDimensions(binaryReader);
                }
                catch (ArgumentException e)
                {
                    if (e.Message.StartsWith(errorMessage))
                    {
                        throw new ArgumentException(errorMessage, @"file", e);
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Gets the dimensions of an image.
        /// </summary>
        /// <param name="binaryReader"></param>
        /// <returns>The dimensions of the specified image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognized format.</exception>    
        public static Size GetDimensions(BinaryReader binaryReader)
        {
            var maxMagicBytesLength = imageFormatDecoders.Keys.OrderByDescending(x => x.Length).First().Length;

            var magicBytes = new byte[maxMagicBytesLength];

            for (var i = 0; i < maxMagicBytesLength; i += 1)
            {
                magicBytes[i] = binaryReader.ReadByte();

                foreach (var kvPair in imageFormatDecoders.Where(kvPair => magicBytes.StartsWith(kvPair.Key)))
                {
                    return kvPair.Value(binaryReader);
                }
            }

            throw new ArgumentException(errorMessage, "binaryReader");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsValidJpeg(byte[] file)
        {
            try
            {
                if (file != null && file.Length > 0 && Equals(GetImageFormat(file), ImageFormat.Jpeg))
                {
                    //mby additional jpeg validators? here
                    return true;
                }

                return false;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static ImageFormat GetImageFormat(byte[] file)
        {
            // see http://www.mikekunz.com/image_file_header.html  
            var bmp = Encoding.ASCII.GetBytes("BM");     // BMP
            var gif = Encoding.ASCII.GetBytes("GIF");    // GIF
            var png = new byte[] { 137, 80, 78, 71 };    // PNG
            var tiff = new byte[] { 73, 73, 42 };         // TIFF
            var tiff2 = new byte[] { 77, 77, 42 };         // TIFF
            var jpeg = new byte[] { 255, 216, 255, 224 }; // jpeg
            var jpeg2 = new byte[] { 255, 216, 255, 225 }; // jpeg canon

            if (bmp.SequenceEqual(file.Take(bmp.Length)))
                return ImageFormat.Bmp;

            if (gif.SequenceEqual(file.Take(gif.Length)))
                return ImageFormat.Gif;

            if (png.SequenceEqual(file.Take(png.Length)))
                return ImageFormat.Png;

            if (tiff.SequenceEqual(file.Take(tiff.Length)))
                return ImageFormat.Tiff;

            if (tiff2.SequenceEqual(file.Take(tiff2.Length)))
                return ImageFormat.Tiff;

            if (jpeg.SequenceEqual(file.Take(jpeg.Length)))
                return ImageFormat.Jpeg;

            if (jpeg2.SequenceEqual(file.Take(jpeg2.Length)))
                return ImageFormat.Jpeg;

            return null;
        }

        /// <summary>
        /// Resize the image to the specified width and height. (High quality)
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsOneColor(byte[] file)
        {
            try
            {
                if (file != null && file.Length > 10)
                {
                    var bmp = CreateBitmapFromByteArray(file);

                    if (bmp != null)
                    {
                        lock (bmp)
                        {
                            // Lock the bitmap's bits.  
                            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                            var bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

                            // Get the address of the first line.
                            var ptr = bmpData.Scan0;

                            // Declare an array to hold the bytes of the bitmap.
                            var bytes = bmpData.Stride * bmp.Height;
                            var rgbValues = new byte[bytes];
                            var firstByte = rgbValues.FirstOrDefault();

                            // Copy the RGB values into the array.

                            Marshal.Copy(ptr, rgbValues, 0, bytes);

                            bool allOneColor = rgbValues.All(rgbValue => firstByte == rgbValue);

                            // Unlock the bits.
                            bmp.UnlockBits(bmpData);
                            return allOneColor;
                        }
                    }
                    else
                    {
                        throw new System.Exception("Bitmap is empty");
                    }
                }
                else
                {
                    throw new System.Exception("File is empty");
                }
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        #region Private implementation

        private const string errorMessage = "Could not recognize image format.";

        private static readonly Dictionary<byte[], Func<BinaryReader, Size>> imageFormatDecoders = new Dictionary<byte[], Func<BinaryReader, Size>>()
        {
            { new byte[]{ 0x42, 0x4D }, DecodeBitmap},
            { new byte[]{ 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 }, DecodeGif },
            { new byte[]{ 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 }, DecodeGif },
            { new byte[]{ 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }, DecodePng },
            { new byte[]{ 0xff, 0xd8 }, DecodeJfif },
        };

        private static bool StartsWith(this byte[] thisBytes, byte[] thatBytes)
        {
            for (int i = 0; i < thatBytes.Length; i += 1)
            {
                if (thisBytes[i] != thatBytes[i])
                {
                    return false;
                }
            }
            return true;
        }

        private static short ReadLittleEndianInt16(this BinaryReader binaryReader)
        {
            byte[] bytes = new byte[sizeof(short)];
            for (int i = 0; i < sizeof(short); i += 1)
            {
                bytes[sizeof(short) - 1 - i] = binaryReader.ReadByte();
            }
            return BitConverter.ToInt16(bytes, 0);
        }

        private static int ReadLittleEndianInt32(this BinaryReader binaryReader)
        {
            byte[] bytes = new byte[sizeof(int)];
            for (int i = 0; i < sizeof(int); i += 1)
            {
                bytes[sizeof(int) - 1 - i] = binaryReader.ReadByte();
            }
            return BitConverter.ToInt32(bytes, 0);
        }

        private static Size DecodeBitmap(BinaryReader binaryReader)
        {
            binaryReader.ReadBytes(16);
            int width = binaryReader.ReadInt32();
            int height = binaryReader.ReadInt32();
            return new Size(width, height);
        }

        private static Size DecodeGif(BinaryReader binaryReader)
        {
            int width = binaryReader.ReadInt16();
            int height = binaryReader.ReadInt16();
            return new Size(width, height);
        }

        private static Size DecodePng(BinaryReader binaryReader)
        {
            binaryReader.ReadBytes(8);
            int width = binaryReader.ReadLittleEndianInt32();
            int height = binaryReader.ReadLittleEndianInt32();
            return new Size(width, height);
        }

        private static Size DecodeJfif(BinaryReader binaryReader)
        {
            while (binaryReader.ReadByte() == 0xff)
            {
                byte marker = binaryReader.ReadByte();
                short chunkLength = binaryReader.ReadLittleEndianInt16();

                if (marker == 0xc0)
                {
                    binaryReader.ReadByte();

                    int height = binaryReader.ReadLittleEndianInt16();
                    int width = binaryReader.ReadLittleEndianInt16();
                    return new Size(width, height);
                }

                binaryReader.ReadBytes(chunkLength - 2);
            }

            throw new ArgumentException(errorMessage);
        }

        #endregion
    }
}