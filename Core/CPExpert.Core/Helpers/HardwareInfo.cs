﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;

namespace CPExpert.Core.Helpers
{
    public static class HardwareInfo
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="key">Hardware key</param>
        /// <returns>Information about hardware</returns>
        public static ManagementObjectCollection GetManagementObjectCollection(string key)
        {
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + key);
                return searcher.Get();
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static IList<ManagementBaseObject> GetManagementObjects(string key)
        {
            try
            {
                var managementObjectCollection = GetManagementObjectCollection(key);
                if (managementObjectCollection != null)
                {
                    var array = new ManagementBaseObject[managementObjectCollection.Count];
                    managementObjectCollection.CopyTo(array, 0);
                    return new List<ManagementBaseObject>(array);
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }

            return null;
        }

        public static class Keys
        {
            public static string Win32_1394Controller = @"Win32_1394Controller";
            public static string Win32_1394ControllerDevice = @"Win32_1394ControllerDevice";
            public static string Win32_Account = @"Win32_Account";
            public static string Win32_AccountSID = @"Win32_AccountSID";
            public static string Win32_ACE = @"Win32_ACE";
            public static string Win32_ActionCheck = @"Win32_ActionCheck";
            public static string Win32_AllocatedResource = @"Win32_AllocatedResource";
            public static string Win32_ApplicationCommandLine = @"Win32_ApplicationCommandLine";
            public static string Win32_ApplicationService = @"Win32_ApplicationService";
            public static string Win32_AssociatedBattery = @"Win32_AssociatedBattery";
            public static string Win32_AssociatedProcessorMemory = @"Win32_AssociatedProcessorMemory";
            public static string Win32_BaseBoard = @"Win32_BaseBoard";
            public static string Win32_BaseService = @"Win32_BaseService";
            public static string Win32_Battery = @"Win32_Battery";
            public static string Win32_Binary = @"Win32_Binary";
            public static string Win32_BindImageAction = @"Win32_BindImageAction";
            public static string Win32_BIOS = @"Win32_BIOS";
            public static string Win32_BootConfiguration = @"Win32_BootConfiguration";
            public static string Win32_Bus = @"Win32_Bus";
            public static string Win32_CacheMemory = @"Win32_CacheMemory";
            public static string Win32_CDROMDrive = @"Win32_CDROMDrive";
            public static string Win32_CheckCheck = @"Win32_CheckCheck";
            public static string Win32_CIMLogicalDeviceCIMDataFile = @"Win32_CIMLogicalDeviceCIMDataFile";
            public static string Win32_ClassicCOMApplicationClasses = @"Win32_ClassicCOMApplicationClasses";
            public static string Win32_ClassicCOMClass = @"Win32_ClassicCOMClass";
            public static string Win32_ClassicCOMClassSetting = @"Win32_ClassicCOMClassSetting";
            public static string Win32_ClassicCOMClassSettings = @"Win32_ClassicCOMClassSettings";
            public static string Win32_ClassInfoAction = @"Win32_ClassInfoAction";
            public static string Win32_ClientApplicationSetting = @"Win32_ClientApplicationSetting";
            public static string Win32_CodecFile = @"Win32_CodecFile";
            public static string Win32_COMApplication = @"Win32_COMApplication";
            public static string Win32_COMApplicationClasses = @"Win32_COMApplicationClasses";
            public static string Win32_COMApplicationSettings = @"Win32_COMApplicationSettings";
            public static string Win32_COMClass = @"Win32_COMClass";
            public static string Win32_ComClassAutoEmulator = @"Win32_ComClassAutoEmulator";
            public static string Win32_ComClassEmulator = @"Win32_ComClassEmulator";
            public static string Win32_CommandLineAccess = @"Win32_CommandLineAccess";
            public static string Win32_ComponentCategory = @"Win32_ComponentCategory";
            public static string Win32_ComputerSystem = @"Win32_ComputerSystem";
            public static string Win32_ComputerSystemProcessor = @"Win32_ComputerSystemProcessor";
            public static string Win32_ComputerSystemProduct = @"Win32_ComputerSystemProduct";
            public static string Win32_COMSetting = @"Win32_COMSetting";
            public static string Win32_Condition = @"Win32_Condition";
            public static string Win32_CreateFolderAction = @"Win32_CreateFolderAction";
            public static string Win32_CurrentProbe = @"Win32_CurrentProbe";
            public static string Win32_DCOMApplication = @"Win32_DCOMApplication";
            public static string Win32_DCOMApplicationAccessAllowedSetting = @"Win32_DCOMApplicationAccessAllowedSetting";
            public static string Win32_DCOMApplicationLaunchAllowedSetting = @"Win32_DCOMApplicationLaunchAllowedSetting";
            public static string Win32_DCOMApplicationSetting = @"Win32_DCOMApplicationSetting";
            public static string Win32_DependentService = @"Win32_DependentService";
            public static string Win32_Desktop = @"Win32_Desktop";
            public static string Win32_DesktopMonitor = @"Win32_DesktopMonitor";
            public static string Win32_DeviceBus = @"Win32_DeviceBus";
            public static string Win32_DeviceMemoryAddress = @"Win32_DeviceMemoryAddress";
            public static string Win32_DeviceSettings = @"Win32_DeviceSettings";
            public static string Win32_Directory = @"Win32_Directory";
            public static string Win32_DirectorySpecification = @"Win32_DirectorySpecification";
            public static string Win32_DiskDrive = @"Win32_DiskDrive";
            public static string Win32_DiskDriveToDiskPartition = @"Win32_DiskDriveToDiskPartition";
            public static string Win32_DiskPartition = @"Win32_DiskPartition";
            public static string Win32_DisplayConfiguration = @"Win32_DisplayConfiguration";
            public static string Win32_DisplayControllerConfiguration = @"Win32_DisplayControllerConfiguration";
            public static string Win32_DMAChannel = @"Win32_DMAChannel";
            public static string Win32_DriverVXD = @"Win32_DriverVXD";
            public static string Win32_DuplicateFileAction = @"Win32_DuplicateFileAction";
            public static string Win32_Environment = @"Win32_Environment";
            public static string Win32_EnvironmentSpecification = @"Win32_EnvironmentSpecification";
            public static string Win32_ExtensionInfoAction = @"Win32_ExtensionInfoAction";
            public static string Win32_Fan = @"Win32_Fan";
            public static string Win32_FileSpecification = @"Win32_FileSpecification";
            public static string Win32_FloppyController = @"Win32_FloppyController";
            public static string Win32_FloppyDrive = @"Win32_FloppyDrive";
            public static string Win32_FontInfoAction = @"Win32_FontInfoAction";
            public static string Win32_Group = @"Win32_Group";
            public static string Win32_GroupUser = @"Win32_GroupUser";
            public static string Win32_HeatPipe = @"Win32_HeatPipe";
            public static string Win32_IDEController = @"Win32_IDEController";
            public static string Win32_IDEControllerDevice = @"Win32_IDEControllerDevice";
            public static string Win32_ImplementedCategory = @"Win32_ImplementedCategory";
            public static string Win32_InfraredDevice = @"Win32_InfraredDevice";
            public static string Win32_IniFileSpecification = @"Win32_IniFileSpecification";
            public static string Win32_InstalledSoftwareElement = @"Win32_InstalledSoftwareElement";
            public static string Win32_IRQResource = @"Win32_IRQResource";
            public static string Win32_Keyboard = @"Win32_Keyboard";
            public static string Win32_LaunchCondition = @"Win32_LaunchCondition";
            public static string Win32_LoadOrderGroup = @"Win32_LoadOrderGroup";
            public static string Win32_LoadOrderGroupServiceDependencies = @"Win32_LoadOrderGroupServiceDependencies";
            public static string Win32_LoadOrderGroupServiceMembers = @"Win32_LoadOrderGroupServiceMembers";
            public static string Win32_LogicalDisk = @"Win32_LogicalDisk";
            public static string Win32_LogicalDiskRootDirectory = @"Win32_LogicalDiskRootDirectory";
            public static string Win32_LogicalDiskToPartition = @"Win32_LogicalDiskToPartition";
            public static string Win32_LogicalFileAccess = @"Win32_LogicalFileAccess";
            public static string Win32_LogicalFileAuditing = @"Win32_LogicalFileAuditing";
            public static string Win32_LogicalFileGroup = @"Win32_LogicalFileGroup";
            public static string Win32_LogicalFileOwner = @"Win32_LogicalFileOwner";
            public static string Win32_LogicalFileSecuritySetting = @"Win32_LogicalFileSecuritySetting";
            public static string Win32_LogicalMemoryConfiguration = @"Win32_LogicalMemoryConfiguration";
            public static string Win32_LogicalProgramGroup = @"Win32_LogicalProgramGroup";
            public static string Win32_LogicalProgramGroupDirectory = @"Win32_LogicalProgramGroupDirectory";
            public static string Win32_LogicalProgramGroupItem = @"Win32_LogicalProgramGroupItem";
            public static string Win32_LogicalProgramGroupItemDataFile = @"Win32_LogicalProgramGroupItemDataFile";
            public static string Win32_LogicalShareAccess = @"Win32_LogicalShareAccess";
            public static string Win32_LogicalShareAuditing = @"Win32_LogicalShareAuditing";
            public static string Win32_LogicalShareSecuritySetting = @"Win32_LogicalShareSecuritySetting";
            public static string Win32_ManagedSystemElementResource = @"Win32_ManagedSystemElementResource";
            public static string Win32_MemoryArray = @"Win32_MemoryArray";
            public static string Win32_MemoryArrayLocation = @"Win32_MemoryArrayLocation";
            public static string Win32_MemoryDevice = @"Win32_MemoryDevice";
            public static string Win32_MemoryDeviceArray = @"Win32_MemoryDeviceArray";
            public static string Win32_MemoryDeviceLocation = @"Win32_MemoryDeviceLocation";
            public static string Win32_MethodParameterClass = @"Win32_MethodParameterClass";
            public static string Win32_MIMEInfoAction = @"Win32_MIMEInfoAction";
            public static string Win32_MotherboardDevice = @"Win32_MotherboardDevice";
            public static string Win32_MoveFileAction = @"Win32_MoveFileAction";
            public static string Win32_MSIResource = @"Win32_MSIResource";
            public static string Win32_NetworkAdapter = @"Win32_NetworkAdapter";
            public static string Win32_NetworkAdapterConfiguration = @"Win32_NetworkAdapterConfiguration";
            public static string Win32_NetworkAdapterSetting = @"Win32_NetworkAdapterSetting";
            public static string Win32_NetworkClient = @"Win32_NetworkClient";
            public static string Win32_NetworkConnection = @"Win32_NetworkConnection";
            public static string Win32_NetworkLoginProfile = @"Win32_NetworkLoginProfile";
            public static string Win32_NetworkProtocol = @"Win32_NetworkProtocol";
            public static string Win32_NTEventlogFile = @"Win32_NTEventlogFile";
            public static string Win32_NTLogEvent = @"Win32_NTLogEvent";
            public static string Win32_NTLogEventComputer = @"Win32_NTLogEventComputer";
            public static string Win32_NTLogEventLog = @"Win32_NTLogEventLog";
            public static string Win32_NTLogEventUser = @"Win32_NTLogEventUser";
            public static string Win32_ODBCAttribute = @"Win32_ODBCAttribute";
            public static string Win32_ODBCDataSourceAttribute = @"Win32_ODBCDataSourceAttribute";
            public static string Win32_ODBCDataSourceSpecification = @"Win32_ODBCDataSourceSpecification";
            public static string Win32_ODBCDriverAttribute = @"Win32_ODBCDriverAttribute";
            public static string Win32_ODBCDriverSoftwareElement = @"Win32_ODBCDriverSoftwareElement";
            public static string Win32_ODBCDriverSpecification = @"Win32_ODBCDriverSpecification";
            public static string Win32_ODBCSourceAttribute = @"Win32_ODBCSourceAttribute";
            public static string Win32_ODBCTranslatorSpecification = @"Win32_ODBCTranslatorSpecification";
            public static string Win32_OnBoardDevice = @"Win32_OnBoardDevice";
            public static string Win32_OperatingSystem = @"Win32_OperatingSystem";
            public static string Win32_OperatingSystemQFE = @"Win32_OperatingSystemQFE";
            public static string Win32_OSRecoveryConfiguration = @"Win32_OSRecoveryConfiguration";
            public static string Win32_PageFile = @"Win32_PageFile";
            public static string Win32_PageFileElementSetting = @"Win32_PageFileElementSetting";
            public static string Win32_PageFileSetting = @"Win32_PageFileSetting";
            public static string Win32_PageFileUsage = @"Win32_PageFileUsage";
            public static string Win32_ParallelPort = @"Win32_ParallelPort";
            public static string Win32_Patch = @"Win32_Patch";
            public static string Win32_PatchFile = @"Win32_PatchFile";
            public static string Win32_PatchPackage = @"Win32_PatchPackage";
            public static string Win32_PCMCIAController = @"Win32_PCMCIAController";
            public static string Win32_Perf = @"Win32_Perf";
            public static string Win32_PerfRawData = @"Win32_PerfRawData";
            public static string Win32_PerfRawData_ASP_ActiveServerPages = @"Win32_PerfRawData_ASP_ActiveServerPages";
            public static string Win32_PerfRawData_ASPNET_114322_ASPNETAppsv114322 = @"Win32_PerfRawData_ASPNET_114322_ASPNETAppsv114322";
            public static string Win32_PerfRawData_ASPNET_114322_ASPNETv114322 = @"Win32_PerfRawData_ASPNET_114322_ASPNETv114322";
            public static string Win32_PerfRawData_ASPNET_ASPNET = @"Win32_PerfRawData_ASPNET_ASPNET";
            public static string Win32_PerfRawData_ASPNET_ASPNETApplications = @"Win32_PerfRawData_ASPNET_ASPNETApplications";
            public static string Win32_PerfRawData_IAS_IASAccountingClients = @"Win32_PerfRawData_IAS_IASAccountingClients";
            public static string Win32_PerfRawData_IAS_IASAccountingServer = @"Win32_PerfRawData_IAS_IASAccountingServer";
            public static string Win32_PerfRawData_IAS_IASAuthenticationClients = @"Win32_PerfRawData_IAS_IASAuthenticationClients";
            public static string Win32_PerfRawData_IAS_IASAuthenticationServer = @"Win32_PerfRawData_IAS_IASAuthenticationServer";
            public static string Win32_PerfRawData_InetInfo_InternetInformationServicesGlobal = @"Win32_PerfRawData_InetInfo_InternetInformationServicesGlobal";
            public static string Win32_PerfRawData_MSDTC_DistributedTransactionCoordinator = @"Win32_PerfRawData_MSDTC_DistributedTransactionCoordinator";
            public static string Win32_PerfRawData_MSFTPSVC_FTPService = @"Win32_PerfRawData_MSFTPSVC_FTPService";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerAccessMethods = @"Win32_PerfRawData_MSSQLSERVER_SQLServerAccessMethods";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerBackupDevice = @"Win32_PerfRawData_MSSQLSERVER_SQLServerBackupDevice";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerBufferManager = @"Win32_PerfRawData_MSSQLSERVER_SQLServerBufferManager";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerBufferPartition = @"Win32_PerfRawData_MSSQLSERVER_SQLServerBufferPartition";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerCacheManager = @"Win32_PerfRawData_MSSQLSERVER_SQLServerCacheManager";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerDatabases = @"Win32_PerfRawData_MSSQLSERVER_SQLServerDatabases";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerGeneralStatistics = @"Win32_PerfRawData_MSSQLSERVER_SQLServerGeneralStatistics";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerLatches = @"Win32_PerfRawData_MSSQLSERVER_SQLServerLatches";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerLocks = @"Win32_PerfRawData_MSSQLSERVER_SQLServerLocks";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerMemoryManager = @"Win32_PerfRawData_MSSQLSERVER_SQLServerMemoryManager";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationAgents = @"Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationAgents";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationDist = @"Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationDist";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationLogreader = @"Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationLogreader";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationMerge = @"Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationMerge";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationSnapshot = @"Win32_PerfRawData_MSSQLSERVER_SQLServerReplicationSnapshot";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerSQLStatistics = @"Win32_PerfRawData_MSSQLSERVER_SQLServerSQLStatistics";
            public static string Win32_PerfRawData_MSSQLSERVER_SQLServerUserSettable = @"Win32_PerfRawData_MSSQLSERVER_SQLServerUserSettable";
            public static string Win32_PerfRawData_NETFramework_NETCLRExceptions = @"Win32_PerfRawData_NETFramework_NETCLRExceptions";
            public static string Win32_PerfRawData_NETFramework_NETCLRInterop = @"Win32_PerfRawData_NETFramework_NETCLRInterop";
            public static string Win32_PerfRawData_NETFramework_NETCLRJit = @"Win32_PerfRawData_NETFramework_NETCLRJit";
            public static string Win32_PerfRawData_NETFramework_NETCLRLoading = @"Win32_PerfRawData_NETFramework_NETCLRLoading";
            public static string Win32_PerfRawData_NETFramework_NETCLRLocksAndThreads = @"Win32_PerfRawData_NETFramework_NETCLRLocksAndThreads";
            public static string Win32_PerfRawData_NETFramework_NETCLRMemory = @"Win32_PerfRawData_NETFramework_NETCLRMemory";
            public static string Win32_PerfRawData_NETFramework_NETCLRRemoting = @"Win32_PerfRawData_NETFramework_NETCLRRemoting";
            public static string Win32_PerfRawData_NETFramework_NETCLRSecurity = @"Win32_PerfRawData_NETFramework_NETCLRSecurity";
            public static string Win32_PerfRawData_Outlook_Outlook = @"Win32_PerfRawData_Outlook_Outlook";
            public static string Win32_PerfRawData_PerfDisk_PhysicalDisk = @"Win32_PerfRawData_PerfDisk_PhysicalDisk";
            public static string Win32_PerfRawData_PerfNet_Browser = @"Win32_PerfRawData_PerfNet_Browser";
            public static string Win32_PerfRawData_PerfNet_Redirector = @"Win32_PerfRawData_PerfNet_Redirector";
            public static string Win32_PerfRawData_PerfNet_Server = @"Win32_PerfRawData_PerfNet_Server";
            public static string Win32_PerfRawData_PerfNet_ServerWorkQueues = @"Win32_PerfRawData_PerfNet_ServerWorkQueues";
            public static string Win32_PerfRawData_PerfOS_Cache = @"Win32_PerfRawData_PerfOS_Cache";
            public static string Win32_PerfRawData_PerfOS_Memory = @"Win32_PerfRawData_PerfOS_Memory";
            public static string Win32_PerfRawData_PerfOS_Objects = @"Win32_PerfRawData_PerfOS_Objects";
            public static string Win32_PerfRawData_PerfOS_PagingFile = @"Win32_PerfRawData_PerfOS_PagingFile";
            public static string Win32_PerfRawData_PerfOS_Processor = @"Win32_PerfRawData_PerfOS_Processor";
            public static string Win32_PerfRawData_PerfOS_System = @"Win32_PerfRawData_PerfOS_System";
            public static string Win32_PerfRawData_PerfProc_FullImage_Costly = @"Win32_PerfRawData_PerfProc_FullImage_Costly";
            public static string Win32_PerfRawData_PerfProc_Image_Costly = @"Win32_PerfRawData_PerfProc_Image_Costly";
            public static string Win32_PerfRawData_PerfProc_JobObject = @"Win32_PerfRawData_PerfProc_JobObject";
            public static string Win32_PerfRawData_PerfProc_JobObjectDetails = @"Win32_PerfRawData_PerfProc_JobObjectDetails";
            public static string Win32_PerfRawData_PerfProc_Process = @"Win32_PerfRawData_PerfProc_Process";
            public static string Win32_PerfRawData_PerfProc_ProcessAddressSpace_Costly = @"Win32_PerfRawData_PerfProc_ProcessAddressSpace_Costly";
            public static string Win32_PerfRawData_PerfProc_Thread = @"Win32_PerfRawData_PerfProc_Thread";
            public static string Win32_PerfRawData_PerfProc_ThreadDetails_Costly = @"Win32_PerfRawData_PerfProc_ThreadDetails_Costly";
            public static string Win32_PerfRawData_RemoteAccess_RASPort = @"Win32_PerfRawData_RemoteAccess_RASPort";
            public static string Win32_PerfRawData_RemoteAccess_RASTotal = @"Win32_PerfRawData_RemoteAccess_RASTotal";
            public static string Win32_PerfRawData_RSVP_ACSPerRSVPService = @"Win32_PerfRawData_RSVP_ACSPerRSVPService";
            public static string Win32_PerfRawData_Spooler_PrintQueue = @"Win32_PerfRawData_Spooler_PrintQueue";
            public static string Win32_PerfRawData_TapiSrv_Telephony = @"Win32_PerfRawData_TapiSrv_Telephony";
            public static string Win32_PerfRawData_Tcpip_ICMP = @"Win32_PerfRawData_Tcpip_ICMP";
            public static string Win32_PerfRawData_Tcpip_IP = @"Win32_PerfRawData_Tcpip_IP";
            public static string Win32_PerfRawData_Tcpip_NBTConnection = @"Win32_PerfRawData_Tcpip_NBTConnection";
            public static string Win32_PerfRawData_Tcpip_NetworkInterface = @"Win32_PerfRawData_Tcpip_NetworkInterface";
            public static string Win32_PerfRawData_Tcpip_TCP = @"Win32_PerfRawData_Tcpip_TCP";
            public static string Win32_PerfRawData_Tcpip_UDP = @"Win32_PerfRawData_Tcpip_UDP";
            public static string Win32_PerfRawData_W3SVC_WebService = @"Win32_PerfRawData_W3SVC_WebService";
            public static string Win32_PhysicalMemory = @"Win32_PhysicalMemory";
            public static string Win32_PhysicalMemoryArray = @"Win32_PhysicalMemoryArray";
            public static string Win32_PhysicalMemoryLocation = @"Win32_PhysicalMemoryLocation";
            public static string Win32_PNPAllocatedResource = @"Win32_PNPAllocatedResource";
            public static string Win32_PnPDevice = @"Win32_PnPDevice";
            public static string Win32_PnPEntity = @"Win32_PnPEntity";
            public static string Win32_PointingDevice = @"Win32_PointingDevice";
            public static string Win32_PortableBattery = @"Win32_PortableBattery";
            public static string Win32_PortConnector = @"Win32_PortConnector";
            public static string Win32_PortResource = @"Win32_PortResource";
            public static string Win32_POTSModem = @"Win32_POTSModem";
            public static string Win32_POTSModemToSerialPort = @"Win32_POTSModemToSerialPort";
            public static string Win32_PowerManagementEvent = @"Win32_PowerManagementEvent";
            public static string Win32_Printer = @"Win32_Printer";
            public static string Win32_PrinterConfiguration = @"Win32_PrinterConfiguration";
            public static string Win32_PrinterController = @"Win32_PrinterController";
            public static string Win32_PrinterDriverDll = @"Win32_PrinterDriverDll";
            public static string Win32_PrinterSetting = @"Win32_PrinterSetting";
            public static string Win32_PrinterShare = @"Win32_PrinterShare";
            public static string Win32_PrintJob = @"Win32_PrintJob";
            public static string Win32_PrivilegesStatus = @"Win32_PrivilegesStatus";
            public static string Win32_Process = @"Win32_Process";
            public static string Win32_Processor = @"Win32_Processor";
            public static string Win32_ProcessStartup = @"Win32_ProcessStartup";
            public static string Win32_Product = @"Win32_Product";
            public static string Win32_ProductCheck = @"Win32_ProductCheck";
            public static string Win32_ProductResource = @"Win32_ProductResource";
            public static string Win32_ProductSoftwareFeatures = @"Win32_ProductSoftwareFeatures";
            public static string Win32_ProgIDSpecification = @"Win32_ProgIDSpecification";
            public static string Win32_ProgramGroup = @"Win32_ProgramGroup";
            public static string Win32_ProgramGroupContents = @"Win32_ProgramGroupContents";
            public static string Win32_ProgramGroupOrItem = @"Win32_ProgramGroupOrItem";
            public static string Win32_Property = @"Win32_Property";
            public static string Win32_ProtocolBinding = @"Win32_ProtocolBinding";
            public static string Win32_PublishComponentAction = @"Win32_PublishComponentAction";
            public static string Win32_QuickFixEngineering = @"Win32_QuickFixEngineering";
            public static string Win32_Refrigeration = @"Win32_Refrigeration";
            public static string Win32_Registry = @"Win32_Registry";
            public static string Win32_RegistryAction = @"Win32_RegistryAction";
            public static string Win32_RemoveFileAction = @"Win32_RemoveFileAction";
            public static string Win32_RemoveIniAction = @"Win32_RemoveIniAction";
            public static string Win32_ReserveCost = @"Win32_ReserveCost";
            public static string Win32_ScheduledJob = @"Win32_ScheduledJob";
            public static string Win32_SCSIController = @"Win32_SCSIController";
            public static string Win32_SCSIControllerDevice = @"Win32_SCSIControllerDevice";
            public static string Win32_SecurityDescriptor = @"Win32_SecurityDescriptor";
            public static string Win32_SecuritySetting = @"Win32_SecuritySetting";
            public static string Win32_SecuritySettingAccess = @"Win32_SecuritySettingAccess";
            public static string Win32_SecuritySettingAuditing = @"Win32_SecuritySettingAuditing";
            public static string Win32_SecuritySettingGroup = @"Win32_SecuritySettingGroup";
            public static string Win32_SecuritySettingOfLogicalFile = @"Win32_SecuritySettingOfLogicalFile";
            public static string Win32_SecuritySettingOfLogicalShare = @"Win32_SecuritySettingOfLogicalShare";
            public static string Win32_SecuritySettingOfObject = @"Win32_SecuritySettingOfObject";
            public static string Win32_SecuritySettingOwner = @"Win32_SecuritySettingOwner";
            public static string Win32_SelfRegModuleAction = @"Win32_SelfRegModuleAction";
            public static string Win32_SerialPort = @"Win32_SerialPort";
            public static string Win32_SerialPortConfiguration = @"Win32_SerialPortConfiguration";
            public static string Win32_SerialPortSetting = @"Win32_SerialPortSetting";
            public static string Win32_Service = @"Win32_Service";
            public static string Win32_ServiceControl = @"Win32_ServiceControl";
            public static string Win32_ServiceSpecification = @"Win32_ServiceSpecification";
            public static string Win32_ServiceSpecificationService = @"Win32_ServiceSpecificationService";
            public static string Win32_SettingCheck = @"Win32_SettingCheck";
            public static string Win32_Share = @"Win32_Share";
            public static string Win32_ShareToDirectory = @"Win32_ShareToDirectory";
            public static string Win32_ShortcutAction = @"Win32_ShortcutAction";
            public static string Win32_ShortcutFile = @"Win32_ShortcutFile";
            public static string Win32_ShortcutSAP = @"Win32_ShortcutSAP";
            public static string Win32_SID = @"Win32_SID";
            public static string Win32_SMBIOSMemory = @"Win32_SMBIOSMemory";
            public static string Win32_SoftwareElement = @"Win32_SoftwareElement";
            public static string Win32_SoftwareElementAction = @"Win32_SoftwareElementAction";
            public static string Win32_SoftwareElementCheck = @"Win32_SoftwareElementCheck";
            public static string Win32_SoftwareElementCondition = @"Win32_SoftwareElementCondition";
            public static string Win32_SoftwareElementResource = @"Win32_SoftwareElementResource";
            public static string Win32_SoftwareFeature = @"Win32_SoftwareFeature";
            public static string Win32_SoftwareFeatureAction = @"Win32_SoftwareFeatureAction";
            public static string Win32_SoftwareFeatureCheck = @"Win32_SoftwareFeatureCheck";
            public static string Win32_SoftwareFeatureParent = @"Win32_SoftwareFeatureParent";
            public static string Win32_SoftwareFeatureSoftwareElements = @"Win32_SoftwareFeatureSoftwareElements";
            public static string Win32_SoundDevice = @"Win32_SoundDevice";
            public static string Win32_StartupCommand = @"Win32_StartupCommand";
            public static string Win32_SubDirectory = @"Win32_SubDirectory";
            public static string Win32_SystemAccount = @"Win32_SystemAccount";
            public static string Win32_SystemBIOS = @"Win32_SystemBIOS";
            public static string Win32_SystemBootConfiguration = @"Win32_SystemBootConfiguration";
            public static string Win32_SystemDesktop = @"Win32_SystemDesktop";
            public static string Win32_SystemDevices = @"Win32_SystemDevices";
            public static string Win32_SystemDriver = @"Win32_SystemDriver";
            public static string Win32_SystemDriverPNPEntity = @"Win32_SystemDriverPNPEntity";
            public static string Win32_SystemEnclosure = @"Win32_SystemEnclosure";
            public static string Win32_SystemLoadOrderGroups = @"Win32_SystemLoadOrderGroups";
            public static string Win32_SystemLogicalMemoryConfiguration = @"Win32_SystemLogicalMemoryConfiguration";
            public static string Win32_SystemMemoryResource = @"Win32_SystemMemoryResource";
            public static string Win32_SystemNetworkConnections = @"Win32_SystemNetworkConnections";
            public static string Win32_SystemOperatingSystem = @"Win32_SystemOperatingSystem";
            public static string Win32_SystemPartitions = @"Win32_SystemPartitions";
            public static string Win32_SystemProcesses = @"Win32_SystemProcesses";
            public static string Win32_SystemProgramGroups = @"Win32_SystemProgramGroups";
            public static string Win32_SystemResources = @"Win32_SystemResources";
            public static string Win32_SystemServices = @"Win32_SystemServices";
            public static string Win32_SystemSetting = @"Win32_SystemSetting";
            public static string Win32_SystemSlot = @"Win32_SystemSlot";
            public static string Win32_SystemSystemDriver = @"Win32_SystemSystemDriver";
            public static string Win32_SystemTimeZone = @"Win32_SystemTimeZone";
            public static string Win32_SystemUsers = @"Win32_SystemUsers";
            public static string Win32_TapeDrive = @"Win32_TapeDrive";
            public static string Win32_TemperatureProbe = @"Win32_TemperatureProbe";
            public static string Win32_Thread = @"Win32_Thread";
            public static string Win32_TimeZone = @"Win32_TimeZone";
            public static string Win32_Trustee = @"Win32_Trustee";
            public static string Win32_TypeLibraryAction = @"Win32_TypeLibraryAction";
            public static string Win32_UninterruptiblePowerSupply = @"Win32_UninterruptiblePowerSupply";
            public static string Win32_USBController = @"Win32_USBController";
            public static string Win32_USBControllerDevice = @"Win32_USBControllerDevice";
            public static string Win32_UserAccount = @"Win32_UserAccount";
            public static string Win32_UserDesktop = @"Win32_UserDesktop";
            public static string Win32_VideoConfiguration = @"Win32_VideoConfiguration";
            public static string Win32_VideoController = @"Win32_VideoController";
            public static string Win32_VideoSettings = @"Win32_VideoSettings";
            public static string Win32_VoltageProbe = @"Win32_VoltageProbe";
            public static string Win32_WMIElementSetting = @"Win32_WMIElementSetting";
            public static string Win32_WMISetting = @"Win32_WMISetting";
        }

        public static class Processor
        {
            public static ManagementBaseObject DefaultProcessorData
            {
                get
                {
                    var processorData = GetManagementObjects(Keys.Win32_Processor);
                    return processorData != null ? processorData.FirstOrDefault() : null;
                }
            }

            public static int Architecture
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["Architecture"]);
                    }

                    return -1;
                }
            }

            public static int Availability
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["Availability"]);
                    }

                    return -1;
                }
            }

            public static string Caption
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["Caption"]);
                    }

                    return string.Empty;
                }
            }

            public static int CurrentClockSpeed
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["CurrentClockSpeed"]);
                    }

                    return -1;
                }
            }

            public static int CurrentVoltage
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["CurrentVoltage"]);
                    }

                    return -1;
                }
            }

            public static string Description
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["Description"]);
                    }

                    return string.Empty;
                }
            }

            public static string DeviceID
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["DeviceID"]);
                    }

                    return string.Empty;
                }
            }

            public static int ExtClock
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["ExtClock"]);
                    }

                    return -1;
                }
            }

            public static int Family
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["Family"]);
                    }

                    return -1;
                }
            }

            public static DateTime InstallDate
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        var dateTime = Convert.ToDateTime(DefaultProcessorData["InstallDate"]);
                        if (dateTime.Year == 1 && dateTime.Month == 1 && dateTime.Day == 1)
                        {
                            return DateTime.MinValue;
                        }

                        return dateTime;
                    }

                    return DateTime.MinValue;
                }
            }

            public static int L2CacheSize
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["L2CacheSize"]);
                    }

                    return -1;
                }
            }

            public static int L2CacheSpeed
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["L2CacheSpeed"]);
                    }

                    return -1;
                }
            }

            public static int L3CacheSize
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["L3CacheSize"]);
                    }

                    return -1;
                }
            }

            public static int L3CacheSpeed
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["L3CacheSpeed"]);
                    }

                    return -1;
                }
            }

            public static int Level
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["Level"]);
                    }

                    return -1;
                }
            }

            public static int LoadPercentage
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["LoadPercentage"]);
                    }

                    return -1;
                }
            }

            public static string Manufacturer
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["Manufacturer"]);
                    }

                    return string.Empty;
                }
            }

            public static int MaxClockSpeed
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["MaxClockSpeed"]);
                    }

                    return -1;
                }
            }

            public static string Name
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["Name"]);
                    }

                    return string.Empty;
                }
            }

            public static int NumberOfCores
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["NumberOfCores"]);
                    }

                    return -1;
                }
            }

            public static int NumberOfEnabledCore
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["NumberOfEnabledCore"]);
                    }

                    return -1;
                }
            }

            public static int NumberOfLogicalProcessors
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["NumberOfLogicalProcessors"]);
                    }

                    return -1;
                }
            }

            public static string ProcessorId
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["ProcessorId"]);
                    }

                    return string.Empty;
                }
            }

            public static int ProcessorType
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["ProcessorType"]);
                    }

                    return -1;
                }
            }

            public static string SerialNumber
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["SerialNumber"]);
                    }

                    return string.Empty;
                }
            }

            public static string Status
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["Status"]);
                    }

                    return string.Empty;
                }
            }

            public static int StatusInfo
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt16(DefaultProcessorData["StatusInfo"]);
                    }

                    return -1;
                }
            }

            public static string Stepping
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToString(DefaultProcessorData["Stepping"]);
                    }

                    return string.Empty;
                }
            }

            public static int ThreadCount
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["ThreadCount"]);
                    }

                    return -1;
                }
            }

            public static int VoltageCaps
            {
                get
                {
                    if (DefaultProcessorData != null)
                    {
                        return Convert.ToInt32(DefaultProcessorData["VoltageCaps"]);
                    }

                    return -1;
                }
            }
        }
    }
}