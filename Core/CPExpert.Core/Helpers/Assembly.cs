﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    ///
    /// </summary>
    public static class Assembly
    {
        private static T GetAttribute<T>(System.Reflection.Assembly assembly) where T : Attribute
        {
            try
            {
                if (assembly != null)
                {
                    var attributes = assembly.GetCustomAttributes(true);
                    if (attributes.Length > 0)
                    {
                        var myAttribute = attributes.FirstOrDefault(x => x is T);

                        if (myAttribute != null)
                        {
                            return (T)myAttribute;
                        }
                    }
                }

                return null;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetGuid()
        {
            try
            {
                var assemblyInfo = GetAttribute<GuidAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Value : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetTitle()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyTitleAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Title : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetDescription()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyDescriptionAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Description : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetConfiguration()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyConfigurationAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Configuration : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetCompany()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyCompanyAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Company : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetProduct()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyProductAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Product : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetCopyright()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyCopyrightAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Copyright : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetTrademark()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyTrademarkAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Trademark : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetCulture()
        {
            try
            {
                var assembly = System.Reflection.Assembly.GetCallingAssembly();
                var cultureInfo = assembly.GetName().CultureInfo;
                return cultureInfo != null ? cultureInfo.Name : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetVersion()
        {
            try
            {
                var assembly = System.Reflection.Assembly.GetCallingAssembly();
                var version = assembly.GetName().Version;
                return version != null ? version.ToString() : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetFileVersion()
        {
            try
            {
                var assemblyInfo = GetAttribute<AssemblyFileVersionAttribute>(System.Reflection.Assembly.GetCallingAssembly());
                return assemblyInfo != null ? assemblyInfo.Version : string.Empty;
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static string GetAssemblyWorkingPath()
        {
            try
            {
                var uri = new UriBuilder(System.Reflection.Assembly.GetCallingAssembly().CodeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
            catch (System.Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}