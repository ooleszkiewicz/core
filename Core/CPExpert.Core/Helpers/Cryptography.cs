﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Polenter.Serialization;

namespace CPExpert.Core.Helpers
{
    public static class Cryptography
    {
        public static class HashCheckSum
        {
            /// <summary>
            /// Calculate a checksum for given object
            /// </summary>
            /// <param name="obj">object to calculate checksum</param>
            /// <returns>Calculated checksum</returns>
            public static string Calculate(object obj)
            {
                using (var md5 = MD5.Create())
                {
                    var serializer = new SharpSerializer();
                    using (var stream = new MemoryStream())
                    {
                        serializer.Serialize(obj, stream);
                        stream.Position = 0;
                        var hashArray = md5.ComputeHash(stream);
                        var sb = new StringBuilder();
                        foreach (var item in hashArray)
                        {
                            sb.Append(item.ToString("X2"));
                        }
                        return sb.ToString();
                    }
                }
            }
        }

        public static class Md5Hash
        {
            /// <summary>
            /// Encrypt a string
            /// SZYFROWANIE
            /// </summary>
            /// <param name="input">string to encrypt</param>
            /// <param name="key">key using to encrypt string</param>
            /// <returns>encrypted string</returns>
            public static string Encoder(string input, string key)
            {
                try
                {
                    var toEncryptArray = Encoding.UTF8.GetBytes(input);
                    var hashmd5 = new MD5CryptoServiceProvider();
                    var keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();

                    var tdes = new TripleDESCryptoServiceProvider
                    {
                        Key = keyArray,
                        Mode = CipherMode.ECB,
                        Padding = PaddingMode.PKCS7
                    };

                    var cTransform = tdes.CreateEncryptor();
                    var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                    tdes.Clear();
                    return Convert.ToBase64String(resultArray, 0, resultArray.Length);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }

            /// <summary>
            /// Decrypt a string
            /// ROZSZYFROWYWANIE
            /// </summary>
            /// <param name="input">string to decrypt</param>
            /// <param name="key">key used to encrypt string</param>
            /// <returns>decrypted string</returns>
            public static string Decoder(string input, string key)
            {
                try
                {
                    if (!string.IsNullOrEmpty(input))
                    {
                        var toEncryptArray = Convert.FromBase64String(input);
                        var hashmd5 = new MD5CryptoServiceProvider();
                        var keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                        hashmd5.Clear();

                        var tdes = new TripleDESCryptoServiceProvider
                        {
                            Key = keyArray,
                            Mode = CipherMode.ECB,
                            Padding = PaddingMode.PKCS7
                        };

                        var cTransform = tdes.CreateDecryptor();
                        var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                        tdes.Clear();
                        return Encoding.UTF8.GetString(resultArray);
                    }

                    return string.Empty;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}