﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class Network
    {
        public static IPAddress GetActiveIpAddress()
        {
            try
            {
                using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
                {
                    socket.Connect("216.58.214.35", 65530);
                    var endPoint = socket.LocalEndPoint as IPEndPoint;
                    if (endPoint != null)
                    {
                        return endPoint.Address;
                    }
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }

            return null;
        }

        public static bool IsInternetConnectionActive(string hostAddress = @"http://www.google.com")
        {
            try
            {
                var url = new Uri(hostAddress);
                var pingUrl = String.Format("{0}", url.Host);
                var result = false;
                var p = new Ping();
                try
                {
                    var reply = p.Send(pingUrl);
                    if (reply != null && reply.Status == IPStatus.Success)
                    {
                        result = true;
                    }
                }
                catch
                {
                    result = false;
                }

                return result;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}