﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using CPExpert.Core.Enums.DataBase;

namespace CPExpert.Core.Helpers.DataBases
{
    /// <summary>
    ///
    /// </summary>
    public static class MsSql
    {
        public static string CreateConnectionString(string server, string database, string user, string password, bool isWindowsAuthentication)
        {
            if (isWindowsAuthentication)
            {
                return
                    string.Format(
                        "Data Source={0};Initial Catalog={1};Persist Security Info=True;Integrated Security=True;MultipleActiveResultSets=True",
                        server, database);
            }

            return
                string.Format(
                    "Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};MultipleActiveResultSets=True",
                    server, database, user, password);
        }

        public static bool IsServerValidConnection(SqlConnection sqlConnection)
        {
            try
            {
                if (sqlConnection != null && !string.IsNullOrEmpty(sqlConnection.ConnectionString))
                {
                    sqlConnection.ConnectionString = ChangeDataBase(sqlConnection.ConnectionString, "master");
                    if (sqlConnection.Database.Equals("master", StringComparison.CurrentCultureIgnoreCase))
                    {
                        sqlConnection.Open();
                        return !string.IsNullOrEmpty(sqlConnection.ServerVersion);
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                return false;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public static bool IsServerValidConnection(string connectionString)
        {
            try
            {
                if (!string.IsNullOrEmpty(connectionString))
                {
                    var sqlConnection = new SqlConnection(connectionString);
                    return IsServerValidConnection(sqlConnection);
                }

                return false;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                return false;
            }
        }

        public static bool IsDataBaseValidConnection(SqlConnection sqlConnection)
        {
            try
            {
                if (sqlConnection != null && !string.IsNullOrEmpty(sqlConnection.ConnectionString))
                {
                    sqlConnection.Open();
                    return !string.IsNullOrEmpty(sqlConnection.ServerVersion);
                }

                return false;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                return false;
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                }
            }
        }

        public static bool IsDataBaseValidConnection(string connectionString)
        {
            try
            {
                if (!string.IsNullOrEmpty(connectionString))
                {
                    var sqlConnection = new SqlConnection(connectionString);
                    return IsDataBaseValidConnection(sqlConnection);
                }

                return false;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                return false;
            }
        }

        public static bool CreateUserAndSetDefaultDbAndRole(SqlConnection sqlConnectionAdmin, string userLogin, string userPassword, string defaultDataBase, DbMsSqlRoles dbUserRole)
        {
            try
            {
                if (sqlConnectionAdmin != null && !string.IsNullOrEmpty(sqlConnectionAdmin.ConnectionString) &&
                    !string.IsNullOrEmpty(userLogin) && !string.IsNullOrEmpty(userLogin) &&
                    !string.IsNullOrEmpty(defaultDataBase))
                {
                    var sqlScript = string.Format(@"
                        use [master];
                        CREATE LOGIN {0} WITH PASSWORD = N'{1}', CHECK_POLICY = OFF, CHECK_EXPIRATION = OFF;
                        alter login [{0}] with default_database = [{2}];
                        create user [{0}] from login [{0}];
                        exec sp_addrolemember '{3}', '{0}';
                        alter authorization on database::[{2}] to [{0}];
                        ", userLogin, userPassword, defaultDataBase, dbUserRole);

                    if (IsServerValidConnection(sqlConnectionAdmin))
                    {
                        var connectionStringToSettingUserDb = ChangeDataBase(sqlConnectionAdmin.ConnectionString,
                            defaultDataBase);
                        if (IsDataBaseValidConnection(connectionStringToSettingUserDb))
                        {
                            if (!IsUserExistOnSqlServer(sqlConnectionAdmin, userLogin))
                            {
                                var command = new SqlCommand();
                                command.CommandText = sqlScript;
                                command.CommandType = CommandType.Text;
                                command.Connection = sqlConnectionAdmin;
                                sqlConnectionAdmin.Open();
                                var reader = command.ExecuteNonQuery();
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
            finally
            {
                if (sqlConnectionAdmin != null)
                {
                    sqlConnectionAdmin.Close();
                }
            }
        }

        public static bool IsUserExistOnSqlServer(SqlConnection sqlConnectionAdmin, string userNameToBeChecked)
        {
            try
            {
                if (IsServerValidConnection(sqlConnectionAdmin) && !string.IsNullOrEmpty(userNameToBeChecked))
                {
                    var command = new SqlCommand();
                    var sqlScript = string.Format(@"select name from master.sys.sql_logins where name = '{0}'",
                        userNameToBeChecked);
                    command.CommandText = sqlScript;
                    command.CommandType = CommandType.Text;
                    command.Connection = sqlConnectionAdmin;
                    sqlConnectionAdmin.Open();
                    var dataReader = command.ExecuteReader();
                    return dataReader.HasRows;
                }

                return false;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
            finally
            {
                if (sqlConnectionAdmin != null)
                {
                    sqlConnectionAdmin.Close();
                }
            }
        }

        public static string ChangeDataBase(string connectionString, string newDbName)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                connectionString = connectionString.ToLower();
                var elements = connectionString.Split(';').ToList();
                if (elements.Any())
                {
                    var newConnectionString = new StringBuilder();
                    foreach (var element in elements)
                    {
                        newConnectionString.Append(element.StartsWith("initial catalog",
                            StringComparison.CurrentCultureIgnoreCase)
                            ? string.Format("Initial Catalog={0};", newDbName)
                            : string.Format("{0};", element));
                    }

                    return newConnectionString.ToString();
                }
            }

            return connectionString;
        }
    }
}