﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using CPExpert.Core.Enums;

namespace CPExpert.Core.Helpers.Internationalization
{
    public static class Translation
    {
        public static LanguagesCodes DefaultLanguage { get; private set; }

        private static Dictionary<string, IList<XElement>> nodes;

        public static void Set(IList<string> translationDirectoryPaths, LanguagesCodes defaultLanguage)
        {
            if (translationDirectoryPaths != null && translationDirectoryPaths.Any() &&
                defaultLanguage != LanguagesCodes.Undefined)
            {
                DefaultLanguage = defaultLanguage;
                nodes = new Dictionary<string, IList<XElement>>();

                foreach (var translationDirectoryPath in translationDirectoryPaths)
                {                    
                    foreach (var filePath in Directory.EnumerateFiles(translationDirectoryPath, "*.xml", SearchOption.AllDirectories))
                    {
                        var Doc = XDocument.Load(filePath);
                        var element = Doc.Descendants("Language").FirstOrDefault();
                        if (element != null)
                        {
                            var xAttribute = element.Attribute("Name");
                            if (xAttribute == null) continue;
                            var code = xAttribute.Value;
                            var elements = Doc.Descendants("element").ToList();

                            if (elements.Any())
                            {
                                if (!nodes.ContainsKey(code))
                                {
                                    nodes.Add(code, elements);
                                }
                                else
                                {
                                    foreach (XElement xElement in elements)
                                    {
                                        if (!nodes[code].Any(x => x.FirstAttribute.Value.Equals(xElement.FirstAttribute.Value)))
                                        {
                                            nodes[code].Add(xElement);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void Set(string translationDirectoryPath, LanguagesCodes defaultLanguage)
        {
            var paths = new List<string> { translationDirectoryPath };
            Set(paths, defaultLanguage);
        }

        public static void Set(LanguagesCodes defaultLanguage)
        {
            var currentExecuteAssemblyPath = Assembly.GetAssemblyWorkingPath();
            if (!string.IsNullOrEmpty(currentExecuteAssemblyPath) && Directory.Exists(currentExecuteAssemblyPath))
            {
                var languageFilesDirectory = Path.Combine(currentExecuteAssemblyPath, "Language");
                if (!string.IsNullOrEmpty(languageFilesDirectory) && Directory.Exists(languageFilesDirectory))
                {
                    var paths = new List<string> {languageFilesDirectory};
                    Set(paths, defaultLanguage);
                }
            }
        }

        private static string search(string elementName, string language)
        {
            if (nodes != null && !string.IsNullOrEmpty(language) && nodes.ContainsKey(language))
            {
                var query = from xe in nodes[language]
                            let xAttribute = xe.Attribute("name")
                            where xAttribute != null && xAttribute.Value == elementName
                            select xe;

                IEnumerable<XElement> elements = query as XElement[] ?? query.ToArray();

                if (elements.Any())
                {
                    XElement element = elements.FirstOrDefault();
                    if (element != null) return element.Value;
                }
            }

            return string.Empty;
        }

        public static string Translate(string key)
        {
            return Translate(key, CultureInfo.InstalledUICulture.Name);
        }

        public static string Translate(string key, LanguagesCodes language)
        {
            var languageCode = Enum<LanguagesCodes>.GetDescription(language);
            return Translate(key, languageCode);
        }

        public static string Translate(string key, string languageCultureInfoName)
        {
            try
            {
                var translateVerb = search(key, languageCultureInfoName);
                if (!string.IsNullOrEmpty(translateVerb))
                {
                    return translateVerb;
                }

                //try search default language
                translateVerb = search(key, Enum<LanguagesCodes>.GetDescription(DefaultLanguage));

                if (!string.IsNullOrEmpty(translateVerb))
                {
                    return translateVerb;
                }

                throw new Exception(string.Format("Can't found translate text for key: {0}", key));
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
            }

            return string.Empty;
        }
    }
}