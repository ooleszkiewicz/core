﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Polenter.Serialization;

namespace CPExpert.Core.Helpers
{
    /// <summary>
    /// Xml operation extension
    /// </summary>
    public class Xml
    {
        private static readonly SharpSerializerXmlSettings _settings = new SharpSerializerXmlSettings
        {
            IncludeAssemblyVersionInTypeName = true,
            IncludeCultureInTypeName = true,
            IncludePublicKeyTokenInTypeName = true,
            Encoding = Encoding.Default
        };

        public static string Serialize(object obj, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                SharpSerializer serializer;
                if (settings != null)
                {
                    serializer = new SharpSerializer(settings);
                }
                else
                {
                    serializer = new SharpSerializer(_settings);
                }

                serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));
                var stream = new MemoryStream();
                serializer.Serialize(obj, stream);
                var r = new StreamReader(stream);
                r.BaseStream.Seek(0, SeekOrigin.Begin);
                return r.ReadToEnd();
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static void SerializeToFile(object obj, string filePath, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                var directoryPath = Path.GetDirectoryName(filePath);

                if (!string.IsNullOrEmpty(directoryPath))
                {
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    Diagnostic.LogDebug(string.Format("Serialize to file: {0}", filePath));
                    SharpSerializer serializer;
                    if (settings != null)
                    {
                        serializer = new SharpSerializer(settings);
                    }
                    else
                    {
                        serializer = new SharpSerializer(_settings);
                    }
                    serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));
                    serializer.Serialize(obj, filePath);
                }
                else
                {
                    throw new Exception("Serialize directory path error!");
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static object Deserialize(string xml, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                object obj;

                SharpSerializer serializer;
                if (settings != null)
                {
                    serializer = new SharpSerializer(settings);

                    serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));

                    using (var stream = new MemoryStream(settings.Encoding.GetBytes(xml)))
                    {
                        obj = serializer.Deserialize(stream);
                    }

                    return obj;
                }

                serializer = new SharpSerializer(_settings);

                serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));

                using (var stream = new MemoryStream(_settings.Encoding.GetBytes(xml)))
                {
                    obj = serializer.Deserialize(stream);
                }

                return obj;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }

        public static object DeserializeFromFile(string filePath, SharpSerializerXmlSettings settings = null)
        {
            try
            {
                var directoryPath = Path.GetDirectoryName(filePath);

                if (!string.IsNullOrEmpty(directoryPath))
                {
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    if (System.IO.File.Exists(filePath))
                    {
                        Diagnostic.LogDebug(string.Format("Deserialize from file: {0}", filePath));

                        SharpSerializer serializer;
                        if (settings != null)
                        {
                            serializer = new SharpSerializer(settings);
                        }
                        else
                        {
                            serializer = new SharpSerializer(_settings);
                        }

                        serializer.PropertyProvider.AttributesToIgnore.Add(typeof(XmlIgnoreAttribute));
                        return serializer.Deserialize(filePath);
                    }

                    Diagnostic.LogDebug("Deserialize file not found!");
                }
                else
                {
                    Diagnostic.LogDebug("Deserialize directory path error!");
                }

                return null;
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}