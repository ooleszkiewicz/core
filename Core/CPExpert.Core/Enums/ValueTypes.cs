﻿namespace CPExpert.Core.Enums
{
    public enum ValueTypes
    {
        /// <summary>
        /// 
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Value is integer type
        /// </summary>
        Integer = 10,

        /// <summary>
        /// Value is double type
        /// </summary>
        Double = 20,

        /// <summary>
        /// Value is string type
        /// </summary>
        String = 30,

        /// <summary>
        /// Value is boolean type
        /// </summary>
        Boolean = 40,

        /// <summary>
        /// Value is xml type
        /// </summary>
        Xml = 50
    }
}