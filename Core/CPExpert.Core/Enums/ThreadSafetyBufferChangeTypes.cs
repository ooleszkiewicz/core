﻿namespace CPExpert.Core.Enums
{
    public enum ThreadSafetyBufferChangeTypes
    {
        Undefined = 0,
        Added = 10,
        Removed = 20,
        Updated = 30,
    }
}