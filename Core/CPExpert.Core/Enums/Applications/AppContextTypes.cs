﻿namespace CPExpert.Core.Enums.Applications
{
    public enum AppContextTypes
    {
        Undefined = 0,
        Web = 100,
        Desktop = 200,
        Service = 300,
        Test = 400
    }
}