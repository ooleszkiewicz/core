﻿using System.ComponentModel;

namespace CPExpert.Core.Enums
{
    public enum LanguagesCodes
    {
        [Description("")]
        Undefined = 0,
        [Description("pl-PL")] //CultureInfo - Name property
        Polsih = 100,
        [Description("en-EN")]
        English = 200
    }
}