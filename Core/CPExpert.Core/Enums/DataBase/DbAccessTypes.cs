﻿namespace CPExpert.Core.Enums.DataBase
{
    public enum DbAccessTypes
    {
        Undefined = 0,
        Restricted = 100,
        Unrestricted = 200,
    }
}