﻿namespace CPExpert.Core.Enums.DataBase.Security
{
    public enum SecurityObjectTypes
    {
        Undefined = 0,
        User = 100,
        Group = 200
    }
}