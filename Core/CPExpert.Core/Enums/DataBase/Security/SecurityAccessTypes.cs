﻿namespace CPExpert.Core.Enums.DataBase.Security
{
    public enum SecurityAccessTypes
    {
        Undefined = 0,
        Read = 100,
        Write = 200,
        Remove = 300,
    }
}