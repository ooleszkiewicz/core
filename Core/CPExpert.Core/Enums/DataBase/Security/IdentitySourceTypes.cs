﻿namespace CPExpert.Core.Enums.DataBase.Security
{
    public enum IdentitySourceTypes
    {
        Undefined = 0,
        ActiveDirectory = 100,
        Ldap = 200,
        Application = 300,
        LocalPc = 400
    }
}