﻿using System.ComponentModel;

namespace CPExpert.Core.Enums.DataBase
{
    public enum DbSecurityModes
    {
        [Description("Integrated security")]
        IntegratedSecurity = 100,

        [Description("User and password")]
        UserAndPassword = 200
    }
}