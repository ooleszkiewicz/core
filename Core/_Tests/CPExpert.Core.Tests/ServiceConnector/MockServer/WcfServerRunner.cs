﻿using System;
using System.Linq;
using System.ServiceModel;

namespace CPExpert.Core.Tests.ServiceConnector.MockServer
{
    public class WcfServerRunner
    {
        public bool StartServer()
        {
            try
            {
                Diagnostic.LogDebug(string.Format("StartSelfHostedWcfImporterService at: {0}", DateTime.Now));
                //instance = SqlProviderServices.SingletonInstance;

                //inicjalizacja wszytskich serwisów
                ServiceHost = new ServiceHost(typeof(WcfService));

                //uruchomienie serwisów
                if (ServiceHost != null)
                {
                    try
                    {
                        if (ServiceHost.BaseAddresses.Any())
                        {
                            foreach (var baseAddress in ServiceHost.BaseAddresses)
                            {
                                Diagnostic.LogDebug("Service is Running at following address: {0}", baseAddress);
                            }
                        }

                        ServiceHost.Open();
                        return true;
                    }
                    catch (Exception e)
                    {
                        Diagnostic.Log(e);
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
            }

            return false;
        }

        public bool StopServer()
        {
            try
            {
                if (ServiceHost != null)
                {
                    ServiceHost.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
            }

            return false;
        }

        private ServiceHost ServiceHost { get; set; }
    }
}