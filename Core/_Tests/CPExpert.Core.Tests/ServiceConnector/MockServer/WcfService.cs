﻿using System;

namespace CPExpert.Core.Tests.ServiceConnector.MockServer
{
    public class WcfService : IWcfService
    {
        public WcfService()
        {
            _iterator = 0;
        }

        public bool IsWcfServiceAvailable()
        {
            return true;
        }

        private int _iterator;

        public int SingleSessionTest_Iterator()
        {
            return _iterator++;
        }

        public DateTime GetCurrentDate()
        {
            return DateTime.Now;
        } 
    }
}