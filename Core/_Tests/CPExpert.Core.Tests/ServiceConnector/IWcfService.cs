﻿using System;
using System.ServiceModel;
using CPExpert.Core.Interfaces.Communications.Wcf;

namespace CPExpert.Core.Tests.ServiceConnector
{
    [ServiceContract]
    public interface IWcfService : IWcfBase
    {
        [OperationContract]
        DateTime GetCurrentDate();
    }
}