﻿using System;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using CPExpert.Core.Tests.ServiceConnector.MockServer;
using NUnit.Framework;
using CPExpert.Core.Objects.Communications.Wcf;

namespace CPExpert.Core.Tests.ServiceConnector
{
    public class TestServiceClientConnector_Duplex
    {
        private readonly BasicHttpBinding customHttpBinding = new BasicHttpBinding
        {
            Name = "ClientWcf_BasicHttpBinding",
            CloseTimeout = TimeSpan.FromMinutes(10),
            OpenTimeout = TimeSpan.FromMinutes(5),
            ReceiveTimeout = TimeSpan.FromMinutes(10),
            SendTimeout = TimeSpan.FromMinutes(10),
            MaxBufferPoolSize = 2147483647,
            MaxBufferSize = 2147483647,
            MaxReceivedMessageSize = 2147483647,
            MessageEncoding = WSMessageEncoding.Text
        };

        private WSDualHttpBinding wsDualHttpBinding = new WSDualHttpBinding
        {
            Name = "ClientWcf_WSDualHttpBinding",
            CloseTimeout = TimeSpan.FromMinutes(10),
            OpenTimeout = TimeSpan.FromMinutes(5),
            ReceiveTimeout = TimeSpan.FromMinutes(10),
            SendTimeout = TimeSpan.FromMinutes(10),
            MaxBufferPoolSize = 2147483647,
            MaxReceivedMessageSize = 2147483647,
            MessageEncoding = WSMessageEncoding.Text
        };

        //[Test, Order(1)]
        public void TestRunServer()
        {
            Task.Factory.StartNew(RunServer);
            Thread.Sleep(10000);
        }

        //[Test]
        //[TestCase("http://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost/Temporary_Listen_Addresses")]
        //[TestCase("http://127.0.0.1/Temporary_Listen_Addresses")]
        public void CreateAndConnectClientToServer(string serviceAddress)
        {
            var taskStartWcfHost = Task.Factory.StartNew(RunServer);
            Task.WaitAll(taskStartWcfHost);

            var clientConnextor = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>(serviceAddress, customHttpBinding);
            Assert.IsTrue(clientConnextor.Connect());

            var clientConnextor2 = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>("http://localhost:9999", wsDualHttpBinding, null, new InstanceContext(this));
            Assert.IsTrue(clientConnextor2.Connect());
        }

        //[Test]
        //[TestCase("http://127.0.0.1;80000/Temporary_Listen_Addresses")]
        //[TestCase("\\sqoo")]
        //[TestCase("xxxxxxx")]
        //[TestCase("abcdefghijklmnopr0123oksjaj")]
        //[TestCase("ab12cd0s3")]
        //[TestCase("domain.pl\\sq")]
        //[TestCase("httpm://127.0.0.1/Temporary_Listen_Addresses")]
        public void CreateConnectorAndValidateAddress_Fail(string serviceAddress)
        {
            var taskStartWcfHost = Task.Factory.StartNew(RunServer);
            Task.WaitAll(taskStartWcfHost);
            ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>> clientConnextor;

            try
            {
                clientConnextor = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>(serviceAddress, customHttpBinding);
            }
            catch (Exception)
            {
                clientConnextor = null;
            }

            Assert.IsNull(clientConnextor);
        }

        //[Test]
        //[TestCase("http://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost/Temporary_Listen_Addresses")]
        //[TestCase("http://127.0.0.1/Temporary_Listen_Addresses")]
        //[TestCase("http://oskarpc.cp-expert.local")]
        //[TestCase("http://oskarpc.cp-expert.local/xxxx")]
        //[TestCase("http://oskarpc.cp-expert.local/xxxx/yyyy/costam.svc")]
        //[TestCase("http://oskarpc.cp-expert.local/xxxx/costam.svc")]
        //[TestCase("http://2887188589")]
        //[TestCase("http://2887188589/xxxxx")]
        //[TestCase("http://2887188589/xxxxx/yyyy")]
        //[TestCase("http://2887188589/xxxxx/yyyy/service.svc")]
        //[TestCase("https://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("https://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("https://localhost/Temporary_Listen_Addresses")]
        //[TestCase("https://127.0.0.1/Temporary_Listen_Addresses")]
        //[TestCase("https://oskarpc.cp-expert.local/xxxx")]
        //[TestCase("https://oskarpc.cp-expert.local/xxxx/yyyy/costam.svc")]
        //[TestCase("https://oskarpc.cp-expert.local/xxxx/costam.svc")]
        //[TestCase("https://2887188589")]
        //[TestCase("https://2887188589/xxxxx")]
        //[TestCase("https://2887188589/xxxxx/yyyy")]
        //[TestCase("https://2887188589/xxxxx/yyyy/service.svc")]
        //[TestCase("net.tcp://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("net.tcp://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("net.tcp://localhost/Temporary_Listen_Addresses")]
        //[TestCase("net.tcp://127.0.0.1/Temporary_Listen_Addresses")]
        //[TestCase("net.tcp://oskarpc.cp-expert.local/xxxx")]
        //[TestCase("net.tcp://oskarpc.cp-expert.local/xxxx/yyyy/costam.svc")]
        //[TestCase("net.tcp://oskarpc.cp-expert.local/xxxx/costam.svc")]
        //[TestCase("net.tcp://2887188589")]
        //[TestCase("net.tcp://2887188589/xxxxx")]
        //[TestCase("net.tcp://2887188589/xxxxx/yyyy")]
        //[TestCase("net.tcp://2887188589/xxxxx/yyyy/service.svc")]
        //[TestCase("http://nazwa_dns:9991")]
        //[TestCase("http://moja.nazwa.dns:9991")]
        public void CreateConnectorAndValidateAddress_Success(string serviceAddress)
        {
            var taskStartWcfHost = Task.Factory.StartNew(RunServer);
            Task.WaitAll(taskStartWcfHost);
            ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>> clientConnextor;

            try
            {
                clientConnextor = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>(serviceAddress, customHttpBinding);
            }
            catch (Exception)
            {
                clientConnextor = null;
            }

            Assert.IsNotNull(clientConnextor);
        }

        //[Test]
        //[TestCase("http://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost/Temporary_Listen_Addresses")]
        //[TestCase("http://127.0.0.1/Temporary_Listen_Addresses")]
        public void ExecuteMethod(string serviceAddress)
        {
            var taskStartWcfHost = Task.Factory.StartNew(RunServer);
            Task.WaitAll(taskStartWcfHost);

            var clientConnextor = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>(serviceAddress, customHttpBinding);
            Assert.IsTrue(clientConnextor.Connect());
            Assert.IsTrue(clientConnextor.WcfService.IsWcfServiceAvailable());
            var currentDate = clientConnextor.WcfService.GetCurrentDate();
            Assert.AreEqual(currentDate.Year, DateTime.Now.Year);
            Assert.AreEqual(currentDate.Month, DateTime.Now.Month);
            Assert.AreEqual(currentDate.Day, DateTime.Now.Day);
            Assert.AreEqual(currentDate.Hour, DateTime.Now.Hour);
        }

        //[Test]
        //[TestCase("http://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost/Temporary_Listen_Addresses")]
        //[TestCase("http://127.0.0.1/Temporary_Listen_Addresses")]
        public void ExecuteMethodWithCustomBinding(string serviceAddress)
        {
            var taskStartWcfHost = Task.Factory.StartNew(RunServer);
            Task.WaitAll(taskStartWcfHost);

            var clientConnextor = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>(serviceAddress, customHttpBinding);

            Assert.IsTrue(clientConnextor.Connect());
            Assert.IsTrue(clientConnextor.WcfService.IsWcfServiceAvailable());
            var currentDate = clientConnextor.WcfService.GetCurrentDate();
            Assert.AreEqual(currentDate.Year, DateTime.Now.Year);
            Assert.AreEqual(currentDate.Month, DateTime.Now.Month);
            Assert.AreEqual(currentDate.Day, DateTime.Now.Day);
            Assert.AreEqual(currentDate.Hour, DateTime.Now.Hour);
        }

        //[Test]
        //[TestCase("http://127.0.0.1:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost:80/Temporary_Listen_Addresses")]
        //[TestCase("http://localhost/Temporary_Listen_Addresses")]
        //[TestCase("http://127.0.0.1/Temporary_Listen_Addresses")]
        public void ExecuteMethodCloseAndTryExecuteMethodOnce(string serviceAddress)
        {
            var taskStartWcfHost = Task.Factory.StartNew(RunServer);
            Task.WaitAll(taskStartWcfHost);

            var clientConnextor = new ServiceConnector<IWcfService, DuplexChannelFactory<IWcfService>>(serviceAddress, customHttpBinding);
            Assert.IsTrue(clientConnextor.Connect());
            Assert.IsTrue(clientConnextor.WcfService.IsWcfServiceAvailable());
            var currentDate = clientConnextor.WcfService.GetCurrentDate();
            Assert.AreEqual(currentDate.Year, DateTime.Now.Year);
            Assert.AreEqual(currentDate.Month, DateTime.Now.Month);
            Assert.AreEqual(currentDate.Day, DateTime.Now.Day);
            Assert.AreEqual(currentDate.Hour, DateTime.Now.Hour);

            clientConnextor.Close("1qaz@WSX");
            Assert.IsTrue(clientConnextor.WcfService.IsWcfServiceAvailable());
            currentDate = clientConnextor.WcfService.GetCurrentDate();
            Assert.AreEqual(currentDate.Year, DateTime.Now.Year);
            Assert.AreEqual(currentDate.Month, DateTime.Now.Month);
            Assert.AreEqual(currentDate.Day, DateTime.Now.Day);
            Assert.AreEqual(currentDate.Hour, DateTime.Now.Hour);
        }

        private WcfServerRunner wcfServerHost { get; set; }
        private void RunServer()
        {
            try
            {
                if (wcfServerHost != null)
                {
                    if (wcfServerHost.StopServer())
                    {
                        wcfServerHost = null;
                    }
                    else
                    {
                        Diagnostic.LogWarn("Can't stop service!");
                    }
                }

                //musi poczekac az stary test ubije serwer WCF
                Thread.Sleep(1000);

                wcfServerHost = new WcfServerRunner();

                //create wcf mock wcfServerHost
                if (!wcfServerHost.StartServer())
                {
                    throw new Exception("Error when create wcfServerHost host");
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
    }
}