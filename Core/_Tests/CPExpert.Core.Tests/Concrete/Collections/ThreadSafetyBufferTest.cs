﻿using System.Collections.Generic;
using System.Linq;
using CPExpert.Core.Enums;
using CPExpert.Core.Objects;
using CPExpert.Core.Objects.Collections;
using CPExpert.Core.Objects.Events.EventArguments;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Concrete.Collections
{
    [TestFixture]
    public class ThreadSafetyBufferTest : Test
    {
        private ThreadSafetyBuffer<int> tsbc;
        
        public override void Initialize()
        {
            Assert.AreEqual(true, true);
            ChangeData = new ChangeData();
        }

        [SetUp]
        public void SetupUp()
        {
            tsbc = new ThreadSafetyBuffer<int>();
            
            int ob1 = 100;
            int ob2 = 200;
            int ob3 = 300;
         
            tsbc.Add(ob1);
            tsbc.AddRange(new List<int>() {ob2, ob3});
        }

        [Test]
        public void AddTest()
        {
            int currCount = tsbc.Count;
            tsbc.Add(333);
            Assert.AreEqual(currCount + 1, tsbc.Count, 0, "Add new item - Failure");
        }

        [Test]
        public void AddRangeTest()
        {
            int currCount = tsbc.Count;
            var newItemsList = new List<int>() {0, 1, 2, 3};
            tsbc.AddRange(newItemsList);
            Assert.AreEqual(currCount + newItemsList.Count, tsbc.Count, 0, "AddRange of new items - Failure");
        }

        [Test]
        public void RemoveTest()
        {
            int currCount = tsbc.Count;
            tsbc.Remove(tsbc.GetAll().First());
            Assert.AreEqual(currCount - 1, tsbc.Count, 0, "Remove item - Failure");
        }

        [Test]
        public void RemoveRangeTest()
        {
            int currCount = tsbc.Count;
            int itemsToRemoveCount = 2;

            tsbc.RemoveRange(tsbc.GetAll().Take(itemsToRemoveCount).ToList());
            Assert.AreEqual(currCount - itemsToRemoveCount, tsbc.Count, 0, "RemoveRange - Failure");

            IList<object> list = new List<object>() { 1, "oskar", null, new object() };
            ThreadSafetyBuffer<object> threadSafetyBuffer = new ThreadSafetyBuffer<object>(list);

            threadSafetyBuffer.RemoveRange(list);
        }

        [Test]
        public void AnyTest()
        {
            Assert.AreEqual(true, tsbc.Any());
            tsbc.RemoveRange(tsbc.GetAll().ToList());
            Assert.AreEqual(false, tsbc.Any());
        }

        [Test]
        public void TestAddSingleElementEvent()
        {
            ChangeData = new ChangeData();
            var safetyBuffer = new ThreadSafetyBuffer<int>();
            safetyBuffer.CollectionChangedEvent += CollectionChange;
            safetyBuffer.Add(20);

            Assert.AreEqual(ThreadSafetyBufferChangeTypes.Added, ChangeData.Type);
            Assert.AreEqual(20, ChangeData.ChangeElements[0]);
        }

        [Test]
        public void TestAddMultipleElementEvent()
        {
            ChangeData = new ChangeData();
            var safetyBuffer = new ThreadSafetyBuffer<int>();
            safetyBuffer.CollectionChangedEvent += CollectionChange;
            var listToChange = new List<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
            safetyBuffer.AddRange(listToChange);

            Assert.AreEqual(ThreadSafetyBufferChangeTypes.Added, ChangeData.Type);

            for (int i = 0; i < listToChange.Count; i++)
            {
                Assert.AreEqual(listToChange[0], ChangeData.ChangeElements[0]);
            }
        }

        [Test]
        public void TestRemoveSingleElementEvent()
        {
            ChangeData = new ChangeData();
            var safetyBuffer = new ThreadSafetyBuffer<int>();
            safetyBuffer.CollectionChangedEvent += CollectionChange;
            safetyBuffer.Add(20);

            Assert.AreEqual(ThreadSafetyBufferChangeTypes.Added, ChangeData.Type);

            safetyBuffer.Remove(20);

            Assert.AreEqual(ThreadSafetyBufferChangeTypes.Removed, ChangeData.Type);
            Assert.AreEqual(20, ChangeData.ChangeElements[0]);
        }

        [Test]
        public void TestRemoveMultipleElementEvent()
        {
            ChangeData = new ChangeData();
            var safetyBuffer = new ThreadSafetyBuffer<int>();
            safetyBuffer.CollectionChangedEvent += CollectionChange;
            var listToChange = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            safetyBuffer.AddRange(listToChange);

            Assert.AreEqual(ThreadSafetyBufferChangeTypes.Added, ChangeData.Type);

            safetyBuffer.RemoveRange(listToChange);

            Assert.AreEqual(ThreadSafetyBufferChangeTypes.Removed, ChangeData.Type);

            for (int i = 0; i < listToChange.Count; i++)
            {
                Assert.AreEqual(listToChange[0], ChangeData.ChangeElements[0]);
            }
        }

        [Test]
        public void CountTest()
        {
            int currCount = tsbc.Count;
            tsbc.Add(133);
            tsbc.Add(233);
            tsbc.Add(333);
            tsbc.Add(433);
            tsbc.Add(533);
            tsbc.Add(633);
            tsbc.Add(733);
            tsbc.Add(833);
            tsbc.Add(933);
            int cuuCount2 = tsbc.Count;
            Assert.AreEqual(currCount + 9, cuuCount2);
        }

        private void CollectionChange(object sender, ThreadSafetyBufferEventArgs<int> e)
        {
            if (sender is ThreadSafetyBuffer<int> && e != null)
            {
                ChangeData.ChangeElements = e.Items;
                ChangeData.Type = e.ChangeType;
            }
        }

        private ChangeData ChangeData { get; set; }
    }

    internal class ChangeData
    {
        public ChangeData()
        {
            Type = ThreadSafetyBufferChangeTypes.Undefined;
            ChangeElements = new List<int>();
        }

        public ThreadSafetyBufferChangeTypes Type { get; set; }
        public IList<int> ChangeElements { get; set; } 
    }
}
