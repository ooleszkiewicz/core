﻿using System.IO;
using CPExpert.Core.Helpers;
using CPExpert.Core.Objects.Generators;
using NUnit.Framework;
using Exception = System.Exception;
using File = System.IO.File;

namespace CPExpert.Core.Tests.Concrete.Bussiness
{
    [TestFixture]
    public class SqFrameTest
    {
        [Test]
        public void GetFileTest()
        {
            var filePath1 = Path.Combine(Assembly.GetAssemblyWorkingPath(), "zamczysko1_160208_050100269.jpg");
            var filePath2 = Path.Combine(Assembly.GetAssemblyWorkingPath(), "Image_161003_165310438.jpg");

            if (File.Exists(filePath1))
            {
                var sqFrame = new Frame<int>();
                sqFrame.Create(filePath1, 2);
                Assert.IsNotNull(sqFrame.ImageBytes);
            }
            else
            {
                throw new Exception("File not found!");
            }

            if (File.Exists(filePath2))
            {
                var sqFrame = new Frame<int>();
                sqFrame.Create(filePath2, 2);
                Assert.IsNotNull(sqFrame.ImageBytes);
            }
            else
            {
                throw new Exception("File not found!");
            }
        }
    }
}