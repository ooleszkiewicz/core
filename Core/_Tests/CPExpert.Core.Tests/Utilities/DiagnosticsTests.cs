﻿using System;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class DiagnosticsTests : Test
    {
        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogDebugTest(bool _insertFullNamespace)
        {
            if (_insertFullNamespace)
            {
                Diagnostic.EnableFullMethodName();
            }

            Diagnostic.LogDebug("debug log level");
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogErrorTest(bool _insertFullNamespace)
        {
            if (_insertFullNamespace)
            {
                Diagnostic.EnableFullMethodName();
            }

            Diagnostic.LogError("error log level");
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogFatalTest(bool _insertFullNamespace)
        {
            if (_insertFullNamespace)
            {
                Diagnostic.EnableFullMethodName();
            }

            Diagnostic.LogFatal("fatal log level");
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogTraceTest(bool _insertFullNamespace)
        {
            if (_insertFullNamespace)
            {
                Diagnostic.EnableFullMethodName();
            }

            Diagnostic.LogTrace("trace log level");
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogWarnTest(bool _insertFullNamespace)
        {
            if (_insertFullNamespace)
            {
                Diagnostic.EnableFullMethodName();
            }

            Diagnostic.LogWarn("warn log level");
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogExceptionTest(bool _insertFullNamespace)
        {
            Diagnostic.Log(new Exception("exception log level"));
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void LogInfoTest(bool _insertFullNamespace)
        {
            if (_insertFullNamespace)
            {
                Diagnostic.EnableFullMethodName();
            }

            Diagnostic.LogInfo("info log level"); 
            Diagnostic.LogInfo("info log level with stringFormat and objects value: {0}", new testObject("OK"));
        }

        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }

        public class testObject
        {
            public string testObjectString1 { get; set; }

            public testObject(string testObjectString1)
            {
                this.testObjectString1 = testObjectString1;
            }

            public override string ToString()
            {
                return string.Format("testObject str1: {0}", testObjectString1);
            }
        }
    }
}
