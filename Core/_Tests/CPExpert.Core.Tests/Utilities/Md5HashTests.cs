﻿using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{   
    [TestFixture]
    public class Md5HashTests : Test
    {
        [TestCase("sample")]
        [TestCase("anotherSample")]
        public void Md5HashEncode_ReturnTrue(string input)
        {
            string key = "HardToBrokeKey";
            string encodedString1 = Cryptography.Md5Hash.Encoder(input, key);
            string encodedString2 = Cryptography.Md5Hash.Encoder(input, key);
            Assert.AreEqual(encodedString2,encodedString1);
        }
        [Test]
        public void Md5HashEncode_ReturnFalse()
        {
            string key = "HardToBrokeKey";
            string encodedString1 = Cryptography.Md5Hash.Encoder("sample", key);
            string encodedString2 = Cryptography.Md5Hash.Encoder("sAmple", key);
            Assert.AreNotEqual(encodedString2,encodedString1);
        }
        [TestCase("fARWJnCfOao=", "sample", "HardToBrokeKey")]
        [TestCase("GjjbXBE3x/nuRXXyu19ZSg==", "anotherSample", "HardToBrokeKey")]
        public void Md5HashDecode_ReturnTrue(string input, string expectedOutput, string key)
        {
            string decodedString1 = Cryptography.Md5Hash.Decoder(input, key);
            Assert.AreEqual(decodedString1,expectedOutput);
        }
        [TestCase("fARWJnCfOao=", "sAmple", "HardToBrokeKey")]
        [TestCase("GjjbXBE3x/nuRXXyu19ZSg==", "an0therSample", "HardToBrokeKey")]
        public void Md5HashDecode_ReturnFalse(string input, string expectedOutput, string key)
        {
            string decodedString1 = Cryptography.Md5Hash.Decoder(input, key);
            Assert.AreNotEqual(decodedString1, expectedOutput);
        }

        [Test]
        public void EncryptedAndDecrypted()
        {
            var password = @"P@ssw0rd32167";
            var key = @"^TLZD64M4M9#EBG_";

            var encryptedPassword = Cryptography.Md5Hash.Encoder(password, key);
            var decryptedPassword = Cryptography.Md5Hash.Decoder(encryptedPassword, key);
            Assert.AreEqual(password, decryptedPassword);
        }

        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }
    }
}
