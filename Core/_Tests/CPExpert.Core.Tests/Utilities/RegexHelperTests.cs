﻿using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class RegexHelperTests : Test
    {
        [TestCase("10.0.0.1")]
        [TestCase("127.0.0.1:7900")]
        [TestCase("192.168.0.1")]
        [TestCase("http://192.168.0.1")]
        [TestCase("https://192.168.0.1:4233")]
        public void RegexCheckIpAddress_ReturnTrue(string ip)
        {
            var regex = Utility.RegexPatterns.IsIpAddress;
            Assert.IsTrue(regex.IsMatch(ip));
        }

        [TestCase("999.1.1.1")]
        [TestCase("aaa.aaa.aaa.aaa")]
        [TestCase("000.000.000.000")]
        [TestCase("192.168.0.1:65555")]
        [TestCase("htt://192.168.0.1")]
        public void RegexCheckIpAddress_ReturnFalse(string ip)
        {
            var regex = Utility.RegexPatterns.IsIpAddress;
            Assert.IsFalse(regex.IsMatch(ip));
        }
        [TestCase("http://sq.net.pl")]
        [TestCase("http://wp.pl:1230")]
        [TestCase("https://sq.pl")]
        [TestCase("sq.pl:2134")]
        [TestCase("sq.pl")]
        public void RegexCheckDomainAddress_ReturnTrue(string domain)
        {
            var regex = Utility.RegexPatterns.IsDomainAddress;
            Assert.IsTrue(regex.IsMatch(domain));
        }
        [TestCase("http:/sq.net.pl")]
        [TestCase("http://sq.net.pl:70000")]
        public void RegexCheckDomainAddress_ReturnFalse(string domain)
        {
            var regex = Utility.RegexPatterns.IsDomainAddress;
            Assert.IsFalse(regex.IsMatch(domain));
        }
        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }
    }
}
