﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPExpert.Core.Enums;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class InternacionalizationTest
    {
        [Test]
        public void SetTranslation()
        {
            CPExpert.Core.Helpers.Internationalization.Translation.Set(LanguagesCodes.Polsih);
        }
    }
}
