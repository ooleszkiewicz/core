﻿using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class HashCheckSumTests : Test
    {
        [Test]
        public void HashCheck_ReturnTrue()
        {
            Person one = new Person("Jan",22);
            Person two = new Person("Jan",22);
            string checkSumOfOne = Cryptography.HashCheckSum.Calculate(two);
            string checkSumOfTwo = Cryptography.HashCheckSum.Calculate(one);
            Assert.AreEqual(checkSumOfOne,checkSumOfTwo);
        }
        [Test]
        public void HashCheck_ReturnFalse()
        {
            Person one = new Person("Jan", 22);
            Person two = new Person("Janek", 22);
            string checkSumOfOne = Cryptography.HashCheckSum.Calculate(two);
            string checkSumOfTwo = Cryptography.HashCheckSum.Calculate(one);
            Assert.AreNotEqual(checkSumOfOne, checkSumOfTwo);
        }
        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }
    }
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Person() { }
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}