﻿using System;
using System.Data.SqlClient;
using CPExpert.Core.Enums;
using CPExpert.Core.Enums.DataBase;
using CPExpert.Core.Helpers.DataBases;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class MsSqlHelperTests
    {
        [Test]
        public void CreateConnectionStringWithCreditionals()
        {
            var connectionString = MsSql.CreateConnectionString(serverAndInstanceName, dataBaseName, userLogin, userPassword, false);
            Assert.IsNotNull(connectionString);
			Assert.IsNotEmpty(connectionString);
            var compare = connectionToCompareWithCreditionals.ToLower().Trim();
            var generated = connectionString.ToLower().Trim();

            if (!compare.Equals(generated, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new Exception("Compare and generated connection string is not the same!");
            }
        }

        [Test]
        public void CreateConnectionStringWithIntegratedSecurity()
        {
            var connectionString = MsSql.CreateConnectionString(serverAndInstanceName, dataBaseName, userLogin, userPassword, true);
            Assert.IsNotNull(connectionString);
            Assert.IsNotEmpty(connectionString);
            var compare = connectionToCompareWithIntegratedSecurity.ToLower().Trim();
            var generated = connectionString.ToLower().Trim();

            if (!compare.Equals(generated, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new Exception("Compare and generated connection string is not the same!");
            }
        }

		//[Test]
        public void TestConnectionToServerInstanceWithNoExistDataBaseWithCreditionals_Success()
        {
            var connectionString = MsSql.CreateConnectionString(serverAndInstanceName, "no_exist", userLogin, userPassword, false);
            bool isServerAvaible = MsSql.IsServerValidConnection(connectionString);
            Assert.IsTrue(isServerAvaible);
        }

        //[Test]
        public void TestConnectionToDataBaseInstanceWithExistDataBaseWithCreditionals_Success()
        {
            var connectionString = MsSql.CreateConnectionString(serverAndInstanceName, dataBaseName, userLogin, userPassword, false);
            bool isDataBaseAvaible = MsSql.IsDataBaseValidConnection(connectionString);
            Assert.IsTrue(isDataBaseAvaible);
        }

        //[Test]
        public void TestConnectionToDataBaseInstanceWithExistDataBaseWithIntegratedSecurity_Success()
        {
            var connectionString = MsSql.CreateConnectionString(serverAndInstanceName, dataBaseName, userLogin, userPassword, false);
            bool isDataBaseAvaible = MsSql.IsDataBaseValidConnection(connectionString);
            Assert.IsTrue(isDataBaseAvaible);
        }

        //[Test]
        public void CreateNewUserOnDataBase()
        {
            var userLogin = string.Format("user_{0}", Guid.NewGuid().ToString("N"));
            var password = "12345";
            SqlConnection connection = new SqlConnection(connectionToCompareWithCreditionals);
            bool createUserSuccess = MsSql.CreateUserAndSetDefaultDbAndRole(connection, userLogin, password, connection.Database, DbMsSqlRoles.db_owner);
            Assert.IsTrue(createUserSuccess);
        }

        const string serverAndInstanceName = @"172.23.0.137\SQLEXPRESS";
        const string dataBaseName = @"TestDataBase1";
        const string userLogin = @"testuser";
        const string userPassword = @"P@ssw0rd";

        static readonly string connectionToCompareWithCreditionals = 
            string.Format(@"Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};MultipleActiveResultSets=True",
            serverAndInstanceName, dataBaseName, userLogin, userPassword);

        static readonly string connectionToCompareWithIntegratedSecurity = 
            string.Format(@"Data Source={0};Initial Catalog={1};Persist Security Info=True;Integrated Security=True;MultipleActiveResultSets=True",
            serverAndInstanceName, dataBaseName);
    }
}