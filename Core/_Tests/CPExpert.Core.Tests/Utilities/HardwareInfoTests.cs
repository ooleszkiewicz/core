﻿using System.Management;
using CPExpert.Core.Helpers;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class HardwareInfoTests
    {
        [Test]
        public void GetValueByKey()
        {
            var id = string.Empty;
            var list = HardwareInfo.GetManagementObjectCollection(HardwareInfo.Keys.Win32_Processor);
            foreach (var o in list)
            {
                var mo = (ManagementObject) o;
                id = mo["ProcessorID"].ToString();
            }

            Assert.IsNotEmpty(id);
            Assert.IsNotNull(id);
        }
    }
}