﻿using CPExpert.Core.Helpers;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class AssemblyHelperTests
    {
        [Test]
        [TestCase("284be8b2-b168-44c9-96f9-cc749dea4cf7")]
        public void GetGuid(string guid)
        {
            Assert.AreEqual(guid, Assembly.GetGuid());
        }

        [Test]
        [TestCase("CPExpert.Core.Tests")]
        public void GetTitle(string title)
        {
            Assert.AreEqual(title, Assembly.GetTitle());
        }


        [Test]
        [TestCase("description1")]
        public void GetDescription(string desc)
        {
            Assert.AreEqual(desc, Assembly.GetDescription());
        }


        [Test]
        [TestCase("configuration1")]
        public void GetConfiguration(string cfg)
        {
            Assert.AreEqual(cfg, Assembly.GetConfiguration());
        }

        [Test]
        [TestCase("company1")]
        public void GetCompany(string company)
        {
            Assert.AreEqual(company, Assembly.GetCompany());
        }

        [Test]
        [TestCase("Common.Tests")]
        public void GetProduct(string product)
        {
            Assert.AreEqual(product, Assembly.GetProduct());
        }

        [Test]
        [TestCase("Copyright ©  2016")]
        public void GetCopyright(string product)
        {
            Assert.AreEqual(product, Assembly.GetCopyright());
        }

        [Test]
        [TestCase("trademark1")]
        public void GetTrademark(string trademark)
        {
            Assert.AreEqual(trademark, Assembly.GetTrademark());
        }

        [Test]
        [TestCase("en")]
        public void GetCulture(string culture)
        {
            Assert.AreEqual(culture, Assembly.GetCulture());
        }

        [Test]
        [TestCase("1.0.0.0")]
        public void GetVersion(string assemblyVersion)
        {
            Assert.AreEqual(assemblyVersion, Assembly.GetVersion());
        }

        [Test]
        [TestCase("1.0.0.0")]
        public void GetFileVersion(string fileVersion)
        {
            Assert.AreEqual(fileVersion, Assembly.GetFileVersion());
        }
    }
}