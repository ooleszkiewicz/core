﻿using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class NetworkHelperTests : Test
    {
        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }

        [Test]
        public void TryGetActiveIpAddress()
        {
            var x = Helpers.Network.GetActiveIpAddress();
            Assert.IsNotNull(x);
        }
    }
}