﻿using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class ValidatorsTests : Test
    {
        [TestCase("5590026953")]
        [TestCase("9532539098")]
        [TestCase("1111111111")]
        [TestCase("2222222222")]
        public void ValidateNipNumber_ReturnTrue(string nip)
        {
            Assert.IsTrue(Validators.Nip(nip));
        }
        [TestCase("953")]
        [TestCase("asd")]
        [TestCase("9532 39098")]
        [TestCase("9532x39098")]
        [TestCase("55900269531")]
        [TestCase("0000000000")]
        [TestCase("2222222220")]
        [TestCase("")]
        public void ValidateNipNumber_RetrunFalse(string nip)
        {
            Assert.IsFalse(Validators.Nip(nip));
        }
        [TestCase("32060905173")]
        public void ValidatePeselNumber_ReturnTrue(string pesel)
        {
            Assert.IsTrue(Validators.Pesel(pesel));
        }
        [TestCase("320609051731")]
        [TestCase("asdfghoiu12")]
        [TestCase("")]
        [TestCase("32060f05173")]
        public void ValidatePeselNumber_ReturnFalse(string pesel)
        {
            Assert.IsFalse(Validators.Pesel(pesel));
        }
        [TestCase("253031970")]
        [TestCase("01678458479523")]
        public void ValidateRegonNumber_ReturnTrue(string regon)
        {
            Assert.IsTrue(Validators.Regon(regon));
        }
        [TestCase("2530s1970")]
        [TestCase("016784584795f3")]
        [TestCase("")]
        [TestCase("1")]
        public void ValidateRegonNumber_ReturnFalse(string regon)
        {
            Assert.IsFalse(Validators.Regon(regon));
        }
        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }
    }
}
