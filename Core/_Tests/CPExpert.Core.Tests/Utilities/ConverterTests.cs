﻿using CPExpert.Core.Enums;
using CPExpert.Core.Enums.Files;
using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Utilities
{
    [TestFixture]
    public class ConverterTests : Test
    {
        [Test]
        public void FileExtensionConvertFileNameToFileExtensionTest()
        {
            //documents
            const string pdfFile = @"document.pdf";
            const string wordFile = @"document.docx";

            //images
            const string jpegFile = @"image.jpeg";
            const string jpgFile = @"image.jpg";
            const string bmpFile = @"image.bmp";
            const string pngFile = @"image.png";
            const string gifFile = @"image.gif";

            //undefined
            const string undefinedFile = @"undefined.xxx";

            Assert.AreEqual(FileExtensions.Pdf, File.GetFileExtension(pdfFile));
            Assert.AreEqual(FileExtensions.Docx, File.GetFileExtension(wordFile));
            Assert.AreEqual(FileExtensions.Jpeg, File.GetFileExtension(jpegFile));
            Assert.AreEqual(FileExtensions.Jpg, File.GetFileExtension(jpgFile));
            Assert.AreEqual(FileExtensions.Bmp, File.GetFileExtension(bmpFile));
            Assert.AreEqual(FileExtensions.Png, File.GetFileExtension(pngFile));
            Assert.AreEqual(FileExtensions.Gif, File.GetFileExtension(gifFile));
            Assert.AreEqual(FileExtensions.Undefined, File.GetFileExtension(undefinedFile));
        }

        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }
    }
}