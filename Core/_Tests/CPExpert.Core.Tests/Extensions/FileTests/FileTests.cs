﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;
using Exception = System.Exception;
using File = System.IO.File;
using Image = CPExpert.Core.Helpers.Image;

namespace CPExpert.Core.Tests.Extensions.FileTests
{
    [TestFixture]
    public class FileTests : Test
    {
        private static readonly string FilesRepoPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Extensions", @"FileTests", @"Files");
        private static readonly string FilesRepoCopyPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Extensions", @"FileTests", @"Files", @"Copy");
        private static readonly string FilesRepoTestPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Extensions", @"FileTests", @"Files", @"Test");
        private string[] FilesOnHdd { get; set; }

        private const string IconCopy = @"icon — kopia.ico";
        private const string Icon = @"icon.ico";
        private const string PackageExample = @"packageExample.zip";
        private const string SimpleSite = @"simpleSite.md";

        #region Compare

        [Test]
        public void CompareTwoSameFilesWithSameNamesOnDiferentDirectory()
        {
            var pathOfFile1 = Path.Combine(FilesRepoPath, Icon);
            var pathOfFile2 = Path.Combine(FilesRepoCopyPath, Icon);

            Assert.IsTrue(File.Exists(pathOfFile1));
            Assert.IsTrue(File.Exists(pathOfFile2));

            Assert.IsTrue(Helpers.File.Compare(pathOfFile1, pathOfFile2));
        }

        [Test]
        public void CompareTwoSameFilesWithDifferentNamesInTheSameDirectory()
        {
            var pathOfFile1 = Path.Combine(FilesRepoPath, Icon);
            var pathOfFile2 = Path.Combine(FilesRepoPath, IconCopy);

            Assert.IsTrue(File.Exists(pathOfFile1));
            Assert.IsTrue(File.Exists(pathOfFile2));

            Assert.IsTrue(Helpers.File.Compare(pathOfFile1, pathOfFile2));
        }

        [Test]
        public void CompareTwoDifferentFiles()
        {
            var pathOfFile1 = Path.Combine(FilesRepoPath, Icon);
            var pathOfFile2 = Path.Combine(FilesRepoPath, PackageExample);

            Assert.IsTrue(File.Exists(pathOfFile1));
            Assert.IsTrue(File.Exists(pathOfFile2));

            Assert.IsFalse(Helpers.File.Compare(pathOfFile1, pathOfFile2));
        }

        #endregion

        #region Copy

        [Test]
        public void CopyFileLessThan2Gb()
        {
            var fileName = PackageExample;
            var filePath = Path.Combine(FilesRepoPath, fileName);
            var destinationFilePath = Path.Combine(FilesRepoCopyPath, fileName);

            if(File.Exists(destinationFilePath))
            {
                File.Delete(destinationFilePath);
            }

            byte[] file = Helpers.File.Read(filePath);

            Assert.IsNotNull(file);

            if (file.Length > 0)
            {
                Assert.IsFalse(File.Exists(destinationFilePath));
                Helpers.File.Write(destinationFilePath, file);
                Assert.IsTrue(File.Exists(destinationFilePath));
                File.Delete(destinationFilePath);
                Assert.IsFalse(File.Exists(destinationFilePath), "File can't deleted from directory, remove manually!");
            }
            else
            {
                Assert.Fail("File is empty!");
            }
        }

        [Test]
        public void CopyFileLargerThan2Gb()
        {
            //create file larged than 2GB, check is disk space avaible
            string driveLabel = Path.GetPathRoot(FilesRepoTestPath);

            if (!string.IsNullOrEmpty(driveLabel))
            {
                DriveInfo driveInfo = new DriveInfo(driveLabel);
                long availableFreeSpace = driveInfo.AvailableFreeSpace;
                double gigaBytesSize = Math.Round(((availableFreeSpace/1024f)/1024f)/1024f, 2);

                //copy need 2x file size
                //for test i create file larged 2GB + 1GB "buffer"
                if (gigaBytesSize > 5)
                {
                    //create larged file
                    var fileLargedThan2GbName = @"LargedThan2GbFileForTest.dat";
                    var fileLargedThan2GbNameFullPath = Path.Combine(FilesRepoTestPath, fileLargedThan2GbName);
                    var fileLargedThan2GbNameFullPathForCopy = Path.Combine(FilesRepoCopyPath, fileLargedThan2GbName);

                    if (File.Exists(fileLargedThan2GbNameFullPath))
                    {
                        File.Delete(fileLargedThan2GbNameFullPath);
                    }

                    if (File.Exists(fileLargedThan2GbNameFullPathForCopy))
                    {
                        File.Delete(fileLargedThan2GbNameFullPathForCopy);
                    }

                    FileStream fs = new FileStream(fileLargedThan2GbNameFullPath, FileMode.CreateNew);
                    fs.Seek(2048L * 1024 * 1024, SeekOrigin.Begin);
                    fs.WriteByte(0);
                    fs.Close();

                    Assert.IsTrue(File.Exists(fileLargedThan2GbNameFullPath), "Create file for test error");
                    Assert.IsFalse(File.Exists(fileLargedThan2GbNameFullPathForCopy), "File exist on copy directory");

                    Assert.IsTrue(Helpers.File.Copy(fileLargedThan2GbNameFullPath, FilesRepoCopyPath), "File copied error");

                    Assert.IsTrue(File.Exists(fileLargedThan2GbNameFullPath), "File deleted from primary directory");
                    Assert.IsTrue(File.Exists(fileLargedThan2GbNameFullPathForCopy), "Can't copy file!");

                    if(File.Exists(fileLargedThan2GbNameFullPath))
                    {
                        File.Delete(fileLargedThan2GbNameFullPath);
                    }

                    if (File.Exists(fileLargedThan2GbNameFullPathForCopy))
                    {
                        File.Delete(fileLargedThan2GbNameFullPathForCopy);
                    }

                    if (File.Exists(fileLargedThan2GbNameFullPath) || File.Exists(fileLargedThan2GbNameFullPathForCopy))
                    {
                        Assert.Fail("Can't remove test files!");
                    }
                }
                else
                {
                    Assert.Fail("No disk space avaible for test, please free up disk space! Disk space now avaible: {0} GB", gigaBytesSize);
                }
            }
            else
            {
                Assert.Fail("Can't get disk label!");
            }
        }

        #endregion

        #region Move

        [Test]
        public void MoveFileLessThan2Gb()
        {
            //var fileName = PackageExample;
            //var filePath = Path.Combine(FilesRepoPath, fileName);
        }

        [Test]
        public void MoveFileLargerThan2Gb()
        {
            //var fileName = PackageExample;
            //var filePath = Path.Combine(FilesRepoPath, fileName);
        }

        #endregion

        #region Access

        [Test]
        public void TrySetAccessToFile()
        {

        }

        [Test]
        public void TrySetAccessToEmptyDirectory()
        {

        }

        [Test]
        public void TrySetAccessToDirectoryWithFiles()
        {

        }

        #endregion

        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);

            if (!Directory.Exists(FilesRepoPath))
            {
                Assert.Fail("Directory path error: {0}", FilesRepoPath);
            }

            if (!Directory.Exists(FilesRepoCopyPath))
            {
                Assert.Fail("Directory path error: {0}", FilesRepoCopyPath);
            }

            if (!Directory.Exists(FilesRepoTestPath))
            {
                Directory.CreateDirectory(FilesRepoTestPath);
                if (!Directory.Exists(FilesRepoTestPath))
                {
                    Assert.Fail("Directory path error: {0}", FilesRepoTestPath);
                }
            }

            FilesOnHdd = Directory.GetFiles(FilesRepoPath);

            if (FilesOnHdd != null && FilesOnHdd.Length == 4)
            {
                Assert.IsNotNull(FilesOnHdd.FirstOrDefault(x =>
                {
                    var fileName = Path.GetFileName(x);
                    return fileName != null && fileName.Equals(Icon, StringComparison.CurrentCultureIgnoreCase);
                }));

                Assert.IsNotNull(FilesOnHdd.FirstOrDefault(x =>
                {
                    var fileName = Path.GetFileName(x);
                    return fileName != null && fileName.Equals(IconCopy, StringComparison.CurrentCultureIgnoreCase);
                }));

                Assert.IsNotNull(FilesOnHdd.FirstOrDefault(x =>
                {
                    var fileName = Path.GetFileName(x);
                    return fileName != null && fileName.Equals(Icon, StringComparison.CurrentCultureIgnoreCase);
                }));

                Assert.IsNotNull(FilesOnHdd.FirstOrDefault(x =>
                {
                    var fileName = Path.GetFileName(x);
                    return fileName != null && fileName.Equals(PackageExample, StringComparison.CurrentCultureIgnoreCase);
                }));

                Assert.IsNotNull(FilesOnHdd.FirstOrDefault(x =>
                {
                    var fileName = Path.GetFileName(x);
                    return fileName != null && fileName.Equals(SimpleSite, StringComparison.CurrentCultureIgnoreCase);
                }));

                //get copy of file
                Assert.IsTrue(File.Exists(Path.Combine(FilesRepoCopyPath, Icon)));
            }
            else
            {
                Assert.Fail("Error on loading files to test");
            }
        }

        //[Test]
        //public void CheckJpegFileFullyWrittenTest()
        //{
        //    var pathOfFile1 = Path.Combine(FilesRepoPath, Icon);

        //    if (File.Exists(pathOfFile1))
        //    {
        //        Common.Extensions.File.CheckJpegFileFullyWritten(pathOfFile1);
        //    }
        //}

        [Test]
        public void CreateBitmap()
        {
            var directoryWithData = Path.Combine(Assembly.GetAssemblyWorkingPath());
            var files = Directory.GetFiles(directoryWithData, "*.jpg").ToList();
            if (files.Any())
            {
                foreach (var file in files)
                {
                    var bytes = Helpers.File.Read(file);
                    if (bytes != null)
                    {
                        var bitmap = Image.CreateBitmapFromByteArray(bytes);

                        Assert.IsNotNull(bitmap);
                    }
                    else
                    {
                        throw new Exception("bytes is null");
                    }
                }
            }
            else
            {
                throw new Exception("no files found");
            }
        }

        [Test]
        public void CreateBitmap2()
        {
            var directoryWithData = Path.Combine(Assembly.GetAssemblyWorkingPath());
            var files = Directory.GetFiles(directoryWithData, "*.jpg").ToList();
            if (files.Any())
            {
                foreach (var file in files)
                {
                    if (string.IsNullOrEmpty(file))
                    {
                        continue;
                    }

                    var directoryPath = Path.Combine(Assembly.GetAssemblyWorkingPath(), "TestFiles", string.Format("{0}", Path.GetFileNameWithoutExtension(file)));

                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    byte[] bytes1 = Helpers.File.Read(file);
                    if (bytes1 != null)
                    {
                        //save - write
                        Helpers.File.Write(Path.Combine(directoryPath, "byte1.jpg"), bytes1);

                        Bitmap bitmap = Image.CreateBitmapFromByteArray(bytes1);
                        if (bitmap != null)
                        {
                            bitmap.Save(Path.Combine(directoryPath, "bitmap1.jpg"), ImageFormat.Jpeg);
                            byte[] newBytesArray2 = Image.CreateByteArrayFromBitmap(bitmap);
                            if (newBytesArray2 != null)
                            {
                                //save - write
                                Helpers.File.Write(Path.Combine(directoryPath, "byte2.jpg"), newBytesArray2);

                                Bitmap newBitmapFromByteArray = Image.CreateBitmapFromByteArray(newBytesArray2);
                                if (newBitmapFromByteArray != null)
                                {
                                    newBitmapFromByteArray.Save(Path.Combine(directoryPath, "bitmap2.jpg"), ImageFormat.Jpeg);
                                    byte[] newBytesArray3 = Image.CreateByteArrayFromBitmap(newBitmapFromByteArray);
                                    if (newBytesArray3 != null)
                                    {
                                        //save - write
                                        Helpers.File.Write(Path.Combine(directoryPath, "byte3.jpg"), newBytesArray3);
                                    }
                                    else
                                    {
                                        throw new Exception("newBytesArray3 is null");
                                    }
                                }
                                else
                                {
                                    throw new Exception("newBitmapFromByteArray is null");
                                }
                            }
                            else
                            {
                                throw new Exception("newBytesArray is null");
                            }
                        }
                        else
                        {
                            throw new Exception("bitmap is null");
                        }
                    }
                    else
                    {
                        throw new Exception("bytes is null");
                    }
                }
            }
            else
            {
                throw new Exception("no files found");
            }
        }
    }
}