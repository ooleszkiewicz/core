﻿using System.IO;
using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Extensions.ImageTest
{
    [TestFixture]
    public class ImageExtensionsTests : Test
    {
        public override void Initialize()
        {

        }

        [Test]
        public void ResizeImage()
        {
            var imgFilePath = Path.Combine(Assembly.GetAssemblyWorkingPath(), "zamczysko1_160208_050100269.jpg");
            Assert.IsTrue(System.IO.File.Exists(imgFilePath));
            var Img = System.Drawing.Image.FromFile(imgFilePath);
            var smallBitmap = Image.ResizeImage(Img, 320, 240);
            Assert.IsTrue(Image.IsValidJpeg(Image.CreateByteArrayFromBitmap(smallBitmap)));
            Assert.AreEqual(smallBitmap.Size.Width, 320);
            Assert.AreEqual(smallBitmap.Size.Height, 240);
        }
    }
}
