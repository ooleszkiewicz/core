﻿using System.Collections.Generic;
using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;
using Exception = System.Exception;

namespace CPExpert.Core.Tests.Extensions.XmlTests
{
    [TestFixture]
    public class XmlExtensionTests : Test
    {
        public override void Initialize()
        {
            xmlTest = string.Empty;
            threadSafetyBuffer = new List<int>();

            for (int i = 0; i < 100; i++)
            {
                threadSafetyBuffer.Add(i);
            }
        }

        private IList<int> threadSafetyBuffer { get; set; }
        private string xmlTest { get; set; }

        [Test]
        public void SerializeTest()
        {
            xmlTest = string.Empty;
            xmlTest = Xml.Serialize(threadSafetyBuffer);
            Assert.IsNotNull(xmlTest);
        }

        [Test]
        public void DeserializeTest()
        {
            SerializeTest();
            List<int> tempFromXml = null;
            Assert.IsNotNull(xmlTest);
            Assert.IsNull(tempFromXml);

            if (!string.IsNullOrEmpty(xmlTest))
            {
                var objectFromXml = Xml.Deserialize(xmlTest);
                Assert.IsNotNull(objectFromXml);

                if (objectFromXml is List<int>)
                {
                    tempFromXml = objectFromXml as List<int>;

                    Assert.AreEqual(tempFromXml.Count, threadSafetyBuffer.Count);

                    for (int i = 0; i < threadSafetyBuffer.Count; i++)
                    {
                        Assert.AreEqual(tempFromXml[i], threadSafetyBuffer[i]);
                    }
                }
                else
                {
                    throw new Exception("Deserialize error");
                }
            }
        }
    }

    class CTestType1
    {
        public int Id = -1;
        public string String1;
        public int Int1;

        public CTestType1(string String1 = "TestString", int Int1 = 0)
        {
            this.String1 = String1;
            this.Int1 = Int1;
        }
    }
}