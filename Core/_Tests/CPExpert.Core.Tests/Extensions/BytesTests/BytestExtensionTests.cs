﻿using System;
using System.Collections.Generic;
using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Extensions.BytesTests
{
    [TestFixture]
    public class BytestExtensionTests : Test
    {
        public override void Initialize()
        {
            byteTest = null;
            threadSafetyBuffer = new List<int>();

            for (int i = 0; i < 100; i++)
            {
                threadSafetyBuffer.Add(i);
            }
        }

        private IList<int> threadSafetyBuffer { get; set; }
        private byte[] byteTest { get; set; }

        [Test]
        public void SerializeTest()
        {
            byteTest = Bytes.Serialize(threadSafetyBuffer);
            Assert.IsNotNull(byteTest);
        }

        [Test]
        public void DeserializeTest()
        {
            SerializeTest();
            List<int> tempFromBytes = null;
            Assert.IsNotNull(byteTest);
            Assert.IsNull(tempFromBytes);

            if (byteTest != null)
            {
                var objectFromBytes = Bytes.Deserialize(byteTest);
                Assert.IsNotNull(objectFromBytes);

                if (objectFromBytes is List<int>)
                {
                    tempFromBytes = objectFromBytes as List<int>;

                    Assert.AreEqual(tempFromBytes.Count, threadSafetyBuffer.Count);

                    for (int i = 0; i < threadSafetyBuffer.Count; i++)
                    {
                        Assert.AreEqual(tempFromBytes[i], threadSafetyBuffer[i]);
                    }
                }
                else
                {
                    throw new Exception("Deserialize error");
                }
            }
        }
    }
}