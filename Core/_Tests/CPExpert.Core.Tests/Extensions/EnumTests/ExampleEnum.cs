﻿using System.ComponentModel;

namespace CPExpert.Core.Tests.Extensions.EnumTests
{
    public enum ExampleEnum
    {
        [Description("first")]
        FirstEnum = 0,

        [Description("second")]
        SecondEnum = 1,

        [Description("last")]
        LastEnum,

        NoAttrDescription
    }
}