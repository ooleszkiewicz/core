﻿using CPExpert.Core.Helpers;
using CPExpert.Core.Objects;
using NUnit.Framework;

namespace CPExpert.Core.Tests.Extensions.EnumTests
{
    [TestFixture]
    public class EnumTests : Test
    {
        private const string FirstDescription = "first";
        private const string SecondDescription = "second";
        private const string LastDescription = "last";
        private const string Default = "default";

        [Test]
        public void GetDescriptionFromAttribute()
        {
            var firstEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.FirstEnum);
            var secondEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.SecondEnum);
            var lastEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.LastEnum);

            Assert.AreEqual(firstEnumDescription, FirstDescription);
            Assert.AreEqual(secondEnumDescription, SecondDescription);
            Assert.AreEqual(lastEnumDescription, LastDescription);
        }

        [Test]
        public void GetDescriptionFromAttributeWithDefaultValue()
        {
            var firstEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.FirstEnum, Default);
            var secondEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.SecondEnum, Default);
            var lastEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.LastEnum, Default);
            var descFromEnumNoHaveDescAttr = Enum<ExampleEnum>.GetDescription(ExampleEnum.NoAttrDescription, Default);

            Assert.AreEqual(firstEnumDescription, FirstDescription);
            Assert.AreEqual(secondEnumDescription, SecondDescription);
            Assert.AreEqual(lastEnumDescription, LastDescription);
            Assert.AreEqual(descFromEnumNoHaveDescAttr, Default);
        }

        [Test]
        public void GetEnumFromDescription()
        {
            var firstEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.FirstEnum, Default);
            var secondEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.SecondEnum, Default);
            var lastEnumDescription = Enum<ExampleEnum>.GetDescription(ExampleEnum.LastEnum, Default);

            Assert.AreEqual(ExampleEnum.FirstEnum, Enum<ExampleEnum>.GetEnum(firstEnumDescription));
            Assert.AreEqual(ExampleEnum.SecondEnum, Enum<ExampleEnum>.GetEnum(secondEnumDescription));
            Assert.AreEqual(ExampleEnum.LastEnum, Enum<ExampleEnum>.GetEnum(lastEnumDescription));
        }

        public override void Initialize()
        {
            Assert.AreEqual(4, 2 + 2);
        }
    }
}
