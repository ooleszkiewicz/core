﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CP-EXPERT Core Desktop")]
[assembly: AssemblyProduct("CP-EXPERT Core Desktop Library")]
[assembly: AssemblyDescription("CP-EXPERT Core Desktop library for Computer Programming Expert IT projects")]
[assembly: Guid("46ef01f2-5d9a-4356-849d-5629f928209d")]

[assembly: AssemblyCompany("CP-EXPERT")]
[assembly: AssemblyCopyright("Copyright © 2016 CP-EXPERT")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.1")]
[assembly: NeutralResourcesLanguage("en")]