﻿using System;
using System.Windows.Forms;
using CPExpert.Core.Desktop.Common.Enums;

namespace CPExpert.Core.Desktop.Common.Events
{
    /// <summary>
    /// Event Args for the event that is fired after the hot key has been pressed.
    /// </summary>
    public class KeyPressedEventArgs : EventArgs
    {
        internal KeyPressedEventArgs(ModifierKeys modifier, Keys key)
        {
            _modifier = modifier;
            _key = key;
        }

        private readonly ModifierKeys _modifier;
        public ModifierKeys Modifier
        {
            get { return _modifier; }
        }

        private readonly Keys _key;
        public Keys Key
        {
            get { return _key; }
        }
    }
}