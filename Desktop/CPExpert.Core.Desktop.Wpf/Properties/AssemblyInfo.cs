﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CP-EXPERT Core Desktop WPF")]
[assembly: AssemblyProduct("CP-EXPERT Core Desktop WPF Library")]
[assembly: AssemblyDescription("CP-EXPERT Core Desktop WPF library for Computer Programming Expert IT projects")]
[assembly: Guid("21c53ab4-1968-467a-8678-eacca6784b60")]

[assembly: AssemblyCompany("CP-EXPERT")]
[assembly: AssemblyCopyright("Copyright © 2018 CP-EXPERT")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.0.55")]
[assembly: NeutralResourcesLanguage("en")]