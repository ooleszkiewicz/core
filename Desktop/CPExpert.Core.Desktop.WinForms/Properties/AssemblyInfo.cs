﻿﻿using System.Reflection;
﻿using System.Resources;
﻿using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CP-EXPERT Core Desktop WinForms")]
[assembly: AssemblyProduct("CP-EXPERT Core Desktop WinForms Library")]
[assembly: AssemblyDescription("CP-EXPERT Core Desktop WinForms library for Computer Programming Expert IT projects")]
[assembly: Guid("464778c1-a779-407b-b4df-d2d61250c05c")]

[assembly: AssemblyCompany("CP-EXPERT")]
[assembly: AssemblyCopyright("Copyright © 2018 CP-EXPERT")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.0.61")]
[assembly: NeutralResourcesLanguage("en")]