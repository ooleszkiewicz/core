﻿namespace CPExpert.Core.Desktop.WinForms.Forms
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelSeperator = new System.Windows.Forms.Label();
            this.linkLabelEN = new System.Windows.Forms.LinkLabel();
            this.linkLabelPL = new System.Windows.Forms.LinkLabel();
            this.PictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.PanelLanguageSelect = new System.Windows.Forms.Panel();
            this.PanelBase = new System.Windows.Forms.Panel();
            this.LabelFormTitle = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.PanelLanguageSelect.SuspendLayout();
            this.PanelBase.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSeperator
            // 
            this.labelSeperator.AutoSize = true;
            this.labelSeperator.Location = new System.Drawing.Point(23, 9);
            this.labelSeperator.Name = "labelSeperator";
            this.labelSeperator.Size = new System.Drawing.Size(9, 13);
            this.labelSeperator.TabIndex = 14;
            this.labelSeperator.Text = "|";
            // 
            // linkLabelEN
            // 
            this.linkLabelEN.AutoSize = true;
            this.linkLabelEN.LinkColor = System.Drawing.Color.Black;
            this.linkLabelEN.Location = new System.Drawing.Point(31, 9);
            this.linkLabelEN.Name = "linkLabelEN";
            this.linkLabelEN.Size = new System.Drawing.Size(22, 13);
            this.linkLabelEN.TabIndex = 13;
            this.linkLabelEN.TabStop = true;
            this.linkLabelEN.Text = "EN";
            this.linkLabelEN.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkLabelEN.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEN_LinkClicked);
            // 
            // linkLabelPL
            // 
            this.linkLabelPL.AutoSize = true;
            this.linkLabelPL.LinkColor = System.Drawing.Color.Black;
            this.linkLabelPL.Location = new System.Drawing.Point(5, 9);
            this.linkLabelPL.Name = "linkLabelPL";
            this.linkLabelPL.Size = new System.Drawing.Size(20, 13);
            this.linkLabelPL.TabIndex = 12;
            this.linkLabelPL.TabStop = true;
            this.linkLabelPL.Text = "PL";
            this.linkLabelPL.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkLabelPL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelPL_LinkClicked);
            // 
            // PictureBoxLogo
            // 
            this.PictureBoxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxLogo.BackColor = System.Drawing.SystemColors.Control;
            this.PictureBoxLogo.Location = new System.Drawing.Point(446, 12);
            this.PictureBoxLogo.Name = "PictureBoxLogo";
            this.PictureBoxLogo.Size = new System.Drawing.Size(142, 63);
            this.PictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxLogo.TabIndex = 15;
            this.PictureBoxLogo.TabStop = false;
            this.PictureBoxLogo.Visible = false;
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.ContainerControl = this;
            // 
            // PanelLanguageSelect
            // 
            this.PanelLanguageSelect.Controls.Add(this.linkLabelEN);
            this.PanelLanguageSelect.Controls.Add(this.linkLabelPL);
            this.PanelLanguageSelect.Controls.Add(this.labelSeperator);
            this.PanelLanguageSelect.Location = new System.Drawing.Point(17, 12);
            this.PanelLanguageSelect.Name = "PanelLanguageSelect";
            this.PanelLanguageSelect.Size = new System.Drawing.Size(58, 32);
            this.PanelLanguageSelect.TabIndex = 16;
            // 
            // PanelBase
            // 
            this.PanelBase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelBase.Controls.Add(this.LabelFormTitle);
            this.PanelBase.Controls.Add(this.labelError);
            this.PanelBase.Controls.Add(this.PictureBoxLogo);
            this.PanelBase.Controls.Add(this.PanelLanguageSelect);
            this.PanelBase.Location = new System.Drawing.Point(0, 0);
            this.PanelBase.Name = "PanelBase";
            this.PanelBase.Size = new System.Drawing.Size(600, 334);
            this.PanelBase.TabIndex = 17;
            // 
            // LabelFormTitle
            // 
            this.LabelFormTitle.AutoSize = true;
            this.LabelFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelFormTitle.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LabelFormTitle.Location = new System.Drawing.Point(19, 47);
            this.LabelFormTitle.Name = "LabelFormTitle";
            this.LabelFormTitle.Size = new System.Drawing.Size(198, 33);
            this.LabelFormTitle.TabIndex = 18;
            this.LabelFormTitle.Text = "form login title";
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Location = new System.Drawing.Point(3, 3);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(10, 13);
            this.labelError.TabIndex = 17;
            this.labelError.Text = " ";
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 334);
            this.Controls.Add(this.PanelBase);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(255, 156);
            this.Name = "BaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SensataBaseForm";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.PanelLanguageSelect.ResumeLayout(false);
            this.PanelLanguageSelect.PerformLayout();
            this.PanelBase.ResumeLayout(false);
            this.PanelBase.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelSeperator;
        private System.Windows.Forms.LinkLabel linkLabelEN;
        private System.Windows.Forms.LinkLabel linkLabelPL;
        public System.Windows.Forms.PictureBox PictureBoxLogo;
        public System.Windows.Forms.ErrorProvider ErrorProvider;
        public System.Windows.Forms.Panel PanelLanguageSelect;
        public System.Windows.Forms.Panel PanelBase;
        private System.Windows.Forms.Label labelError;
        public System.Windows.Forms.Label LabelFormTitle;
    }
}