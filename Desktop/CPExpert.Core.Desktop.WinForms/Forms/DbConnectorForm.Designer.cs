﻿using CPExpert.Core.Desktop.WinForms.Controls;

namespace SQ.Framework.Desktop.Tools.DbConnect
{
    partial class DbConnectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataBaseConnectorControl = new DbConnectorControl();
            this.SuspendLayout();
            // 
            // dataBaseConnectorControl
            // 
            this.dataBaseConnectorControl.Location = new System.Drawing.Point(12, 7);
            this.dataBaseConnectorControl.MaximumSize = new System.Drawing.Size(864, 222);
            this.dataBaseConnectorControl.MinimumSize = new System.Drawing.Size(864, 77);
            this.dataBaseConnectorControl.Name = "dataBaseConnectorControl";
            this.dataBaseConnectorControl.Size = new System.Drawing.Size(864, 77);
            this.dataBaseConnectorControl.TabIndex = 0;
            // 
            // DbConnectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 91);
            this.Controls.Add(this.dataBaseConnectorControl);
            this.MaximumSize = new System.Drawing.Size(918, 275);
            this.MinimumSize = new System.Drawing.Size(918, 130);
            this.Name = "DbConnectorForm";
            this.Text = "DbConnectionTest";
            this.ResumeLayout(false);

        }

        #endregion

        private DbConnectorControl dataBaseConnectorControl;

    }
}