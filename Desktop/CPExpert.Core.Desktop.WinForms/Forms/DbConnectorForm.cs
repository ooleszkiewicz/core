﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SQ.Framework.Desktop.Tools.DbConnect
{
    public partial class DbConnectorForm : Form
    {
        public DbConnectorForm(string initialConnectionString = "", bool whenConnectSuccessCloseForm = false)
        {
            InitializeComponent();
            dataBaseConnectorControl.SetInitialConnectionString(initialConnectionString);
            dataBaseConnectorControl.SizeChanged += (sender, args) =>
            {
                switch (dataBaseConnectorControl.Height)
                {
                    case 77:
                        Height = 130;
                        break;
                    case 222:
                        Height = 275;
                        break;
                    default:
                        Height = 300;
                        break;
                }
            };

            if (whenConnectSuccessCloseForm)
            {
                dataBaseConnectorControl.OnConnectionSuccessEvent += (sender, args) =>
                {
                    if (dataBaseConnectorControl.IsConnectionSuccess)
                    {
                        Close();
                    }
                };
            }
        }

        public string ConnectionString
        {
            get { return dataBaseConnectorControl.ConnectionString; }
        }

        public bool IsConnectionSuccess
        {
            get { return dataBaseConnectorControl.IsConnectionSuccess; }
        }

        public List<Exception> Exceptions
        {
            get { return dataBaseConnectorControl.GetAllExceptions(); }
        }
    }
}
