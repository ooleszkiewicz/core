﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using CPExpert.Core.Desktop.WinForms.Properties;
using CPExpert.Core.Enums;
using CPExpert.Core.Helpers;
using Image = System.Drawing.Image;

namespace CPExpert.Core.Desktop.WinForms.Forms
{
    public partial class BaseForm : Form
    {
        #region Constructor

        public BaseForm()
        {
            InitializeComponent();
            HideLanguageSection();
            DisabledKeyShortcut();
        }

        #endregion

        #region Public methods and implementation

        public virtual void TranslateForm() { }
        public string Translate(string key)
        {
            return Helpers.Internationalization.Translation.Translate(key, Settings.Default.Language);
        }
        public void ShowLanguageSection()
        {
            PanelLanguageSelect.Visible = true;
        }
        public void HideLanguageSection()
        {
            PanelLanguageSelect.Visible = false;
        }
        public void ReloadTranslation()
        {
            try
            {
                ErrorProvider.Clear();
                SetLanguageLinkState();
                TranslateForm();
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
        public void SetError(string message)
        {
            ErrorProvider.SetError(labelError, message);
        }
        public void SetLanguage(string language)
        {
            try
            {
                var currentLanguage = !string.IsNullOrEmpty(language) ? language : CultureInfo.InstalledUICulture.Name;
                if (!string.IsNullOrEmpty(currentLanguage))
                {
                    if (!currentLanguage.Equals(Settings.Default.Language))
                    {
                        Settings.Default.Language = currentLanguage;
                        Settings.Default.Save();
                        ReloadTranslation();
                    }
                }
                else
                {
                    throw new Exception("Language not set correctly!");
                }
            }
            catch (Exception e)
            {
                Diagnostic.Log(e);
                throw;
            }
        }
        public virtual void KeyPressOnFormHandler(object sender, KeyEventArgs keyEventArgs)
        {
            Diagnostic.LogDebug("Key press on form: {0}", keyEventArgs.KeyCode);
        }
        public void EnabledKeyShortcut()
        {
            KeyDown += BaseForm_KeyDown;
        }
        public void DisabledKeyShortcut()
        {
            KeyDown -= BaseForm_KeyDown;
        }
        public void SetLogo(Image logo)
        {
            if (logo != null)
            {
                PictureBoxLogo.Image = logo;
                PictureBoxLogo.SizeMode = PictureBoxSizeMode.StretchImage;
                PictureBoxLogo.Visible = true;
            }
        }

        #endregion

        #region Private methods and implementation

        private void SetLanguageLinkState()
        {
            var LanguagesCode = Enum<LanguagesCodes>.GetEnum(Settings.Default.Language);

            switch (LanguagesCode)
            {
                case LanguagesCodes.Polsih:
                    linkLabelEN.LinkColor = Color.Gray;
                    linkLabelEN.Font = new Font(linkLabelEN.Font, FontStyle.Regular);

                    linkLabelPL.LinkColor = Color.Black;
                    linkLabelPL.Font = new Font(linkLabelPL.Font, FontStyle.Bold);
                    break;
                case LanguagesCodes.English:
                    linkLabelEN.LinkColor = Color.Black;
                    linkLabelEN.Font = new Font(linkLabelEN.Font, FontStyle.Bold);

                    linkLabelPL.LinkColor = Color.Gray;
                    linkLabelPL.Font = new Font(linkLabelPL.Font, FontStyle.Regular);
                    break;
            }
        }
        private void linkLabelPL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var language = Enum<LanguagesCodes>.GetDescription(LanguagesCodes.Polsih);
            SetLanguage(language);
        }
        private void linkLabelEN_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var language = Enum<LanguagesCodes>.GetDescription(LanguagesCodes.English);
            SetLanguage(language);
        }
        private void BaseForm_KeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            if (sender != null && keyEventArgs != null)
            {
                KeyPressOnFormHandler(sender, keyEventArgs);
            }
        }

        #endregion

        #region Private fileds


        #endregion
    }
}
