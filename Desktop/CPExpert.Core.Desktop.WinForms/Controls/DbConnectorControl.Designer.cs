﻿namespace CPExpert.Core.Desktop.WinForms.Controls
{
    partial class DbConnectorControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonTestConnectionString = new System.Windows.Forms.Button();
            this.textBoxConnectionString = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxAvaibleDataBases = new System.Windows.Forms.ComboBox();
            this.labelConcreteDataBase = new System.Windows.Forms.Label();
            this.buttonLoadDatabases = new System.Windows.Forms.Button();
            this.textBoxDataBaseInstanceName = new System.Windows.Forms.TextBox();
            this.labelDataBaseInstanceName = new System.Windows.Forms.Label();
            this.textBoxDataBaseInstanceAddress = new System.Windows.Forms.TextBox();
            this.labelDataBaseInstanceAddress = new System.Windows.Forms.Label();
            this.textBoxUserPassword = new System.Windows.Forms.TextBox();
            this.labelUserPassword = new System.Windows.Forms.Label();
            this.comboBoxSecurityConnectionMode = new System.Windows.Forms.ComboBox();
            this.labelSecurityConnectionMode = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.labelUserName = new System.Windows.Forms.Label();
            this.buttonExpandPanel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonTestConnectionString
            // 
            this.buttonTestConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTestConnectionString.Location = new System.Drawing.Point(21, 187);
            this.buttonTestConnectionString.Name = "buttonTestConnectionString";
            this.buttonTestConnectionString.Size = new System.Drawing.Size(778, 23);
            this.buttonTestConnectionString.TabIndex = 0;
            this.buttonTestConnectionString.Text = "Try Connect to data base by using CONNECTION STRING";
            this.buttonTestConnectionString.UseVisualStyleBackColor = true;
            this.buttonTestConnectionString.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxConnectionString
            // 
            this.textBoxConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxConnectionString.Location = new System.Drawing.Point(21, 161);
            this.textBoxConnectionString.Name = "textBoxConnectionString";
            this.textBoxConnectionString.Size = new System.Drawing.Size(778, 20);
            this.textBoxConnectionString.TabIndex = 1;
            this.textBoxConnectionString.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.comboBoxAvaibleDataBases);
            this.groupBox1.Controls.Add(this.labelConcreteDataBase);
            this.groupBox1.Controls.Add(this.buttonLoadDatabases);
            this.groupBox1.Controls.Add(this.textBoxDataBaseInstanceName);
            this.groupBox1.Controls.Add(this.labelDataBaseInstanceName);
            this.groupBox1.Controls.Add(this.textBoxDataBaseInstanceAddress);
            this.groupBox1.Controls.Add(this.labelDataBaseInstanceAddress);
            this.groupBox1.Controls.Add(this.textBoxUserPassword);
            this.groupBox1.Controls.Add(this.labelUserPassword);
            this.groupBox1.Controls.Add(this.comboBoxSecurityConnectionMode);
            this.groupBox1.Controls.Add(this.labelSecurityConnectionMode);
            this.groupBox1.Controls.Add(this.textBoxUserName);
            this.groupBox1.Controls.Add(this.labelUserName);
            this.groupBox1.Location = new System.Drawing.Point(21, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(778, 143);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data base configuration";
            // 
            // comboBoxAvaibleDataBases
            // 
            this.comboBoxAvaibleDataBases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAvaibleDataBases.Enabled = false;
            this.comboBoxAvaibleDataBases.FormattingEnabled = true;
            this.comboBoxAvaibleDataBases.ItemHeight = 13;
            this.comboBoxAvaibleDataBases.Location = new System.Drawing.Point(451, 113);
            this.comboBoxAvaibleDataBases.Name = "comboBoxAvaibleDataBases";
            this.comboBoxAvaibleDataBases.Size = new System.Drawing.Size(321, 21);
            this.comboBoxAvaibleDataBases.TabIndex = 23;
            this.comboBoxAvaibleDataBases.SelectedIndexChanged += new System.EventHandler(this.comboBoxAvaibleDataBases_SelectedIndexChanged);
            // 
            // labelConcreteDataBase
            // 
            this.labelConcreteDataBase.AutoSize = true;
            this.labelConcreteDataBase.Location = new System.Drawing.Point(448, 97);
            this.labelConcreteDataBase.Name = "labelConcreteDataBase";
            this.labelConcreteDataBase.Size = new System.Drawing.Size(85, 13);
            this.labelConcreteDataBase.TabIndex = 25;
            this.labelConcreteDataBase.Text = "Data base name";
            // 
            // buttonLoadDatabases
            // 
            this.buttonLoadDatabases.Enabled = false;
            this.buttonLoadDatabases.Location = new System.Drawing.Point(348, 113);
            this.buttonLoadDatabases.Name = "buttonLoadDatabases";
            this.buttonLoadDatabases.Size = new System.Drawing.Size(97, 21);
            this.buttonLoadDatabases.TabIndex = 22;
            this.buttonLoadDatabases.Text = "Load databases";
            this.buttonLoadDatabases.UseVisualStyleBackColor = true;
            this.buttonLoadDatabases.Click += new System.EventHandler(this.buttonLoadDatabases_Click);
            // 
            // textBoxDataBaseInstanceName
            // 
            this.textBoxDataBaseInstanceName.Location = new System.Drawing.Point(351, 74);
            this.textBoxDataBaseInstanceName.Name = "textBoxDataBaseInstanceName";
            this.textBoxDataBaseInstanceName.ReadOnly = true;
            this.textBoxDataBaseInstanceName.Size = new System.Drawing.Size(424, 20);
            this.textBoxDataBaseInstanceName.TabIndex = 20;
            this.textBoxDataBaseInstanceName.TextChanged += new System.EventHandler(this.textBoxConnectionDataTextImput_TextChanged);
            // 
            // labelDataBaseInstanceName
            // 
            this.labelDataBaseInstanceName.AutoSize = true;
            this.labelDataBaseInstanceName.Location = new System.Drawing.Point(348, 58);
            this.labelDataBaseInstanceName.Name = "labelDataBaseInstanceName";
            this.labelDataBaseInstanceName.Size = new System.Drawing.Size(128, 13);
            this.labelDataBaseInstanceName.TabIndex = 24;
            this.labelDataBaseInstanceName.Text = "Data base instance name";
            // 
            // textBoxDataBaseInstanceAddress
            // 
            this.textBoxDataBaseInstanceAddress.Location = new System.Drawing.Point(348, 34);
            this.textBoxDataBaseInstanceAddress.Name = "textBoxDataBaseInstanceAddress";
            this.textBoxDataBaseInstanceAddress.ReadOnly = true;
            this.textBoxDataBaseInstanceAddress.Size = new System.Drawing.Size(424, 20);
            this.textBoxDataBaseInstanceAddress.TabIndex = 19;
            this.textBoxDataBaseInstanceAddress.TextChanged += new System.EventHandler(this.textBoxConnectionDataTextImput_TextChanged);
            // 
            // labelDataBaseInstanceAddress
            // 
            this.labelDataBaseInstanceAddress.AutoSize = true;
            this.labelDataBaseInstanceAddress.Location = new System.Drawing.Point(348, 18);
            this.labelDataBaseInstanceAddress.Name = "labelDataBaseInstanceAddress";
            this.labelDataBaseInstanceAddress.Size = new System.Drawing.Size(139, 13);
            this.labelDataBaseInstanceAddress.TabIndex = 21;
            this.labelDataBaseInstanceAddress.Text = "Data base instance address";
            // 
            // textBoxUserPassword
            // 
            this.textBoxUserPassword.Location = new System.Drawing.Point(6, 113);
            this.textBoxUserPassword.Name = "textBoxUserPassword";
            this.textBoxUserPassword.ReadOnly = true;
            this.textBoxUserPassword.Size = new System.Drawing.Size(328, 20);
            this.textBoxUserPassword.TabIndex = 17;
            this.textBoxUserPassword.TextChanged += new System.EventHandler(this.textBoxConnectionDataTextImput_TextChanged);
            // 
            // labelUserPassword
            // 
            this.labelUserPassword.AutoSize = true;
            this.labelUserPassword.Location = new System.Drawing.Point(9, 97);
            this.labelUserPassword.Name = "labelUserPassword";
            this.labelUserPassword.Size = new System.Drawing.Size(127, 13);
            this.labelUserPassword.TabIndex = 18;
            this.labelUserPassword.Text = "Data base user password";
            // 
            // comboBoxSecurityConnectionMode
            // 
            this.comboBoxSecurityConnectionMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSecurityConnectionMode.Enabled = false;
            this.comboBoxSecurityConnectionMode.FormattingEnabled = true;
            this.comboBoxSecurityConnectionMode.Location = new System.Drawing.Point(6, 34);
            this.comboBoxSecurityConnectionMode.Name = "comboBoxSecurityConnectionMode";
            this.comboBoxSecurityConnectionMode.Size = new System.Drawing.Size(328, 21);
            this.comboBoxSecurityConnectionMode.TabIndex = 14;
            this.comboBoxSecurityConnectionMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxSecurityConnectionMode_SelectedIndexChanged);
            // 
            // labelSecurityConnectionMode
            // 
            this.labelSecurityConnectionMode.AutoSize = true;
            this.labelSecurityConnectionMode.Location = new System.Drawing.Point(9, 18);
            this.labelSecurityConnectionMode.Name = "labelSecurityConnectionMode";
            this.labelSecurityConnectionMode.Size = new System.Drawing.Size(74, 13);
            this.labelSecurityConnectionMode.TabIndex = 15;
            this.labelSecurityConnectionMode.Text = "Security mode";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(6, 74);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.ReadOnly = true;
            this.textBoxUserName.Size = new System.Drawing.Size(328, 20);
            this.textBoxUserName.TabIndex = 16;
            this.textBoxUserName.TextChanged += new System.EventHandler(this.textBoxConnectionDataTextImput_TextChanged);
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(9, 58);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(108, 13);
            this.labelUserName.TabIndex = 13;
            this.labelUserName.Text = "Data base user name";
            // 
            // buttonExpandPanel
            // 
            this.buttonExpandPanel.Image = global::CPExpert.Core.Desktop.WinForms.Properties.Resources.two_down_arrows;
            this.buttonExpandPanel.Location = new System.Drawing.Point(805, 15);
            this.buttonExpandPanel.Name = "buttonExpandPanel";
            this.buttonExpandPanel.Size = new System.Drawing.Size(48, 50);
            this.buttonExpandPanel.TabIndex = 2;
            this.buttonExpandPanel.UseVisualStyleBackColor = true;
            this.buttonExpandPanel.Click += new System.EventHandler(this.button2_Click);
            // 
            // DbConnectorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonExpandPanel);
            this.Controls.Add(this.textBoxConnectionString);
            this.Controls.Add(this.buttonTestConnectionString);
            this.MaximumSize = new System.Drawing.Size(864, 222);
            this.MinimumSize = new System.Drawing.Size(864, 77);
            this.Name = "DbConnectorControl";
            this.Size = new System.Drawing.Size(864, 222);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonTestConnectionString;
        private System.Windows.Forms.TextBox textBoxConnectionString;
        private System.Windows.Forms.Button buttonExpandPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxAvaibleDataBases;
        private System.Windows.Forms.Label labelConcreteDataBase;
        private System.Windows.Forms.Button buttonLoadDatabases;
        private System.Windows.Forms.TextBox textBoxDataBaseInstanceName;
        private System.Windows.Forms.Label labelDataBaseInstanceName;
        private System.Windows.Forms.TextBox textBoxDataBaseInstanceAddress;
        private System.Windows.Forms.Label labelDataBaseInstanceAddress;
        private System.Windows.Forms.TextBox textBoxUserPassword;
        private System.Windows.Forms.Label labelUserPassword;
        private System.Windows.Forms.ComboBox comboBoxSecurityConnectionMode;
        private System.Windows.Forms.Label labelSecurityConnectionMode;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label labelUserName;
    }
}

