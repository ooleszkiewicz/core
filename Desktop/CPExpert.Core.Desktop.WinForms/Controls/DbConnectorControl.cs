﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using CPExpert.Core.Desktop.WinForms.Properties;
using CPExpert.Core.Enums.DataBase;
using CPExpert.Core.Helpers;

namespace CPExpert.Core.Desktop.WinForms.Controls
{
    public partial class DbConnectorControl : UserControl
    {
        public DbConnectorControl()
        {
            InitializeComponent();
            Height = 77;
            groupBox1.Visible = false;
            var items = Utility.DataBaseSecurityConnectionModesValues;
            comboBoxSecurityConnectionMode.DataSource = items;
            comboBoxSecurityConnectionMode.DisplayMember = "DisplayName";
            comboBoxSecurityConnectionMode.ValueMember = "DataBaseSecurityConnectionMode";
            buttonTestConnectionString.Enabled = false;
            textBoxConnectionString.Text = string.Empty;
        }

        public string ConnectionString
        {
            get
            {
                if (textBoxConnectionString != null && !string.IsNullOrEmpty(textBoxConnectionString.Text))
                {
                    return textBoxConnectionString.Text;
                }

                return string.Empty;
            }
        }

        public List<Exception> GetAllExceptions()
        {
            var exceptions = new List<Exception>();

            try
            {
                if (!string.IsNullOrEmpty(ConnectionString))
                {
                    var myConnection = new SqlConnection(ConnectionString);
                    try
                    {
                        myConnection.Open();
                    }
                    catch (Exception ex)
                    {
                        exceptions.Add(ex);
                    }
                    finally
                    {
                        myConnection.Close();
                    }
                }
                else
                {
                    exceptions.Add(new Exception("connection string is empty!"));
                }
            }
            catch (Exception ex)
            {
                exceptions.Add(ex);
            }

            return exceptions;
        }

        public bool IsConnectionSuccess
        {
            get
            {
                var allExceptions = GetAllExceptions();
                return !allExceptions.Any();
            }
        }

        public void SetInitialConnectionString(string initialConnectionString)
        {
            if (!string.IsNullOrEmpty(initialConnectionString))
            {
                textBoxConnectionString.Text = initialConnectionString;
            }
            else
            {
                textBoxConnectionString.Text = string.Empty;
            }
        }

        public EventHandler OnConnectionSuccessEvent { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            var exceptions = GetAllExceptions();
            var isSuccess = exceptions != null && !exceptions.Any();

            try
            {
                MessageBox.Show(string.Format("Connect to data base : {0}", isSuccess ? "SUCCESS" : "FAILD"), @"Message", MessageBoxButtons.OK, isSuccess ? MessageBoxIcon.Information : MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
                if (exceptions != null)
                {
                    exceptions.Add(ex);
                }
            }
            finally
            {
                if (exceptions != null)
                {
                    foreach (var exception in exceptions)
                    {
                        MessageBox.Show(exception.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                RaiseOnConnectionSuccessEvent(isSuccess);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            switch (Height)
            {
                case 77:
                    Height = 222;
                    buttonExpandPanel.Image = Resources.two_up_arrows;
                    textBoxConnectionString.ReadOnly = true;
                    groupBox1.Visible = true;
                    comboBoxSecurityConnectionMode.Enabled = true;
                    textBoxDataBaseInstanceAddress.Enabled = true;
                    textBoxDataBaseInstanceAddress.ReadOnly = false;
                    textBoxDataBaseInstanceName.Enabled = true;
                    textBoxDataBaseInstanceName.ReadOnly = false;

                    if (!string.IsNullOrEmpty(textBoxConnectionString.Text))
                    {
                        try
                        {
                            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(textBoxConnectionString.Text);

                            if (!string.IsNullOrEmpty(sqlConnectionStringBuilder.DataSource))
                            {
                                var dataSourceArray = sqlConnectionStringBuilder.DataSource.Split('\\');

                                if (dataSourceArray.Length == 2)
                                {
                                    if (sqlConnectionStringBuilder.IntegratedSecurity)
                                    {
                                        //zintegrowane

                                        comboBoxSecurityConnectionMode.SelectedIndex = comboBoxSecurityConnectionMode
                                            .FindString(Enum<DbSecurityModes>.GetDescription(DbSecurityModes.IntegratedSecurity));
                                        textBoxUserName.Text = string.Empty;
                                        textBoxUserPassword.Text = string.Empty;
                                        textBoxDataBaseInstanceAddress.Text = dataSourceArray[0];
                                        textBoxDataBaseInstanceName.Text = dataSourceArray[1];

                                    }
                                    else
                                    {
                                        //pass i login

                                        comboBoxSecurityConnectionMode.SelectedIndex = comboBoxSecurityConnectionMode
                                            .FindString(Enum<DbSecurityModes>.GetDescription(DbSecurityModes.UserAndPassword));
                                        textBoxUserName.Text = sqlConnectionStringBuilder.UserID;
                                        textBoxUserPassword.Text = sqlConnectionStringBuilder.Password;
                                        textBoxDataBaseInstanceAddress.Text = dataSourceArray[0];
                                        textBoxDataBaseInstanceName.Text = dataSourceArray[1];
                                    }

                                    //szybkie załadowani baz
                                    buttonLoadDatabases_Click(this, null);

                                    comboBoxAvaibleDataBases.SelectedIndex = comboBoxAvaibleDataBases.FindString(sqlConnectionStringBuilder.InitialCatalog);
                                }
                            }
                        }
                        catch
                        {
                            // ignored
                        }
                    }

                    break;
                case 222:
                    Height = 77;
                    buttonExpandPanel.Image = Resources.two_down_arrows;
                    textBoxConnectionString.ReadOnly = false;
                    groupBox1.Visible = false;
                    comboBoxSecurityConnectionMode.Enabled = false;
                    textBoxDataBaseInstanceAddress.Enabled = false;
                    textBoxDataBaseInstanceAddress.ReadOnly = true;
                    textBoxDataBaseInstanceName.Enabled = false;
                    textBoxDataBaseInstanceName.ReadOnly = true;
                    break;
            }
        }
        private void comboBoxSecurityConnectionMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxAvaibleDataBases.DataSource = null;
            var selectedItem = comboBoxSecurityConnectionMode.SelectedItem;
            if (selectedItem is DataBaseSecurityConnectionModesComboBoxValue)
            {
                var selectedItemConcrete = (DataBaseSecurityConnectionModesComboBoxValue)selectedItem;

                switch (selectedItemConcrete.DataBaseSecurityConnectionMode)
                {
                    case DbSecurityModes.IntegratedSecurity:
                        Lock_LoginAndPasswordSection();
                        buttonLoadDatabases.Enabled = !string.IsNullOrEmpty(textBoxDataBaseInstanceAddress.Text) && !string.IsNullOrEmpty(textBoxDataBaseInstanceName.Text);
                        break;
                    case DbSecurityModes.UserAndPassword:
                        Unlock_LoginAndPasswordSection();
                        buttonLoadDatabases.Enabled = !string.IsNullOrEmpty(textBoxDataBaseInstanceAddress.Text) && !string.IsNullOrEmpty(textBoxDataBaseInstanceName.Text) &&
                              !string.IsNullOrEmpty(textBoxUserName.Text) && !string.IsNullOrEmpty(textBoxUserPassword.Text);
                        break;
                }
            }

            comboBoxAvaibleDataBases.Enabled = buttonLoadDatabases.Enabled;
        }
        private void Unlock_LoginAndPasswordSection()
        {
            textBoxUserName.ReadOnly = false;
            textBoxUserPassword.ReadOnly = false;
        }
        private void Lock_LoginAndPasswordSection()
        {
            textBoxUserName.ReadOnly = true;
            textBoxUserPassword.ReadOnly = true;
            textBoxUserName.Text = string.Empty;
            textBoxUserPassword.Text = string.Empty;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            buttonTestConnectionString.Enabled = !string.IsNullOrEmpty(textBoxConnectionString.Text) && textBoxConnectionString.Text.Length > 5;
        }
        private void textBoxConnectionDataTextImput_TextChanged(object sender, EventArgs e)
        {
            comboBoxAvaibleDataBases.DataSource = null;
            var selectedItem = comboBoxSecurityConnectionMode.SelectedItem;
            if (selectedItem is DataBaseSecurityConnectionModesComboBoxValue)
            {
                var selectedItemConcrete = (DataBaseSecurityConnectionModesComboBoxValue)selectedItem;

                switch (selectedItemConcrete.DataBaseSecurityConnectionMode)
                {
                    case DbSecurityModes.IntegratedSecurity:
                        buttonLoadDatabases.Enabled = !string.IsNullOrEmpty(textBoxDataBaseInstanceAddress.Text) && !string.IsNullOrEmpty(textBoxDataBaseInstanceName.Text);
                        break;
                    case DbSecurityModes.UserAndPassword:
                        buttonLoadDatabases.Enabled = !string.IsNullOrEmpty(textBoxDataBaseInstanceAddress.Text) && !string.IsNullOrEmpty(textBoxDataBaseInstanceName.Text) &&
                                                      !string.IsNullOrEmpty(textBoxUserName.Text) && !string.IsNullOrEmpty(textBoxUserPassword.Text);
                        break;
                }
            }

            comboBoxAvaibleDataBases.Enabled = buttonLoadDatabases.Enabled;
        }

        private void buttonLoadDatabases_Click(object sender, EventArgs e)
        {
            Enabled = false;

            string currentDataBase = comboBoxAvaibleDataBases.Text;
            object selectedItemObject = comboBoxSecurityConnectionMode.SelectedItem;

            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            sqlConnectionStringBuilder.DataSource = string.Format(@"{0}\{1}", textBoxDataBaseInstanceAddress.Text, textBoxDataBaseInstanceName.Text);
            sqlConnectionStringBuilder.InitialCatalog = @"master";
            sqlConnectionStringBuilder.ConnectTimeout = 10;
            sqlConnectionStringBuilder.MultipleActiveResultSets = true;

            if (selectedItemObject != null && selectedItemObject is DataBaseSecurityConnectionModesComboBoxValue)
            {
                var selectedItem = (DataBaseSecurityConnectionModesComboBoxValue) selectedItemObject;
                if (selectedItem.DataBaseSecurityConnectionMode == DbSecurityModes.IntegratedSecurity)
                {
                    sqlConnectionStringBuilder.IntegratedSecurity = true;
                }
                else
                {
                    sqlConnectionStringBuilder.IntegratedSecurity = false;
                    sqlConnectionStringBuilder.UserID = textBoxUserName.Text;
                    sqlConnectionStringBuilder.Password = textBoxUserPassword.Text;
                }
            }

            var myConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);
            try
            {
                myConnection.Open();
                var dataBasesList = new List<string>();
                var databases = myConnection.GetSchema("Databases");

                foreach (DataRow database in databases.Rows)
                {
                    var databaseName = database.Field<string>("database_name");
                    if (!string.IsNullOrEmpty(databaseName))
                    {
                        if (!dataBasesList.Contains(databaseName))
                        {
                            sqlConnectionStringBuilder.InitialCatalog = databaseName;
                            dataBasesList.Add(databaseName);
                        }
                    }
                }

                if (dataBasesList.Any())
                {
                    comboBoxAvaibleDataBases.DataSource = dataBasesList;
                    comboBoxAvaibleDataBases.Enabled = true;

                    if (dataBasesList.FirstOrDefault(x => x.Equals(currentDataBase)) != null)
                    {
                        comboBoxAvaibleDataBases.SelectedItem = currentDataBase;
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
            }
            finally
            {
                myConnection.Close();
            }

            Enabled = true;
        }

        private void comboBoxAvaibleDataBases_SelectedIndexChanged(object sender, EventArgs e)
        {
            //tworzenie connection stringa
            try
            {
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
                sqlConnectionStringBuilder.DataSource = string.Format(@"{0}\{1}", textBoxDataBaseInstanceAddress.Text, textBoxDataBaseInstanceName.Text);
                sqlConnectionStringBuilder.ConnectTimeout = 90;
                sqlConnectionStringBuilder.MultipleActiveResultSets = true;

                var selectedItemObject = comboBoxSecurityConnectionMode.SelectedItem;
                if (selectedItemObject is DataBaseSecurityConnectionModesComboBoxValue)
                {
                    var selectedItem = (DataBaseSecurityConnectionModesComboBoxValue)selectedItemObject;
                    if (selectedItem.DataBaseSecurityConnectionMode == DbSecurityModes.IntegratedSecurity)
                    {
                        sqlConnectionStringBuilder.IntegratedSecurity = true;
                    }
                    else
                    {
                        sqlConnectionStringBuilder.IntegratedSecurity = false;
                        sqlConnectionStringBuilder.UserID = textBoxUserName.Text;
                        sqlConnectionStringBuilder.Password = textBoxUserPassword.Text;
                    }
                }

                var selectedDataBaseObject = comboBoxAvaibleDataBases.SelectedItem;
                if (selectedDataBaseObject is string)
                {
                    sqlConnectionStringBuilder.InitialCatalog = Convert.ToString(selectedDataBaseObject);

                    var myConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);
                    try
                    {
                        myConnection.Open();
                        textBoxConnectionString.Text = sqlConnectionStringBuilder.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        Diagnostic.Log(ex);
                        textBoxConnectionString.Text = string.Empty;
                    }
                    finally
                    {
                        myConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostic.Log(ex);
            }
        }

        private void RaiseOnConnectionSuccessEvent(bool isSuccess)
        {
            if (OnConnectionSuccessEvent != null && isSuccess)
            {
                OnConnectionSuccessEvent(null, new EventArgs());
            }
        }

        internal class Utility
        {
            public static List<DataBaseSecurityConnectionModesComboBoxValue> DataBaseSecurityConnectionModesValues
            {
                get
                {
                    var dataBaseSecurityConnectionModesValues = new List<DataBaseSecurityConnectionModesComboBoxValue>();

                    try
                    {
                        var items = Enum.GetValues(typeof(DbSecurityModes));
                        if (items.Length > 0)
                        {
                            foreach (DbSecurityModes item in items)
                            {
                                switch (item)
                                {
                                    case DbSecurityModes.IntegratedSecurity:
                                        dataBaseSecurityConnectionModesValues.Add(new DataBaseSecurityConnectionModesComboBoxValue(DbSecurityModes.IntegratedSecurity,
                                            Enum<DbSecurityModes>.GetDescription(DbSecurityModes.IntegratedSecurity)));
                                        break;

                                    case DbSecurityModes.UserAndPassword:
                                        dataBaseSecurityConnectionModesValues.Add(new DataBaseSecurityConnectionModesComboBoxValue(DbSecurityModes.UserAndPassword,
                                            Enum<DbSecurityModes>.GetDescription(DbSecurityModes.UserAndPassword)));
                                        break;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Diagnostic.Log(e);
                    }

                    return dataBaseSecurityConnectionModesValues;
                }
            }
        }

        internal class DataBaseSecurityConnectionModesComboBoxValue
        {
            public DataBaseSecurityConnectionModesComboBoxValue(DbSecurityModes dataBaseSecurityConnectionMode, string displayName)
            {
                DataBaseSecurityConnectionMode = dataBaseSecurityConnectionMode;
                DisplayName = displayName;
            }

            public DbSecurityModes DataBaseSecurityConnectionMode { get; private set; }
            public string DisplayName { get; private set; }
        }
    }
}
