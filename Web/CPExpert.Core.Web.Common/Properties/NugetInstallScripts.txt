mkdir "Y:\Symbols"

################ CPExpert.Core
nuget pack 'C:\Projects\Public Community\Core\CPExpert.Core\CPExpert.Core.csproj' -OutputDirectory 'Y:\' -IncludeReferencedProjects -Build -Properties Configuration=Debug -Verbose -Symbols

move "Y:\*.symbols.nupkg" "Y:\Symbols"
nuget push "Y:\Symbols\*.symbols.nupkg" 123 -Source http://www.cp-expert.pl/SymbolServer/NuGet
del "Y:\Symbols\*"

################ on nuget web site!
nuget pack 'C:\Projects\Public Community\CpExpert.Framework\Core\CPExpert.Core\CPExpert.Core.csproj' -IncludeReferencedProjects -Build -Properties Configuration=Debug -Symbols
nuget push CPExpert.Core.1.0.0.85.nupkg oy2dlg46ppfga7lndzcqc5lctgehfmfungvkhc4fmmqo44 -Source https://www.nuget.org/api/v2/package
del *.nupkg