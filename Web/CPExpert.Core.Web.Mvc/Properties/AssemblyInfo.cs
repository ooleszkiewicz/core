﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CP-EXPERT Core Web")]
[assembly: AssemblyProduct("CP-EXPERT Core Web MVC Library")]
[assembly: AssemblyDescription("CP-EXPERT Core Web MVC library for Computer Programming Expert IT projects")]
[assembly: Guid("6a79aeba-5612-4ef3-b961-0b191650f355")]

[assembly: AssemblyCompany("CP-EXPERT")]
[assembly: AssemblyCopyright("Copyright © 2016 CP-EXPERT")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.0.65")]
[assembly: NeutralResourcesLanguage("en")]