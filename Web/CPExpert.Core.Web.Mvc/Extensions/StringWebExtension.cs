﻿using System;
using System.Diagnostics;
using System.Web;
using CPExpert.Core.Helpers;

namespace CPExpert.Core.Web.Mvc.Extensions
{
    public static class StringWebExtension
    {
        [DebuggerStepThrough]
        public static string UrlEncode(this string value)
        {
            return HttpUtility.UrlEncode(value);
        }

        [DebuggerStepThrough]
        public static string UrlDecode(this string value)
        {
            return HttpUtility.UrlDecode(value);
        }

        [DebuggerStepThrough]
        public static string AttributeEncode(this string value)
        {
            return HttpUtility.HtmlAttributeEncode(value);
        }

        [DebuggerStepThrough]
        public static string HtmlEncode(this string value)
        {
            return HttpUtility.HtmlEncode(value);
        }

        [DebuggerStepThrough]
        public static string HtmlDecode(this string value)
        {
            return HttpUtility.HtmlDecode(value);
        }

        /// <summary>Smart way to create a HTML attribute with a leading space.</summary>
        /// <param name="value"></param>
        /// <param name="name">Name of the attribute.</param>
        /// <param name="htmlEncode"></param>
        public static string ToAttribute(this string value, string name, bool htmlEncode = true)
        {
            if (value == null || Enumerable.IsNullOrEmpty(name))
                return "";

            if (value == "" && name != "value" && !name.StartsWith("data"))
                return "";

            if (name == "maxlength" && (value == "" || value == "0"))
                return "";

            if (name == "checked" || name == "disabled" || name == "multiple")
            {
                if (value == "" || String.Compare(value, "false", StringComparison.OrdinalIgnoreCase) == 0)
                    return "";
                value = (String.Compare(value, "true", StringComparison.OrdinalIgnoreCase) == 0 ? name : value);
            }

            if (name.StartsWith("data"))
                name = name.Insert(4, "-");

            return string.Format(" {0}=\"{1}\"", name, htmlEncode ? HttpUtility.HtmlEncode(value) : value);
        }
    }
}