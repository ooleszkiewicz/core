﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CPExpert.Core.Web.Mvc.Helpers.Html
{
    public static class HtmlTranslateHelper
    {
        #region Label, LabelFor, LabelForModel

        public static MvcHtmlString TranslateLabel(this HtmlHelper html, string expression, object htmlAttributes)
        {
            return TranslateLabel(html, expression, htmlAttributes, null);
        }

        public static MvcHtmlString TranslateLabel(this HtmlHelper html, string expression, IDictionary<string, object> htmlAttributes)
        {
            return TranslateLabel(html, expression, htmlAttributes, null);
        }

        internal static MvcHtmlString TranslateLabel(this HtmlHelper html, string expression, object htmlAttributes, ModelMetadataProvider metadataProvider)
        {
            return TranslateLabel(html, expression, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), metadataProvider);
        }

        internal static MvcHtmlString TranslateLabel(this HtmlHelper html, string expression, IDictionary<string, object> htmlAttributes, ModelMetadataProvider metadataProvider)
        {
            return TranslateLabelHelper(html, ModelMetadata.FromStringExpression(expression, html.ViewData), expression, htmlAttributes);
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString TranslateLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            return TranslateLabelFor(html, expression, null, null);
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString TranslateLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            return TranslateLabelFor(html, expression, htmlAttributes, null);
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString TranslateLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        {
            return TranslateLabelFor(html, expression, htmlAttributes, null);
        }

        internal static MvcHtmlString TranslateLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes, ModelMetadataProvider metadataProvider)
        {
            return TranslateLabelFor(html, expression, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), metadataProvider);
        }

        internal static MvcHtmlString TranslateLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes, ModelMetadataProvider metadataProvider)
        {
            return TranslateLabelHelper(html, ModelMetadata.FromLambdaExpression(expression, html.ViewData), ExpressionHelper.GetExpressionText(expression), htmlAttributes);
        }

        public static MvcHtmlString TranslateLabelForModel(this HtmlHelper html)
        {
            return TranslateLabelHelper(html, html.ViewData.ModelMetadata, string.Empty);
        }

        public static MvcHtmlString TranslateLabelForModel(this HtmlHelper html, object htmlAttributes)
        {
            return TranslateLabelHelper(html, html.ViewData.ModelMetadata, string.Empty, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString TranslateLabelForModel(this HtmlHelper html, IDictionary<string, object> htmlAttributes)
        {
            return TranslateLabelHelper(html, html.ViewData.ModelMetadata, string.Empty, htmlAttributes);
        }

        internal static MvcHtmlString TranslateLabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName, IDictionary<string, object> htmlAttributes = null)
        {
            var resolvedLabelText = Core.Helpers.Internationalization.Translation.Translate(metadata.DisplayName);
            if (string.IsNullOrEmpty(resolvedLabelText))
            {
                resolvedLabelText = metadata.PropertyName;
                if (string.IsNullOrEmpty(resolvedLabelText))
                {
                    resolvedLabelText = htmlFieldName.Split('.').Last();
                    if (string.IsNullOrEmpty(resolvedLabelText))
                    {
                        return MvcHtmlString.Empty;
                    }
                }
            }

            var tag = new TagBuilder("label");
            tag.Attributes.Add("for", TagBuilder.CreateSanitizedId(html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)));
            tag.SetInnerText(resolvedLabelText);
            tag.MergeAttributes(htmlAttributes, true);
            return new MvcHtmlString(tag.ToString());
        }

        #endregion
    }
}