﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace CPExpert.Core.Web.Mvc.Helpers.Html
{
    public static class CustomHtmlHelper
    {
        public static MvcHtmlString MenuLink(this HtmlHelper helper, string text, string action, string controller)
        {
            var routeData = helper.ViewContext.RouteData.Values;
            var currentController = Convert.ToString(routeData["controller"]);
            var currentAction = Convert.ToString(routeData["action"]);

            var tag = new TagBuilder("li")
            {
                InnerHtml = helper.ActionLink(text, action, controller).ToHtmlString()
            };

            if (string.Equals(action, currentAction, StringComparison.OrdinalIgnoreCase) && string.Equals(controller, currentController, StringComparison.OrdinalIgnoreCase))
            {
                tag.AddCssClass("active");
            }

            return new MvcHtmlString(tag.ToString());
        }
    }
}