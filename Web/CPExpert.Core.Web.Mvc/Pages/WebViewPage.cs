﻿using System.Web.Mvc;
using CPExpert.Core.Enums;
using CPExpert.Core.Helpers;
using CPExpert.Core.Web.Mvc.Interfaces;

namespace CPExpert.Core.Web.Mvc.Pages
{
    public abstract class WebViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {
        public IWorkWebContext WorkWebContext { get; private set; }

        public string T(string key)
        {
            var language = string.Empty;
            if (WorkWebContext != null)
            {
                language = Enum<LanguagesCodes>.GetDescription(WorkWebContext.WorkingLanguage);
            }

            return Core.Helpers.Internationalization.Translation.Translate(key, language);
        }

        public override void InitHelpers()
        {
            base.InitHelpers();
            WorkWebContext = DependencyResolver.Current.GetService<IWorkWebContext>();
        }

        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }

    public abstract class WebViewPage : WebViewPage<dynamic> { }
}