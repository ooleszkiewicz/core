﻿using System.Collections.Generic;
using CPExpert.Core.Enums;

namespace CPExpert.Core.Web.Mvc.Interfaces
{
    public interface IWorkWebContext
    {
        /// <summary>
        /// Get or set current user working language
        /// </summary>
        LanguagesCodes WorkingLanguage { get; }
        string IpAdress { get; }
        List<string> Roles { get; }
    }
}